<?php
/*
Template Name: Nouveau CleanUp
*/
//$current_user = wp_get_current_user();

?>

<?php get_header("membre"); ?> 

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Organiser un Digital Cleanup', 'cwcud');?></h2>
    </div>
</header>
        
<?php
$id_cleanup = false;
if(isset($_GET['id_cleanup'])){
    
    //$post = get_post($_GET['id_cleanup']);
    //setup_postdata($post);
    $id_cleanup = $_GET['id_cleanup'];
}
?>
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<section class="wrapper">
    <form action="" method="post"  name="cleanupForm" id="cleanupForm" class="form-style">
        <input type="hidden" name="honeyPot" value="">
        <input type="hidden" name="id" value="<?php echo $id_cleanup;?>">
        <?php
        if(isset($_GET['imadmin'])){
            echo '<script>var action_url = "'.admin_url().'edit.php?post_type=cleanup";</script>';
        }
        ?>
        <p><h2 class="ctr no-margin"><?php _e('L\'organisateur', 'cwcud');?></h2><p>

        <label for="user_name"><?php _e('Nom Prenom', 'cwcud'); ?>*</label>
        <input type="text" id="user_name" name="user_name" placeholder="" required value="<?php echo get_post_meta($id_cleanup, "user_name", true );?>">
    
        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Type de structure *', 'cwcud');?></p>

            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="citizen" class="structure" value="citizen" onclick="displayRadioValue()" checked>
                <label class="checkbox-label" for="citizen"><?php _e('Citoyen', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="association" class="structure" value="association" onclick="displayRadioValue()" <?php checked("association", get_post_meta($id_cleanup, "type_structure", true ));?>>
                <label class="checkbox-label" for="association"><?php _e('Association', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="school" class="structure" value="school" onclick="displayRadioValue()" <?php checked("school", get_post_meta($id_cleanup, "type_structure", true ));?>>
                <label class="checkbox-label" for="school"><?php _e('École', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="collectivity" class="structure" value="collectivity" onclick="displayRadioValue()"  <?php checked("collectivity", get_post_meta($id_cleanup, "type_structure", true ));?>>
                <label class="checkbox-label" for="collectivity"><?php _e('Collectivité', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="company" class="structure" value="company" onclick="displayRadioValue()" <?php checked("company", get_post_meta($id_cleanup, "type_structure", true ));?>>
                <label class="checkbox-label" for="company"><?php _e('Entreprise', 'cwcud');?></label>
            </div>
            <!--<div class="checkbox form-sub-item">
                <input type="radio" name="type_structure" id="sponsorship" class="structure" value="sponsorship" onclick="displayRadioValue()">
                <label class="checkbox-label" for="sponsorship"><?php _e('Parainage', 'cwcud');?></label>
            </div>-->
        </div> 

        <div class="form-item" id="structure_name">
            <label for="structure_name_id"><?php _e('Nom de la structure', 'cwcud'); ?>*</label>
            <input type="text"  id="structure_name_id" name="structure_name" placeholder="Nom de votre structure" required value="<?php echo get_post_meta($id_cleanup, "structure_name", true );?>">
            <p class="form-info form-sub-item"><?php _e('Afin de conserver un caractère humain et faciliter l’identification de l’organisateur par les participants votre nom et prénom resteront associés au nom de la structure', 'cwcud');?></p>
        </div>

        <div class="form-item">
            <p class="no-margin label-like"><?php _e('Localisation *', 'cwcud');?></p>

            <div class="checkbox form-sub-item">
                <input type="radio" name="localisation" id="localisation_fr" class="" value="fr" <?php checked("fr", get_post_meta($id_cleanup, "localisation", true ));?>>
                <label class="checkbox-label" for="localisation_fr"><?php _e('France', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="localisation" id="localisation_be" class="" value="be" <?php checked("be", get_post_meta($id_cleanup, "localisation", true ));?>>
                <label class="checkbox-label" for="localisation_be"><?php _e('Belgique', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="localisation" id="localisation_ch" class="" value="ch" <?php checked("ch", get_post_meta($id_cleanup, "localisation", true ));?>>
                <label class="checkbox-label" for="localisation_ch"><?php _e('Suisse', 'cwcud');?></label>
            </div>
            <p class="form-info form-sub-item"><?php _e("Si votre pays n'est pas listé, rendez-vous sur le site de l'international pour référencer votre Digital Cleanup <a href=\"https://www.digitalcleanupday.org/\" target=\"_blank\">https://www.digitalcleanupday.org/</a>", 'cwcud');?></p>
        </div> 
        
        <label for="user_email"><?php _e('Adresse email', 'cwcud'); ?>*</label>
        <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required value="<?php echo get_post_meta($id_cleanup, "user_email", true );?>">
        <p class="form-info form-sub-item"><?php _e('Cette information sera visible sur le site Digital Cleanup Day uniquement si votre Cleanup est public - elle permettra aux participants de vous contacter.', 'cwcud');?></p>
        
        <label for="user_phone"><?php _e('Téléphone', 'cwcud'); ?>*</label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required value="<?php echo get_post_meta($id_cleanup, "user_phone", true );?>">
        <p class="form-info form-sub-item"><?php _e('Cette information est nécessaire pour que l’équipe projet puisse vous contacter en cas de besoins.', 'cwcud');?></p>
        <p></p>
        <p class="no-margin label-like"><?php _e('Souhaitez-vous afficher votre téléphone sur la page de votre Digital Cleanup pour les participants ?', 'cwcud');?></p>
        <div class="checkbox form-sub-item">
            <input type="radio" required name="display_phone" id="display_phone_ok" class="" value="display_phone_ok" onchange="" <?php checked("display_phone_ok", get_post_meta($id_cleanup, "display_phone", true ));?>>
            <label class="checkbox-label" for="display_phone_ok"><?php _e('Oui', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" required name="display_phone" id="display_phone_no" class="" value="display_phone_no" onchange="" <?php checked("display_phone_no", get_post_meta($id_cleanup, "display_phone", true ));?>>
            <label class="checkbox-label" for="display_phone_no"><?php _e('Non', 'cwcud');?></label>
        </div>

        <!--<p></p>
        <p><h2 class="ctr no-margin"><?php _e('Réseaux sociaux', 'cwcud'); ?></h2><p class="form-info form-sub-item"><?php _e('Aidez-nous à vous identifier (optionnel)', 'cwcud'); ?></p></p>
        
        <label for="user_twitter"><?php _e('Twitter', 'cwcud'); ?></label>
        <input type="text" id="user_twitter" name="user_twitter" placeholder="@DigitalCleanupDay"  value="<?php echo get_post_meta($id_cleanup, "user_twitter", true );?>">

        <label for="user_facebook"><?php _e('Facebook', 'cwcud'); ?></label>
        <input type="text" id="user_facebook" name="user_facebook" placeholder="https://www.facebook.com/cyberworldcleanupday/"  value="<?php echo get_post_meta($id_cleanup, "user_facebook", true );?>">

        <label for="user_linkedin"><?php _e('Linkedin', 'cwcud'); ?></label>
        <input type="text" id="user_linkedin" name="user_linkedin" placeholder="@DigitalCleanupDay"  value="<?php echo get_post_meta($id_cleanup, "user_linkedin", true );?>">

        <label for="user_instagram"><?php _e('Instagram', 'cwcud'); ?></label>
        <input type="text" id="user_instagram" name="user_instagram" placeholder="@DigitalCleanupDay" value="<?php echo get_post_meta($id_cleanup, "user_instagram", true );?>" >
    -->
        <p></p>
        <p><h2 class="ctr no-margin"><?php _e('Le Cleanup', 'cwcud'); ?></h2></p>
        
        <p class="form-info form-sub-item"><?php _e('Les informations renseignées ici seront celles affichées sur la page de votre Digital Cleanup et sur la carte des Digital Cleanups. 
                <br>Vous pourrez à tout moment modifier les infos sur la page dédiée accessible via votre Compte sur la page "Mes Digital Cleanups".', 'cwcud');?>
        </p>
        <P></P>
        <p class="no-margin label-like"><?php _e('Catégorie de Digital Cleanup *', 'cwcud');?></p>
        
        <div class="checkbox form-sub-item">
            <input type="radio" required name="cat_cybercleanup" id="data" class="" value="data" onchange="onchangeCatCWCUD()" <?php checked("data", (isset($_GET['cat_digitalcleanup']) && !empty($_GET['cat_digitalcleanup'])) ? $_GET['cat_digitalcleanup'] : get_post_meta($id_cleanup, "cat_cybercleanup", true ));?>>
            <label class="checkbox-label" for="data"><?php _e('Digital Cleanup Données', 'cwcud');?></label>
        </div>
        <p class="form-info form-sub-item"><?php _e("Supprimer nos données stockées sur nos équipements et sur le cloud", 'cwcud');?></p>
        <div class="checkbox form-sub-item">
            <input type="radio" name="cat_cybercleanup" id="reuse" class="" value="reuse" onchange="onchangeCatCWCUD()" <?php checked("reuse", (isset($_GET['cat_digitalcleanup']) && !empty($_GET['cat_digitalcleanup'])) ? $_GET['cat_digitalcleanup'] : get_post_meta($id_cleanup, "cat_cybercleanup", true ));?>>
            <label class="checkbox-label" for="reuse"><?php _e('Digital Cleanup Réemploi', 'cwcud');?></label>
        </div>
        <p class="form-info form-sub-item"><?php _e("Ressortir mes anciens équipements fonctionnels et leur donner une seconde vie (protéger, réutiliser, donner, réparer)", 'cwcud');?></p>
        <div class="checkbox form-sub-item">
            <input type="radio" name="cat_cybercleanup" id="hardware" class="" value="hardware" onchange="onchangeCatCWCUD()" <?php checked("hardware", (isset($_GET['cat_digitalcleanup']) && !empty($_GET['cat_digitalcleanup'])) ? $_GET['cat_digitalcleanup'] : get_post_meta($id_cleanup, "cat_cybercleanup", true ));?>>
            <label class="checkbox-label" for="hardware"><?php _e('Digital Cleanup Recyclage', 'cwcud');?></label>
        </div>
        <p class="form-info form-sub-item"><?php _e("Récolter vos équipements numériques non fonctionnels afin de favoriser leur fin de vie (DEEE)", 'cwcud');?></p>

        <div id="checkbox_data" style="<?php echo (get_post_meta($id_cleanup, "cat_cybercleanup", true ) != "data")?'display:none':'';?>">
        <br>
            <p class="no-margin label-like"><?php _e('Vous organisez une (des) session(s) de nettoyage des données numériques sur  ', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="data_cat[]" id="data_desk" class="" value="data_desk" <?php echo (is_array(get_post_meta($id_cleanup, "data_cat", true)) && in_array("data_desk", get_post_meta($id_cleanup, "data_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="data_desk"><?php _e('Vos ordinateurs', 'cwcud');?></label>
            </div>
            
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="data_cat[]" id="smartphone" class="" value="smartphone" <?php echo (is_array(get_post_meta($id_cleanup, "data_cat", true)) && in_array("smartphone", get_post_meta($id_cleanup, "data_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="smartphone"><?php _e('Vos smartphones', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="data_cat[]" id="cloud" class="" value="cloud" <?php echo (is_array(get_post_meta($id_cleanup, "data_cat", true)) && in_array("cloud", get_post_meta($id_cleanup, "data_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="cloud"><?php _e('Vos cloud', 'cwcud');?></label>
            </div>
            <div class="form-sub-item" style="margin-left:1.5em;">
                <label for="data_cat_other" style="font-weight:normal"><?php _e('Autre (précisez)', 'cwcud');?></label>
                <input type="text" name="data_cat_other" id="data_cat_other" class="" value="<?php echo get_post_meta($id_cleanup, "data_cat_other", true );?>">
            </div>
        </div>

        <div id="checkbox_reuse" style="<?php echo (get_post_meta($id_cleanup, "cat_cybercleanup", true ) != "reuse")?'display:none':'';?>">
        <br>
            <p class="no-margin label-like"><?php _e('Vous organisez une (des) session(s) pour ', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="reuse_cat[]" id="reuse_smartphone" class="" value="reuse_smartphone" <?php echo (is_array(get_post_meta($id_cleanup, "reuse_cat", true)) && in_array("reuse_smartphone", get_post_meta($id_cleanup, "reuse_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="reuse_smartphone"><?php _e('Prendre soin de son smartphone (atelier de sensibilisation)', 'cwcud');?></label>
            </div>
            
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="reuse_cat[]" id="reuse_fix" class="" value="reuse_fix" <?php echo (is_array(get_post_meta($id_cleanup, "reuse_cat", true)) && in_array("reuse_fix", get_post_meta($id_cleanup, "reuse_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="reuse_fix"><?php _e('Réparer son ordinateur ou son smartphone (atelier de réparation en interne ou via un partenaire externe)', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="reuse_cat[]" id="reuse_collect" class="" value="reuse_collect" <?php echo (is_array(get_post_meta($id_cleanup, "reuse_cat", true)) && in_array("reuse_collect", get_post_meta($id_cleanup, "reuse_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="reuse_collect"><?php _e('Collecter des smartphones et leur donner une seconde vie (via la GSM Box par ex.) ', 'cwcud');?></label>
            </div>
          
        </div>

        <div id="checkbox_hardware" style="<?php echo (get_post_meta($id_cleanup, "cat_cybercleanup", true ) != "hardware")?'display:none':'';?>">

            <p class=" label-like"><label for="nbr_place_deee"><?php _e('Combien de points de collecte DEEE souhaitez-vous mettre en place ?', 'cwcud');?></label>
            <input type="number" name="nbr_place_deee" id="nbr_place_deee" class="" value="<?php echo max(1, get_post_meta($id_cleanup, "nbr_place_deee", true ));?>">
            <!--<div class="checkbox form-sub-item">
                <input type="checkbox" name="hard_cat[]" id="hard_smartphone" class="" value="smartphones" <?php echo (is_array(get_post_meta($id_cleanup, "hard_cat", true)) && in_array("smartphones", get_post_meta($id_cleanup, "hard_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="hard_smartphone"><?php _e('Smartphones (fonctionnels)', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="hard_cat[]" id="hard_portable" class="" value="PC" <?php echo (is_array(get_post_meta($id_cleanup, "hard_cat", true)) && in_array("PC", get_post_meta($id_cleanup, "hard_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="hard_portable"><?php _e('PC portables & tablettes (fonctionnels)', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="checkbox" name="hard_cat[]" id="hard_deee" class="" value="DEEE" <?php echo (is_array(get_post_meta($id_cleanup, "hard_cat", true)) && in_array("DEEE", get_post_meta($id_cleanup, "hard_cat", true)))?'checked':'';?>>
                <label class="checkbox-label" for="hard_deee"><?php _e('DEEE numérique', 'cwcud');?></label>
            </div>
            <div class="form-sub-item" style="margin-left:1.5em;">
                <label for="hard_cat_other" style="font-weight:normal"><?php _e('Autre (précisez)', 'cwcud');?></label>
                <input type="text" name="hard_cat_other" id="hard_cat_other" class="" value="<?php echo get_post_meta($id_cleanup, "hard_cat_other", true );?>">
            </div>-->
            <br>
            <p class="no-margin label-like"><?php _e('J\'ai un partenaire de collecte ?', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="radio" name="hard_collect" id="hard_orga_with_partner" class="" value="hard_orga_with_partner" onchange="onchangeHardCollect(this)" <?php checked("hard_orga_with_partner", get_post_meta($id_cleanup, "hard_collect", true ));?>>
                <label class="checkbox-label" for="hard_orga_with_partner"><?php _e('Non, 
vous retrouverez dans les guides le process pour mettre en place votre box', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="hard_collect" id="hard_no_orga" class="" value="hard_no_orga" onchange="onchangeHardCollect(this)" <?php checked("hard_no_orga", get_post_meta($id_cleanup, "hard_collect", true ));?>>
                <label class="checkbox-label" for="hard_no_orga"><?php _e('Oui', 'cwcud');?></label>
            </div>
            <div class="form-sub-item" style="margin-left:1.5em;<?php echo (get_post_meta($id_cleanup, "hard_collect", true ) == "hard_no_orga")?"":"display:none";?>" id="cont_hard_collect_other">
                <label for="hard_collect_other" style="font-weight:normal"><?php _e('Lequel ?', 'cwcud');?></label>
                <input type="text" name="hard_collect_other" id="hard_collect_other" class="" value="<?php echo get_post_meta($id_cleanup, "hard_collect_other", true );?>">
            </div>

            
            <div class="checkbox form-item">
            <input type="checkbox" name="ok_engagement_collect" id="ok_engagement_collect" required <?php if(isset($_GET['id_cleanup'])){echo'checked';}?>>
            <div class="checkbox-label">
                <label for="ok_engagement_collect" style="font-weight:bold;"><?php _e('Pour être référencé, vous vous engagez à animer votre Digital Cleanup Recyclage en prévoyant des créneaux d\'animation de votre point de collecte pour sensibiliser vos participants ', 'cwcud');?>*</label>
            </div>
        </div>

            <!--<br>
            <div id="hard_volume" style="<?php echo (get_post_meta($id_cleanup, "hard_collect", true ) == "hard_no_orga")?"display:none;":"";?>">
                <p class="no-margin label-like"><?php _e('Quel volume estimez-vous pouvoir collecter ?', 'cwcud');?></p>
                <div class="checkbox form-sub-item">
                    <input type="radio" name="hard_volume" id="hard_volume_caisse" class="" value="hard_volume_caisse" <?php checked("hard_volume_caisse", get_post_meta($id_cleanup, "hard_volume", true ));?>>
                    <label class="checkbox-label" for="hard_volume_caisse"><?php _e('Une petite caisse de rangement', 'cwcud');?></label>
                </div>
                <div class="checkbox form-sub-item">
                    <input type="radio" name="hard_volume" id="hard_volume_carton" class="" value="hard_volume_carton" <?php checked("hard_volume_carton", get_post_meta($id_cleanup, "hard_volume", true ));?>>
                    <label class="checkbox-label" for="hard_volume_caisse"><?php _e('Un carton de déménagement', 'cwcud');?></label>
                </div>
                <div class="checkbox form-sub-item">
                    <input type="radio" name="hard_volume" id="hard_volume_cartons" class="" value="hard_volume_cartons" <?php checked("hard_volume_cartons", get_post_meta($id_cleanup, "hard_volume", true ));?>>
                    <label class="checkbox-label" for="hard_volume_cartons"><?php _e('Plusieurs cartons de déménagement', 'cwcud');?></label>
                </div>
                <div class="checkbox form-sub-item">
                    <input type="radio" name="hard_volume" id="hard_volume_palette" class="" value="hard_volume_palette" <?php checked("hard_volume_palette", get_post_meta($id_cleanup, "hard_volume", true ));?>>
                    <label class="checkbox-label" for="hard_volume_palette"><?php _e('Au moins une palette', 'cwcud');?></label>
                </div>
            </div>-->
        </div>

        <label for="cleanup_name"><?php _e('Nom du Digital Cleanup *', 'cwcud'); ?></label>
        <input type="text" id="cleanup_name" name="cleanup_name" placeholder="<?php _e('Nom du Digital Cleanup', 'cwcud'); ?>"  required value="<?php echo get_post_meta($id_cleanup, "cleanup_name", true );?>">

        <label for="cleanup_description"><?php _e('Description de votre événement (Durée, déroulé, pot convivial ?) *', 'cwcud'); ?></label>
        <textarea id="cleanup_description" name="cleanup_description" rows="7" placeholder="<?php _e('Préciser le lieu, le déroulement, la durée du DigitalCleanup..', 'cwcud'); ?>"  required><?php echo get_post_meta($id_cleanup, "cleanup_description", true );?></textarea>
        <br>
        <p class="form-item no-margin label-like"><?php _e('Type de Digital Cleanup *', 'cwcud');?></p>
        <div class="form-row">
            <div class="checkbox">
                <input type="radio" required class="visibility" name="visibility" id="public" value="public"  <?php checked("public", get_post_meta($id_cleanup, "visibility", true ));?>>
                <label class="checkbox-label" for="public"><?php _e('<b>Public</b> - Visible sur le site avec la possibilité à tous les visiteurs de s’inscrire', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" class="visibility" name="visibility" id="private" value="private" <?php checked("private", get_post_meta($id_cleanup, "visibility", true ));?>>
                <label class="checkbox-label" for="private"><?php _e('<b>Privé</b>  - Visible sur le site sans la possibilité aux visiteurs de s’inscrire', 'cwcud');?></label>
            </div>
            <div class="checkbox">
                <input type="radio" class="visibility" name="visibility" id="secret" value="secret" <?php checked("secret", get_post_meta($id_cleanup, "visibility", true ));?>>
                <label class="checkbox-label" for="secret"><?php _e('<b>Secret</b>  - Non visible sur le site et sans la possibilité aux visiteurs de s’inscrire', 'cwcud');?></label>
            </div>
        </div>
        <p class="form-info form-sub-item">
            public = votre Digital Cleanup sera visible sur la carte et tout le monde pourra s'inscrire<br>
            privé = votre Digital Cleanup sera visible sur la carte mais l'inscription ne sera pas possible<br>
            secret = votre Digital Cleanup sera bien référencé mais ne sera pas visible sur la carte 
        </p>
        <label for="cleanup_tag"><?php _e('Tag du Digital Cleanup', 'cwcud'); ?></label>
        <input type="text" id="cleanup_tag" name="cleanup_tag" placeholder="<?php _e('#nomdelastructure', 'cwcud'); ?>" value="<?php echo get_post_meta($id_cleanup, "cleanup_tag", true );?>">
        <p class="form-info form-sub-item">
            Le tag vous permet de rassembler plusieurs Digital Cleanups sous un meme nom, aka meme entreprise ou meme type d'événement
        </p>
        <div class="form-item" >
            <label for="date_start"><?php _e('Début du Digital Cleanup *', 'cwcud'); ?></label>
            <input type="date" id="date_start" name="date_start" min="<?php the_field("date_start_event", "option");?>" max="<?php the_field("date_end_event", "option");?>" required value="<?php echo get_post_meta($id_cleanup, "date_start", true );?>">
        </div>
        <div class="form-item" >
            <label for="time_start"><?php _e('Heure de début du Digital Cleanup *', 'cwcud'); ?></label>
            <input type="time" id="time_start" step="" name="time_start" required value="<?php echo get_post_meta($id_cleanup, "time_start", true );?>">
            

        </div>
        
        <div class="form-item" >
            <label for="time_end"><?php _e('Durée de l’atelier *', 'cwcud'); ?></label>
            <div class="select">
                <select name="time_end" id="time_end" required>
                    <?php
                    $t = get_during();
                    foreach ($t as $value):?>
                        <option value="<?php echo $value;?>" <?php selected($value, get_post_meta($id_cleanup, "time_end", true ));?>><?php echo $value;?></option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
        <div class="form-item" >
            <label for="during_day"><?php _e('S\'étend sur plusieurs jours ?', 'cwcud'); ?></label>
            <div class="select">
                <select name="during_day" id="during_day" >
                <option value="" ></option>
                    <?php
                    for($i=1;$i<=7;$i++):?>
                        <option value="<?php echo $i;?>" <?php selected($i, get_post_meta($id_cleanup, "during_day", true ));?>><?php echo $i.' '; _e('jour(s)','cwcud');?></option>
                    <?php endfor;?>
                </select>
            </div>
        </div>

        <!--<p>
            <div id="nb_participant_max_container" style="<?php echo (get_post_meta($id_cleanup, "visibility", true ) != "public")?'display:none':'';?>">
                <label for="nb_participant_max"><?php _e('Nombre maximum de participants :', 'cwcud'); ?></label>
                <input type="number" min="1" id="nb_participant_max" name="nb_participant_max" placeholder="<?php _e('Laisser vide si Illimité', 'cwcud'); ?>"  value="<?php echo get_post_meta($id_cleanup, "nb_participant_max", true );?>" >
                <p class="form-info form-sub-item"><?php _e('Laissez le champ vide pour un nombre illimité de participants.', 'cwcud');?></p>
            </div>
            <div id="nb_participant_target_max_container" style="<?php echo (get_post_meta($id_cleanup, "visibility", true ) == "public")?'display:none':'';?>">
                <label for="nb_participant_target"><?php _e('Nombre de personnes ciblées ou estimées :', 'cwcud'); ?></label>
                <input type="number" min="1" id="nb_participant_target" name="nb_participant_target" placeholder=""  value="<?php echo get_post_meta($id_cleanup, "nb_participant_target", true );?>" >
            </div>
        </p>-->

        <div id="place_cleanup">
            <p class="form-item no-margin label-like cat_a cat_b"><?php _e('Votre Digital Cleanup est-il en *', 'cwcud');?></p>
            <p class="form-item no-margin label-like"><?php _e('Où se situera votre Digital Cleanup ? *', 'cwcud');?></p>
            <script>
                document.addEventListener('DOMContentLoaded', function() {
                    onchangeCatCWCUD();
                });
            </script>
            <div class="checkbox form-sub-item cat_a cat_b">    
                <input type="radio" name="location" id="location_facetoface" class="location" value="location_facetoface" onclick="displayInformations()" <?php checked("location_facetoface", get_post_meta($id_cleanup, "location", true ));?>>
                <label class="checkbox-label" for="location_facetoface"><?php _e('Présentiel', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item  cat_a cat_b">   
                <input type="radio" name="location" id="location_distancing" class="location" value="location_distancing" onclick="displayInformations()" <?php checked("location_distancing", get_post_meta($id_cleanup, "location", true ));?>>
                <label class="checkbox-label" for="location_distancing"><?php _e('Distanciel', 'cwcud');?></label>
            </div>
            <p class="form-info form-sub-item cat_a cat_b"><?php _e("Les aspects logistiques (Salle, visioconférence, …) sont à la charge de l’organisateur.", 'cwcud');?></p>
            
            <div class="form-item" id="cleanup_adresse" >
                <label for="cleanup_adresse_id"><?php _e('Adresse du Cleanup :', 'cwcud'); ?>*</label>
                <input type="text" name="cleanup_adresse" id="cleanup_adresse_id" placeholder="<?php _e('4 boulevard de Lafayette, 76100 Rouen', 'cwcud'); ?>" required value="<?php echo get_post_meta($id_cleanup, "cleanup_adresse", true );?>">
                <input type="hidden" name="coordonate" id="coordonate" value="<?php echo get_post_meta($id_cleanup, "coordonate", true );?>">
            </div>
            <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6Mw5zx6630K1-bj76ffwABUmOALfsGwE&libraries=places&callback=autocompleteMap">
            </script>
            <script>
                let autocomplete;
                function autocompleteMap() {
                    const center = { lat: 46.227638, lng: 2.213749 };
                    const input = document.getElementById("cleanup_adresse_id");
                    const options = {
                        //componentRestrictions: { country: ['fr','ch','be', 'fr', 'gp', 'mq', 'gf', 're', 'pm', 'yt', 'nc', 'pf', 'mf', 'tf'] },
                        fields: ["geometry"],
                        strictBounds: false,
                        types: ["address"],
                    };
                    autocomplete = new google.maps.places.Autocomplete(input, options);
                    autocomplete.addListener("place_changed", fillInAddress);
                }

// [START maps_places_autocomplete_addressform_fillform]
function fillInAddress() {
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    document.getElementById('coordonate').value = lat+','+lng;
    
  }
            </script>
            
            <div class="form-item cat_a cat_b" id="link_connection" style="display:none">
                <label for="link_connection"><?php _e('Lien de connexion :', 'cwcud'); ?></label>
                <input type="url" name="link_connection" placeholder="<?php _e('https://www.visioconference.com', 'cwcud'); ?>" value="<?php echo get_post_meta($id_cleanup, "link_connection", true );?>">
                <p class="form-info form-sub-item"><?php _e('Si vous ne connaissez le lien, vous pourrez le renseigner dans un second temps', 'cwcud');?></p>
            </div>

            <div class="form-item" id="add_info" style="">
                <label for="more_information"><?php _e('Info complémentaire (salle ou batiment)', 'cwcud'); ?></label>
                <input type="text" id="more_information" name="more_information" placeholder="<?php _e('Info complémentaire', 'cwcud'); ?>" value="<?php echo get_post_meta($id_cleanup, "more_information", true );?>">
                <p class="form-info form-sub-item"><?php _e('Lieu précis de positionnement de votre point de collecte)', 'cwcud');?></p>
            </div>

        </div>

        <div class="form-item" >
            <label for="cleanup_area"><?php _e('Département du Digital Cleanup :', 'cwcud'); ?>*</label>
            <div class="select">
                <?php
        			select_geo_area(get_post_meta($id_cleanup, "cleanup_area", true ), 'cleanup_area');    
				?>
            </div>
        </div>

        <p></p><br>
        <h2 class="ctr no-margin"><?php _e('Communication', 'cwcud'); ?></h2>
        
        <div class="form-item " id="link_sign_in">
            <label for="link_sign_in_id"><?php _e("Si vous pensez créer un formulaire d'inscription pour votre Digital Cleanup, précisez le lien ici (url) :", 'cwcud'); ?></label>
            <input type="url" name="link_sign_in" id="link_sign_in_id" placeholder="" value="<?php echo get_post_meta($id_cleanup, "link_sign_in", true );?>">
        </div>
        <div class="form-item " id="link_channel">
            <label for="link_channel_id"><?php _e("Lien vers votre canal de communication ? page web ? ou autre ? (url) :", 'cwcud'); ?></label>
            <input type="url" id="link_channel_id" name="link_channel" placeholder="" value="<?php echo get_post_meta($id_cleanup, "link_channel", true );?>">
            <p class="form-info form-sub-item"><?php _e("Ces infos s'afficheront sur la page de votre Digital Cleanup, vous pourrez les modifier ou les ajouter à postériori via votre Compte", 'cwcud');?></p>
            <p class="form-info form-sub-item"><?php _e("À la validation de votre Digital Cleanup, vous recevrez un lien url sur votre Compte : n'hésitez pas à le partager sur vos réseaux sociaux afin de faire la promotion de votre Digital Cleanup.", 'cwcud');?></p>
        </div>
        
        
        <p></p>
        <p> <h2 class="ctr no-margin"><?php _e('Sensibilisation et Responsabilité', 'cwcud'); ?></h2>
            <p class="form-info form-sub-item"><?php _e("La sensibilisation est une partie importante du dispositif Digital Cleanup Day, il est essentiel que les participants comprennent pourquoi ils participent à cette opération et quels sont les impacts du numérique sur notre société.", 'cwcud');?></p>
        </p>
        
        <div class="checkbox form-sub-item">
            <input type="checkbox" name="orga1" id="orga1" class="" value="1" required <?php if(isset($_GET['id_cleanup'])){echo'checked';}?>>
            <label class="checkbox-label" for="orga1"><?php _e('Je m\'engage à sensibiliser lors du Digital Cleanup que j\'organise', 'cwcud');?>*</label>
        </div>
        <div class="form-sub-item">
            <p class="form-info form-sub-item"><?php _e("Vous retrouvez via Mon Compte tous les éléments pour vous former et les outils pour sensibiliser lors de votre Digital Cleanup.", 'cwcud');?></p>
        </div>

        <div class="checkbox form-sub-item">
            <input type="checkbox" name="orga2" id="orga2" class="" value="1" required  <?php if(isset($_GET['id_cleanup'])){echo'checked';}?>>
            <label class="checkbox-label" for="orga2"><?php _e('Je suis responsable de l\'organisation de mon Digital Cleanup et des messages que je transmets', 'cwcud');?>*</label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="checkbox" name="orga3" id="orga3" class="" value="1" required  <?php if(isset($_GET['id_cleanup'])){echo'checked';}?>>
            <label class="checkbox-label" for="orga3"><?php _e("Je m'engage à remonter mon bilan auprès de l'équipe organisatrice du Digital Cleanup Day après l'opération.", 'cwcud');?>*</label>
        </div>
        <p class="form-info form-sub-item"><?php _e("Ce bilan à compléter sera disponible via Mon Compte pour chaque Digital Cleanup.", 'cwcud');?></p>
        
        <p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

        <!-- Button submit -->
        <button class="form-item  button" type="submit" id="sendMessage">
            <?php
            if(isset($_GET['id_cleanup'])){
                _e('Mettre à jour le Digital Cleanup', 'cwcud'); 
            }else{
                _e('Créer le Digital Cleanup', 'cwcud'); 
            }
            ?>
        </button>
        <p></p>
    </form>
</section>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
?>
