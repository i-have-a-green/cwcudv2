<?php
/*
Template Name: Bilan CleanUp
*/
//$current_user = wp_get_current_user();

?>

<?php get_header("membre"); ?> 

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<!--<h2 class="big"><?php _e('Organiser un Digital Cleanup', 'cwcud');?></h2>-->
    </div>
</header>
        
<?php
$id_cleanup = false;
if(isset($_GET['id_cleanup'])){
    $id_cleanup = $_GET['id_cleanup'];
}
?>
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<section class="wrapper">
    <form action="" method="post"  name="bilanForm" id="bilanForm" class="form-style">
        <input type="hidden" name="honeyPot" value="">
        <input type="hidden" name="id_cleanup" value="<?php echo $id_cleanup;?>">
        <p><?php _e("Félicitations ! Votre opération Digital Cleanup est terminée, vous pouvez à présent nous faire remonter vos chiffres récoltés.
Ce formulaire est disponible et modifiable avec votre compte connecté à tout moment jusqu'au 27 mars 2022.", "cwcud");?></p>
        <?php if("data" == get_post_meta($id_cleanup, "cat_cybercleanup", true )): ?>
            <p><h2 class="ctr no-margin"><?php _e('Digital Cleanup Données', 'cwcud');?></h2><p>
            <p><?php _e("Après avoir rassemblé les chiffres de toutes vos données nettoyées auprès de vos participantes & participants de votre Digital Cleanup Données, veuillez remplir les champs suivant afin de participer au bilan national de Digital Cleanup Day en France !", "cwcud");?></p>

            <p><h2 class="ctr no-margin"><?php _e('E-mails', 'cwcud');?></h2><p>

            <label for="nb_delete_email"><?php _e("Nombre d'e-mails supprimés", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_email" name="nb_delete_email" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_email", true );?>">

            <label for="weight_delete_email"><?php _e("Poids total des e-mails supprimés", 'cwcud'); ?></label>
            <input type="number" class="other_required" data-required="unit_weight_delete_email" min="0" step="0.01" id="weight_delete_email" name="weight_delete_email" placeholder="" value="<?php echo get_post_meta($id_cleanup, "weight_delete_email", true );?>">

            <div class="" id="unit_weight_delete_email_container">
                <label for="unit_weight_delete_email"><?php _e('Unité de taille : ', 'cwcud'); ?></label>
                <div class="select">
                    <select name="unit_weight_delete_email" id="unit_weight_delete_email" >
                        <option value="" ></option>
                        <option value="Ko" <?php selected("Ko", get_post_meta($id_cleanup, "unit_weight_delete_email", true ));?>>Ko</option>
                        <option value="Mo" <?php selected("Mo", get_post_meta($id_cleanup, "unit_weight_delete_email", true ));?>>Mo</option>
                        <option value="Go" <?php selected("Go", get_post_meta($id_cleanup, "unit_weight_delete_email", true ));?>>Go</option>
                        <option value="To" <?php selected("To", get_post_meta($id_cleanup, "unit_weight_delete_email", true ));?>>To</option>
                    </select>
                </div>
            </div>

            <p><h2 class="ctr no-margin"><?php _e('Réseaux sociaux', 'cwcud');?></h2><p>

            <label for="nb_delete_post_social"><?php _e("Nombre de publications sur les réseaux sociaux supprimées", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_post_social" name="nb_delete_post_social" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_post_social", true );?>">
            
            <p><h2 class="ctr no-margin"><?php _e('Fichiers cloud (vos drives)', 'cwcud');?></h2><p>

            <label for="nb_delete_file_cloud"><?php _e("Nombre de fichiers supprimés sur un cloud", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_file_cloud" name="nb_delete_file_cloud" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_file_cloud", true );?>">
            
            <label for="weight_delete_file_cloud"><?php _e("Poids total des fichiers supprimés sur un cloud", 'cwcud'); ?></label>
            <input type="number" class="other_required" data-required="unit_weight_delete_file_cloud" min="0" step="0.01" id="weight_delete_file_cloud" name="weight_delete_file_cloud" placeholder="" value="<?php echo get_post_meta($id_cleanup, "weight_delete_file_cloud", true );?>">

            <div class="" id="unit_weight_delete_file_cloud_container">
                <label for="unit_weight_delete_file_cloud"><?php _e('Unité de taille : ', 'cwcud'); ?></label>
                <div class="select">
                    <select name="unit_weight_delete_file_cloud" id="unit_weight_delete_file_cloud" >
                        <option value="" ></option>
                        <option value="Ko" <?php selected("Ko", get_post_meta($id_cleanup, "unit_weight_delete_file_cloud", true ));?>>Ko</option>
                        <option value="Mo" <?php selected("Mo", get_post_meta($id_cleanup, "unit_weight_delete_file_cloud", true ));?>>Mo</option>
                        <option value="Go" <?php selected("Go", get_post_meta($id_cleanup, "unit_weight_delete_file_cloud", true ));?>>Go</option>
                        <option value="To" <?php selected("To", get_post_meta($id_cleanup, "unit_weight_delete_file_cloud", true ));?>>To</option>
                    </select>
                </div>
            </div>

            <p><h2 class="ctr no-margin"><?php _e('Fichiers sur un ordinateur fixe', 'cwcud');?></h2><p>

            <label for="nb_delete_file_pc"><?php _e("Nombre de fichiers supprimés sur ordinateur fixe", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_file_pc" name="nb_delete_file_pc" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_file_pc", true );?>">
            
            <label for="weight_delete_file_pc"><?php _e("Poids total des fichiers supprimés sur un ordinateur fixe", 'cwcud'); ?></label>
            <input type="number" class="other_required" data-required="unit_weight_delete_file_pc" min="0" step="0.01" id="weight_delete_file_pc" name="weight_delete_file_pc" placeholder="" value="<?php echo get_post_meta($id_cleanup, "weight_delete_file_pc", true );?>">

            <div class="" id="unit_weight_delete_file_pc">
                <label for="unit_weight_delete_file_pc"><?php _e('Unité de taille : ', 'cwcud'); ?></label>
                <div class="select">
                    <select name="unit_weight_delete_file_pc" id="unit_weight_delete_file_pc" >
                        <option value="" ></option>
                        <option value="Ko" <?php selected("Ko", get_post_meta($id_cleanup, "unit_weight_delete_file_pc", true ));?>>Ko</option>
                        <option value="Mo" <?php selected("Mo", get_post_meta($id_cleanup, "unit_weight_delete_file_pc", true ));?>>Mo</option>
                        <option value="Go" <?php selected("Go", get_post_meta($id_cleanup, "unit_weight_delete_file_pc", true ));?>>Go</option>
                        <option value="To" <?php selected("To", get_post_meta($id_cleanup, "unit_weight_delete_file_pc", true ));?>>To</option>
                    </select>
                </div>
            </div>

            <p><h2 class="ctr no-margin"><?php _e('Fichiers sur un ordinateur portable', 'cwcud');?></h2><p>

            <label for="nb_delete_file_laptop"><?php _e("Nombre de fichiers supprimés sur ordinateur portable", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_file_laptop" name="nb_delete_file_laptop" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_file_laptop", true );?>">
            
            <label for="weight_delete_file_laptop"><?php _e("Poids total des fichiers supprimés sur un ordinateur portable", 'cwcud'); ?></label>
            <input type="number" class="other_required" data-required="unit_weight_delete_file_laptop" min="0" step="0.01" id="weight_delete_file_laptop" name="weight_delete_file_laptop" placeholder="" value="<?php echo get_post_meta($id_cleanup, "weight_delete_file_laptop", true );?>">

            <div class="" id="unit_weight_delete_file_laptop_container">
                <label for="unit_weight_delete_file_laptop"><?php _e('Unité de taille : ', 'cwcud'); ?></label>
                <div class="select">
                    <select name="unit_weight_delete_file_laptop" id="unit_weight_delete_file_laptop" >
                        <option value="" ></option>
                        <option value="Ko" <?php selected("Ko", get_post_meta($id_cleanup, "unit_weight_delete_file_laptop", true ));?>>Ko</option>
                        <option value="Mo" <?php selected("Mo", get_post_meta($id_cleanup, "unit_weight_delete_file_laptop", true ));?>>Mo</option>
                        <option value="Go" <?php selected("Go", get_post_meta($id_cleanup, "unit_weight_delete_file_laptop", true ));?>>Go</option>
                        <option value="To" <?php selected("To", get_post_meta($id_cleanup, "unit_weight_delete_file_laptop", true ));?>>To</option>
                    </select>
                </div>
            </div>

            <p><h2 class="ctr no-margin"><?php _e('Fichiers et applications sur un smartphone', 'cwcud');?></h2><p>

            <label for="nb_delete_file_smartphone"><?php _e("Nombre d'applications nettoyées ou supprimées sur mon smartphone", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_file_smartphone" name="nb_delete_file_smartphone" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_file_smartphone", true );?>">
            
            <label for="weight_delete_file_smartphone"><?php _e("Poids total des données supprimées sur smartphone", 'cwcud'); ?></label>
            <input type="number" class="other_required" data-required="unit_weight_delete_file_smartphone" min="0" step="0.01" id="weight_delete_file_smartphone" name="weight_delete_file_smartphone" placeholder="" value="<?php echo get_post_meta($id_cleanup, "weight_delete_file_smartphone", true );?>">

            <div class="" id="unit_weight_delete_file_smartphone_container">
                <label for="unit_weight_delete_file_smartphone"><?php _e('Unité de taille : ', 'cwcud'); ?></label>
                <div class="select">
                    <select name="unit_weight_delete_file_smartphone" id="unit_weight_delete_file_smartphone" >
                        <option value="" ></option>
                        <option value="Ko" <?php selected("Ko", get_post_meta($id_cleanup, "unit_weight_delete_file_smartphone", true ));?>>Ko</option>
                        <option value="Mo" <?php selected("Mo", get_post_meta($id_cleanup, "unit_weight_delete_file_smartphone", true ));?>>Mo</option>
                        <option value="Go" <?php selected("Go", get_post_meta($id_cleanup, "unit_weight_delete_file_smartphone", true ));?>>Go</option>
                        <option value="To" <?php selected("To", get_post_meta($id_cleanup, "unit_weight_delete_file_smartphone", true ));?>>To</option>
                    </select>
                </div>
            </div>

            <p><h2 class="ctr no-margin"><?php _e('Fichiers et applications sur une tablette', 'cwcud');?></h2><p>

            <label for="nb_delete_file_tablet"><?php _e("Nombre d'applications nettoyées ou supprimées sur ma tablette ", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_delete_file_tablet" name="nb_delete_file_tablet" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_delete_file_tablet", true );?>">
            
            <label for="weight_delete_file_tablet"><?php _e("Poids total des données supprimées sur smartphone", 'cwcud'); ?></label>
            <input type="number" class="other_required" data-required="unit_weight_delete_file_tablet" min="0" step="0.01" id="weight_delete_file_tablet" name="weight_delete_file_tablet" placeholder="" value="<?php echo get_post_meta($id_cleanup, "weight_delete_file_tablet", true );?>">

            <div class="" id="unit_weight_delete_file_tablet_container">
                <label for="unit_weight_delete_file_tablet"><?php _e('Unité de taille : ', 'cwcud'); ?></label>
                <div class="select">
                    <select name="unit_weight_delete_file_tablet" id="unit_weight_delete_file_tablet" >
                        <option value="" ></option>
                        <option value="Ko" <?php selected("Ko", get_post_meta($id_cleanup, "unit_weight_delete_file_tablet", true ));?>>Ko</option>
                        <option value="Mo" <?php selected("Mo", get_post_meta($id_cleanup, "unit_weight_delete_file_tablet", true ));?>>Mo</option>
                        <option value="Go" <?php selected("Go", get_post_meta($id_cleanup, "unit_weight_delete_file_tablet", true ));?>>Go</option>
                        <option value="To" <?php selected("To", get_post_meta($id_cleanup, "unit_weight_delete_file_tablet", true ));?>>To</option>
                    </select>
                </div>
            </div>

        <?php elseif("hardware" == get_post_meta($id_cleanup, "cat_cybercleanup", true ) ): ?>
            <p><h2 class="ctr no-margin"><?php _e('Digital Cleanup Equipements', 'cwcud');?></h2><p>
            <p><?php _e("Après avoir rassemblé les chiffres de tous vos équipements récoltés lors de vos opérations de collectes de votre Digital Cleanup Equipements, veuillez remplir les champs suivant afin de participer au bilan national de Digital Cleanup Day en France !", "cwcud");?></p>
        
            <p><h2 class="ctr no-margin"><?php _e('Ordinateurs fixes', 'cwcud');?></h2><p>

            <label for="nb_computer_ok"><?php _e("Nombre d'ordinateurs fixes collectés fonctionnels", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_computer_ok" name="nb_computer_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_computer_ok", true );?>">
            <p class="form-info form-sub-item"><?php _e("Comptez aussi ceux que vous avez pu réparer", 'cwcud');?></p>

            <label for="nb_computer_not_ok"><?php _e("Nombre d'ordinateurs fixes collectés NON fonctionnels", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_computer_not_ok" name="nb_computer_not_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_computer_not_ok", true );?>">

            <p><h2 class="ctr no-margin"><?php _e('Ordinateurs portables', 'cwcud');?></h2><p>

            <label for="nb_laptop_ok"><?php _e("Nombre d'ordinateurs portables collectés fonctionnels", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_laptop_ok" name="nb_laptop_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_laptop_ok", true );?>">
            <p class="form-info form-sub-item"><?php _e("Comptez aussi ceux que vous avez pu réparer", 'cwcud');?></p>

            <label for="nb_laptop_not_ok"><?php _e("Nombre d'ordinateurs portables collectés NON fonctionnels", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_laptop_not_ok" name="nb_laptop_not_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_laptop_not_ok", true );?>">

            <p><h2 class="ctr no-margin"><?php _e('Tablettes', 'cwcud');?></h2><p>

            <label for="nb_tablet_ok"><?php _e("Nombre de talettes collectées fonctionnelles", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_tablet_ok" name="nb_tablet_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_tablet_ok", true );?>">
            <p class="form-info form-sub-item"><?php _e("Comptez aussi ceux que vous avez pu réparer", 'cwcud');?></p>

            <label for="nb_tablet_not_ok"><?php _e("Nombre de talettes collectées NON fonctionnelles", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_tablet_not_ok" name="nb_tablet_not_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_tablet_not_ok", true );?>">

            <p><h2 class="ctr no-margin"><?php _e('Smartphones', 'cwcud');?></h2><p>

            <label for="nb_smartphone_ok"><?php _e("Nombre de smartphones collectés fonctionnels", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_smartphone_ok" name="nb_smartphone_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_smartphone_ok", true );?>">
            <p class="form-info form-sub-item"><?php _e("Comptez aussi ceux que vous avez pu réparer", 'cwcud');?></p>

            <label for="nb_smartphone_not_ok"><?php _e("Nombre de smartphones collectés NON fonctionnels", 'cwcud'); ?></label>
            <input type="number" min="0" step="1" id="nb_smartphone_not_ok" name="nb_smartphone_not_ok" placeholder="" value="<?php echo get_post_meta($id_cleanup, "nb_smartphone_not_ok", true );?>">

        <?php endif;?>
        <p><h2 class="ctr no-margin"><?php _e('Retour d\'expérience', 'cwcud');?></h2><p>
        

        <label for="comment_bilan"><?php _e('Transmettez nous votre retour d\'expérience !', 'cwcud'); ?></label>
        <textarea id="comment_bilan" name="comment_bilan" rows="7" placeholder="" ><?php echo get_post_meta($id_cleanup, "comment_bilan", true );?></textarea>
        <p>
            <button class="form-item  button" type="submit" id="bilanFormSubmit">
            <?php _e("Enregistrer le bilan","cwcud");?>
            </button>
        </p>
        <p id="submit-ok" style="display:none">
            <?php _e("Vos informations ont bien été prises en compte.","cwcud");?><br>
            <a href="<?php echo get_permalink( get_post(get_field("page_list_cleanup", "option")));?>"><?php _e("Retour à mes DigitalCleanupDay", "cwcud");?></a>
        </p>
    </form>
</section>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
?>
