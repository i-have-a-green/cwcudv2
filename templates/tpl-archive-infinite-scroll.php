<?php
/*
Template Name: tpl archive
*/
?>

<?php get_header(); ?>

<!-- Header -->
<header class="page-title">
	<?php wpBreadcrumb(); ?>
	<?php the_title('<h1 class="center">', '</h1>'); ?>
</header>
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<!-- Listing Archive -->
<section>
	
	<!-- wrapper -->
	
	<?php

	$arg = array(
		'posts_per_page'    => get_option('posts_per_page' ),
		'post_status'       => 'publish',
		'post_type'         => 'post',
	);

	//If get_post_type() = training
	/*if(isset($_GET['var_taxo_tag']) && !empty($_GET['var_taxo_tag']) ) {
		$arg['tax_query']  = array(
			array(
				'taxonomy' => 'taxo_tag',
				'field'    => 'term_id',
				'terms'    => $_GET['var_taxo_tag'],
			)
		);
	}*/

	$posts = get_posts( $arg );

	// <!-- pour le scroll -->
	$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);

	if($posts):
	?>
	<div class="listing-archive wrapper v-padding-small"
		data-cpt=<?php echo $cpt; ?>
		data-page="<?php echo $num_page;?>"
		data-url="<?php echo get_the_permalink();?>"
		data-taxo=""
		data-taxo_tag="<?php if(isset($_GET['var_taxo_tag'])): echo $_GET['var_taxo_tag']; endif;?>"
		id="infinite-list">
	
		<?php foreach($posts as $post): 
		
			setup_postdata( $post );

			get_template_part( 'template-parts/archive', get_post_type() );

		endforeach; 

		wp_reset_postdata();
	else :

		get_template_part( 'template-parts/content', 'none' );

	endif;

	?>

	</div><!-- /wrapper -->

</section><!-- End of Listing Archive -->

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();
