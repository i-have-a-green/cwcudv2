<?php
/*
Template Name: Listing ambassadeur
*/
?>

<?php get_header("membre"); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>

	<!-- FILTERS -->
	<section class="wrapper"> 
		
		<form method="GET" class="filters-embassy">
			<!-- Filters -->
			<div class="filters">
				<!-- par départements -->
				<div class="select">
					<?php 
					$current = (isset($_GET['s_dep'])) ? $_GET["s_dep"] : "";
					select_geo_area($current, 's_dep');    ?>
					
				</div>

				<!-- par level-ambassadeur -->
				<!--<div class="select">
					<select name="s_niv_amb" id="">
						<option value=""><?php _e('Tous les ambassadeurs','cwcud');?></option>
						<option value="local" <?php selected( "local", (isset($_GET['s_niv_amb']) ? $_GET["s_niv_amb"] : "")); ?>><?php _e('Niveau local', 'cwcud');?></option>
						<option value="departemental" <?php selected( "departemental", (isset($_GET['s_niv_amb']) ? $_GET["s_niv_amb"] : "")); ?>><?php _e('Niveau départemental', 'cwcud');?></option>
						<option value="regional" <?php selected( "regional", (isset($_GET['s_niv_amb']) ? $_GET["s_niv_amb"] : "")); ?>><?php _e('Niveau régional', 'cwcud');?></option>
						<option value="national" <?php selected( "national", (isset($_GET['s_niv_amb']) ? $_GET["s_niv_amb"] : "")); ?>><?php _e('Niveau national', 'cwcud');?></option>
					</select>
				</div>-->

				<!-- search -->
				<input type="text" placeholder="<?php _e('Rechercher un ambassadeur', 'cwcud');?>" name="s_ambassadeur" value="<?php echo (isset($_GET['s_ambassadeur']) ? $_GET["s_ambassadeur"] : "");?>">
			</div>

			<!-- btn -->
			<input type="submit" class="button" value="<?php _e('Rechercher','cwcud');?>">

		</form>

	</section>


	<!-- Listing Archive -->
	<section>
		
		<!-- wrapper -->
		
		<?php
		$a = array();
		$a['role'] = 'ambassadeur';
		if(isset($_GET['s_dep']) && !empty($_GET['s_dep']) && isset($_GET['s_niv_amb']) && !empty($_GET['s_niv_amb'])){
			$a['meta_query'] = array(
				'relation' => 'AND',
				array(
					'key' => 'user_departement',
					'value' => sanitize_text_field($_GET['s_dep']),
					'compare' => '='
				),
				array(
					'key' => 'level_ambassadeur',
					'value' => sanitize_text_field($_GET['s_niv_amb']),
					'compare' => '='
				)
			);
		}
		else if(isset($_GET['s_dep']) && !empty($_GET['s_dep'])){
			$a['meta_key'] = 'user_geo_area';
			$a['meta_value'] = sanitize_text_field($_GET['s_dep']);
		}
		else if(isset($_GET['s_niv_amb']) && !empty($_GET['s_niv_amb'])){
			$a['meta_key'] = 'level_ambassadeur';
			$a['meta_value'] = sanitize_text_field($_GET['s_niv_amb']);
		}

		
		if(isset($_GET['s_ambassadeur']) && !empty($_GET['s_ambassadeur'])){
			$a['search'] = '*'.sanitize_text_field($_GET['s_ambassadeur']).'*';
			/*$a['search_columns'] = array(
				'user_login',
				'user_nicename',
				'user_email',
				'user_url',
				'first_name',
				'last_name',
			);*/
		}

		//var_dump($a);
		//https://developer.wordpress.org/reference/classes/wp_user_query/
		$users = get_users($a);
		if(!$users):
			$a = array();
			$a['role'] = 'ambassadeur';
			$a['meta_key'] = 'level_ambassadeur';
			$a['meta_value'] = 'national';
			$users = get_users($a);
			echo "<p>Il n'y a pas d'ambassadeur correspondant à votre recherche, voici les ambassadeurs nationaux :</p>";
		endif;

		// <!-- pour le scroll -->
		//$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);

		if($users):?>
			<div class="listing listing-embassy">
			
				<?php foreach($users as $user):?>
					
					<article class="embassy-card">
						<!-- Structure -->
						<h2 class="embassy-structure">
							<?php echo get_user_meta($user->ID, 'structure_name', true );?>	
						</h2>

						<!-- Nom et prénom - Ambassadeur  -->
						<p class="embassy-name">
							<?php echo $user->first_name.' '.$user->last_name;?>	
						</p>
																				
						<!-- Niveau ambassadeur -->
						<p class="embassy-level">
							<?php if (get_user_meta($user->ID, 'level_ambassadeur', true ) === 'regional'):
								echo 'Ambassadeur régional';
							elseif (get_user_meta($user->ID, 'level_ambassadeur', true ) === 'departemental'):
								echo 'Ambassadeur départemental';
							elseif (get_user_meta($user->ID, 'level_ambassadeur', true ) === 'local'):
								echo 'Ambassadeur local';
							elseif (get_user_meta($user->ID, 'level_ambassadeur', true ) === 'national'):
								echo 'Ambassadeur national';
							endif;?>	
						</p>
						
						<!-- Département -->
						
						<p>
							<?php _e('Zone géographique : ', 'cwcud');
							
							
							echo get_geo_area(get_user_meta($user->ID, 'user_geo_area', true ));
							
							?>
						</p>
						
						
						<!-- Code postal -->
						<p>
							<?php _e('Code postal : ', 'cwcud');
							echo get_user_meta($user->ID, 'user_code_postal', true );?>
						</p>							

						<!-- Ville -->
						<p>
							<?php _e('Ville : ', 'cwcud');
							echo get_user_meta($user->ID, 'user_city', true );?>
						</p>

						<!-- Type -->
						<p>
							<?php 
								_e('Type : ', 'cwcud');
								if (get_user_meta($user->ID, 'type_structure', true ) === 'citizen'):
									_e('Citoyen','cwcud');
								elseif (get_user_meta($user->ID, 'type_structure', true ) === 'association'):
									_e('Association','cwcud');
								elseif (get_user_meta($user->ID, 'type_structure', true ) === 'school'):
									_e('École','cwcud');
								elseif (get_user_meta($user->ID, 'type_structure', true ) === 'collectivity'):
									_e('Collectivité','cwcud');
								elseif (get_user_meta($user->ID, 'type_structure', true ) === 'company'):
									_e('Entreprise','cwcud');
								endif;
							?>
						</p>

						<button class="button-yellow get_modal_ambassadeur" data-id="<?php echo $user->ID;?>"><?php _e('Contacter', 'cwcud');?></button>

					</article>
			
				<?php
				endforeach; 
				?>
				<div id="modal_ambassadeur" class="modale"></div>
			</div>
		<?php else :

			//get_template_part( 'template-parts/content', 'none' );

		endif;

		?>

	</section><!-- End of Listing Archive -->
</main>
<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); 

?>
