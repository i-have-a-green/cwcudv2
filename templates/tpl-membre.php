<?php
/*
Template Name: Membre connecté
*/
//$current_user = wp_get_current_user();
get_header('membre'); ?> 

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Espace membre', 'cwcud');?></h2>
    </div>
</header>
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <main id="raw-content">
        <?php the_content(); ?>
    </main>


<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
?>
