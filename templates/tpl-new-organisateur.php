<?php
/*
Template Name: Inscription membre
*/

if ( is_user_logged_in() ) :
    wp_redirect( get_permalink(get_field("page_my_account", "options")), 302);
endif;

?>

<?php get_header(); ?>

<!-- Header --> 
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Compte Digital Cleanup Day', 'cwcud');?></h2>
    </div>
</header>

<main id="raw-content">
	<?php the_content(); ?>
</main>

        
<!-- Begining of the loop -->
<?php //if (have_posts()) : while (have_posts()) : the_post(); ?>

<section class="wrapper">
    <div class="form-container ">
        <h2 style="text-align:center"><?php _e("J'ai déjà un compte Digital Cleanup Day","cwcud");?></h2>
        <?php wp_login_form(
            array(
                'remember'       => false,
                'label_username' => __( 'Identifiant', 'cwcud' ),
                'label_password' => __( 'Mot de passe', 'cwcud' ),
                //'remember'		 => true,
                'redirect'       => get_permalink(get_field("page_member", "options")),
            )
        );?>
        <a class="lost-pswd" href="<?php echo wp_lostpassword_url(get_home_url()) ; ?>"><?php _e("Mot de passe oublié ?", "cwcud"); ?></a>
    </div>
    <h2 style="text-align:center"><?php _e("Je crée un compte Digital Cleanup Day","cwcud");?></h2>
    <form action="" method="post"  name="organisateurForm" id="organisateurForm" class="form-style">
        
        <input type="hidden" name="honeyPot" value="">
    
        <!--<div class="form-item">
            <p class="no-margin label-like"><?php _e('Civilité *', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="radio" name="user_gender" id="user_female" value="F" checked>
                <label class="checkbox-label" for="user_female"><?php _e('Madame', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="user_gender" id="user_male" value="M">
                <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
            </div>
        </div>-->

        <label for="user_lastname"><?php _e('Nom *', 'cwcud'); ?></label>
        <input type="text" id="user_lastname" name="user_lastname" placeholder="Dubois" required>

        <label for="user_firstname"><?php _e('Prénom *', 'cwcud'); ?></label>
        <input type="text" id="user_firstname" name="user_firstname" placeholder="Véronique" required>

        <!--<label for="user_phone"><?php _e('Téléphone *', 'cwcud'); ?></label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required>
        <p class="form-info form-sub-item"><?php _e("Ce numéro est destiné à l'équipe projet pour vous contacter plus rapidement", "cwcud");?></p>-->

        <label for="user_email"><?php _e('Adresse mail *', 'cwcud'); ?></label>
        <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required>
        <p class="form-info form-sub-item"><?php _e('* Votre adresse mail servira d\'identifiant pour votre compte.', 'cwcud');?></p>

        <label for="user_pwd"><?php _e('Mot de passe *', 'cwcud'); ?></label>
        <input type="password" id="user_pwd" name="user_pwd" placeholder="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
            title="<?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "cwcud");?>" required>
        <p class="form-info form-sub-item"><?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "cwcud");?> <span id="user_pwd_btn" style="cursor:pointer;text-decoration:underline">Voir son mot de passe</span></p>


        <div class="checkbox form-item">
            <input type="checkbox" name="ok_cgu" id="ok_cgu" required>
            <div class="checkbox-label">
                <label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Digital Cleanup Day.', 'cwcud');?>*</label>
                <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
            </div>
        </div>

        <button class="button form-item"  type="submit" id="sendMessage"><?php _e('Enregistrer', 'cwcud'); ?></button>
        <p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>
    </form>
</section>
<!-- End of the loop -->
<?php //endwhile; endif;?>

<?php get_footer(); ?>
