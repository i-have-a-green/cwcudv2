<?php
/*
Template Name: Listing Cleanup
*/
?>

<?php get_header(); ?>

<!-- Header
<header >
	<?php the_title(); ?>
</header>

<h2><?php _e('Vos Digital Cleanup', 'cwcud');?></h2>
-->

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <section class="wrapper" id="">
        <?php the_content(); ?>
        <?php $permalink = get_the_permalink();?>
    </section>

    <main class="wrapper">
        <?php

        $args = array(
            'post_type'		=> 'cleanup',
            'post_status'   => array("publish"),
            'posts_per_page'	=> -1,
        );

        $meta_query = array();

        if(isset($_GET['cat_cybercleanup']) && !empty($_GET['cat_cybercleanup'])){
            $meta_query[] = array(
                'key' => 'cat_cybercleanup',
                'value' => sanitize_text_field($_GET['cat_cybercleanup']),
            );
            /*$args['meta_key'] = 'cat_cybercleanup';
            $args['meta_value'] = sanitize_text_field($_GET['cat_cybercleanup']);*/
        }

        if(isset($_GET['id_cleanup']) && !empty($_GET['id_cleanup'])){
            $args['post__in'] = array($_GET['id_cleanup']);
        }

        if(isset($_GET['cleanup_name']) && !empty($_GET['cleanup_name'])){

            $meta_query[] = array(
                'key' => 'cleanup_name',
                'value' => sanitize_text_field($_GET['cleanup_name']),
                'compare' => 'LIKE',
            );
            /*$args['meta_key'] = 'cleanup_name';
            $args['meta_value'] = sanitize_text_field($_GET['cleanup_name']);
            $args['meta_compare'] = 'LIKE';*/
        }

        if(isset($_GET['visibility']) && !empty($_GET['visibility'])){
            $meta_query[] = array(
                'key' => 'visibility',
                'value' => sanitize_text_field($_GET['visibility']),
            );
        }

        if(count($meta_query) > 1) {
            $meta_query['relation'] = 'AND';
        }

        $args['meta_query'] = $meta_query;

        $args2 = $args;

        $args2['post_status'] = array("private", "publish");
        $query = new WP_Query( $args2 );
        $inscrits = 0;
        $query->query_vars['meta_query'] = $meta_query;
        $nb_cleanup = $query->found_posts;
        echo '<p><b>';
        if(isset($_GET['s_cleanup'])):
            echo sprintf( __( 'Il y a %s Digital Cleanups correspondants à la recherche', 'cwcud' ), $nb_cleanup );
        else:
            echo sprintf( __( 'Il y a %s Digital Cleanups', 'cwcud' ), $nb_cleanup );
        endif;
        echo '</b></p>';
        ?>
        <!-- FILTERS -->
        <form method="GET" class="filters-cleanup" action="<?php echo $permalink;?>">
            <!-- Filters -->
            <div class="filters">
                <!-- par départements -->
                <!--<div class="select">
						<?php
                $current_v = isset($_GET['s_dep'])? $_GET['s_dep']:'';
                select_geo_area($current_v, 's_dep', '');
                ?>
					</div>
					<input type="date" id="date_start" name="date_start" min="<?php the_field("date_start_event", "option");?>" max="<?php the_field("date_end_event", "option");?>" value="<?php echo (isset($_GET['date_start']))?$_GET['date_start']:'';?>">
			-->
                <div class="select">
                    <select name="cat_cybercleanup">
                        <option value="" <?php @ihag_selected("",$_GET['cat_cybercleanup']);?>><?php _e('Catégorie de Digital Cleanup', 'cwcud');?></option>
                        <option value="data" <?php @ihag_selected("data",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Données', 'cwcud');?></option>
                        <option value="reuse" <?php @ihag_selected("reuse",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Reemploi', 'cwcud');?></option>
                        <option value="hardware" <?php @ihag_selected("hardware",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Recyclage', 'cwcud');?></option>
                    </select>
                </div>
                <!--			<div class="select">
						<select name="visibility">
							<option value="" <?php selected("",$_GET['visibility']);?>><?php _e('Type de de Digital Cleanup', 'cwcud');?></option>
							<option value="public" <?php selected("public",$_GET['visibility']);?>><?php _e('Public', 'cwcud');?></option>
							<option value="private" <?php selected("private",$_GET['visibility']);?>><?php _e('Privé', 'cwcud');?></option>
						</select>
					</div>

					<input type="text" placeholder="<?php _e('Rechercher un Digital Cleanup','cwcud');?>" name="s_cleanup" value="<?php echo (isset($_GET['s_cleanup']))?$_GET['s_cleanup']:'';?>">
			-->
                <input type="text" placeholder="<?php _e('N° d\'un Digital Cleanup','cwcud');?>" name="id_cleanup" value="<?php echo (isset($_GET['id_cleanup']))?$_GET['id_cleanup']:'';?>">
                <input type="text" name="cleanup_name" id="cleanup_name" value="<?php echo isset($_GET['cleanup_name'])? $_GET['cleanup_name']:'';?>" placeholder="<?php _e('Nom du Digital Cleanup') ?>">
                <div class="select">
                    <select name="visibility">
                        <option value="" <?php @ihag_selected("",$_GET['visibility']);?>>Visibilité</option>
                        <option value="public" <?php @ihag_selected("public",$_GET['visibility']);?>>Public</option>
                        <option value="private" <?php @ihag_selected("private",$_GET['visibility']);?>>Privé</option>
                    </select>
                </div>
            </div>

            <!-- btn -->
            <input type="submit" class="button" value="<?php _e('Rechercher','cwcud');?>">

        </form>

        <!-- Listing Archive -->

        <?php
        //if($result):
        query_posts($args);
        global $wp_query;
        if ( have_posts() ) :

        echo '<br><section class="listing-cleanup">';
        while (have_posts()) : the_post();?>

        <div class="cleanup-card no-useless-margin">
            <?php

            echo '<a class="h2-like" href="'.get_the_permalink().'">';



            $visibility = get_post_meta( $post->ID, "visibility", true );
            $cat_cybercleanup = get_post_meta( $post->ID, "cat_cybercleanup", true );
            if ( $cat_cybercleanup == "data" ) {
                echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_data.png" width="30" alt="Nettoyage Données"> ';
            }
            elseif ( $cat_cybercleanup == "hardware" ) {
                echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_hardware.png"  width="30" alt="Réemploi"> ';
            }
            elseif ( $cat_cybercleanup == "reuse" ) {
                echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_reuse.png" width="30" alt="Seconde vie des équipements numériques"> ';
            }
            the_title();
            echo '</a>';
            $author_id = $post->post_author;
            $user = get_userdata( $author_id );
            echo '<p>'.$user->first_name." ".$user->last_name;
            echo (!empty(get_post_meta($post->ID,'structure_name', true))) ? ' - '.get_post_meta($post->ID,'structure_name', true) : '';
            echo '<p>';

            echo '<p class="cleanup-date">';
            echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")<br>";
            echo '<br>'.get_geo_area(get_post_meta( $post->ID, "cleanup_area", true ));
            echo '</p>';

            echo '<p>';
            if(get_post_meta( $post->ID, "cat_cybercleanup", true ) == "data"){
                _e('Digital Cleanup Données', 'cwcud');
            }
            else if(get_post_meta( $post->ID, "cat_cybercleanup", true ) == "reuse"){
                _e('Digital Cleanup Réemploi', 'cwcud');
            }
            else{
                _e('Digital Cleanup Recyclage ', 'cwcud');
            }
            echo '</p>';

            ?>
            <p>
                <?php echo wp_trim_words(get_the_content(), 25);?>
            </p>
            <?php
            if(get_post_meta( $post->ID, "private", true ) == "no_private"):
                $whichLocation = get_post_meta( $post->ID, "location", true ) ;
                $adresse = get_post_meta($post->ID, "cleanup_adresse", true);
                $link = (!empty(get_post_meta($post->ID, "link_connection", true))) ? '<a href="'.get_post_meta($post->ID, "link_connection", true).'" target="_BLANK">'.get_post_meta($post->ID, "link_connection", true).'</a>' : 'Le lien vous sera envoyé ultérieurement par email.';
                $otherInformation = get_post_meta( $post->ID, "more_information", true );

                if ($whichLocation === "location_facetoface"):
                    echo '<p><span class="label-like">'.__('Évènement présentiel','cwcud').' :</span> '.$adresse.'<br>'.$otherInformation.'</p>';
                elseif($whichLocation === "location_distancing"):
                    echo '<p><span class="label-like">'.__('Évènement distanciel','cwcud').' : </span>'.$link.'</p>';
                elseif($whichLocation === "location_both"):
                    echo '<p><span class="label-like">'.__('Évènement présentiel','cwcud').' :</span> '.$adresse.' - '.$otherInformation;
                    echo '<br><span class="label-like">'.__('Évènement distanciel','cwcud').' :</span> '.$link.'</p>';
                endif;
            endif;
            ?>

            <?php
            if(get_post_meta( $post->ID, "visibility", true ) == "private"){
                echo '<p><strong>'.__('Le Digital Cleanup est privé','cwcud').'</strong></p>';
            }
            /*else{
                $participants = (int)get_post_meta( $post->ID, 'participants', true );
                $nb_participant_max = (int)get_post_meta( $post->ID, "nb_participant_max", true );
                if(empty($nb_participant_max) || $participants < $nb_participant_max):?>
                    <!--<button class="button-yellow inscription_cleanup" data-id="<?php echo get_the_ID();?>" data-child="<?php echo (get_post_meta($post->ID, "child", true) == "yes_child") ? "number":"hidden";?>" data-child-label="<?php echo (get_post_meta($post->ID, "child", true) == "yes_child") ? "inline-block":"none";?>"
                        data-title="<h2>Inscription : <?php echo str_replace('"', '', get_the_title());?></h2><p>Organisé par <?php echo get_post_meta($post->ID, "structure_name", true);?>, le <?php echo date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")";?></p>">
                        <?php _e('Participer', 'cwcud'); ?>
                    </button>-->
                    <br><a href="<?php the_permalink();?>?open=1" class="button-yellow" style="display:inline-block"><?php _e('Participer', 'cwcud'); ?></a>
                <?php
                else:
                    echo '<p><strong>'.__('Le Digital Cleanup est complet','cwcud').'</strong></p>';
                endif;
            }*/

            echo '</div>';
            //part_cleanup($post);
            endwhile;
            echo '</section>';
            ?>

            <?php
            //if(!isset($_GET['s_cleanup']) ||  (isset($_GET['s_cleanup']) && empty($_GET['s_cleanup']) ) ){
            ihag_page_navi();
            //}
            wp_reset_query();
            else:
                ?>
                <p>
                    <?php _e( "Il n'y a pas de Digital Cleanup correpondant à votre recherche.", 'cwcud' );?>
                </p>
            <?php
            endif;
            /*else:
                ?>
                <p>
                <?php _e( "Il n'y a pas de Digital Cleanup correpondant à votre recherche.", 'cwcud' );?>
                </p>
                <?php
            endif;*/?>

            <!--<div class="modale" id="modale_inscription_cleanup">
			<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
			<div class="cleanup-modale">
				<div id="modale_title_cleanup">
				</div>
				<form class="form-white" action="" method="post" name="registerUserCleanUp" id="registerUserCleanUp">
					<label for="name_registerUserCleanUp"><?php _e('Nom', 'cwcud') ?></label>
					<input type="text" name="name_registerUserCleanUp" id="name_registerUserCleanUp" placeholder="<?php _e('Véronique Dubois', 'cwcud'); ?>">

					<label for="phone_registerUserCleanUp"><?php _e('Téléphone', 'cwcud') ?></label>
					<input type="tel" name="phone_registerUserCleanUp" id="phone_registerUserCleanUp" placeholder="<?php _e('+33 6 01 02 03 04', 'cwcud'); ?>">

					<label for="email_registerUserCleanUp"><?php _e('Adresse e-mail', 'cwcud') ?>*</label>
					<input type="email" name="email_registerUserCleanUp" id="email_registerUserCleanUp" required placeholder="<?php _e('adresse@mail.com', 'cwcud'); ?>">
					<p class="form-info">
						<?php _e('* Votre adresse email sera utilisée pour vous recontacter et vous tenir informé.', 'cwcud'); ?>
					</p>
					<label for="invite_registerUserCleanUp"><?php _e("Nombre d'invités (en plus de vous)", 'cwcud') ?></label>
					<input type="number"  min="0" name="invite_registerUserCleanUp" id="invite_registerUserCleanUp" value="" placeholder="Laisser vide si pas d'invité">

					<label for="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp_label"><?php _e("Nombre d'enfants invités", 'cwcud') ?></label>
					<input type="number"  min="0" name="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp"  value="" placeholder="Laisser vide si pas d'invité">

					<p class="form-item no-margin form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

					<div class="checkbox form-item">
						<input type="checkbox" name="ok_cgu" id="ok_cgu" required>
						<div class="checkbox-label">
							<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Digital Cleanup Day.', 'cwcud');?>*</label>
							<div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
						</div>
					</div>

					<div class="checkbox form-item">
						<input type="checkbox" name="checkbox_newsletter" id="checkbox_newsletter" value="true">
						<div class="checkbox-label">
							<label for="checkbox_newsletter">
								<?php _e("Je désire m'inscrire à la newsletter du Digital Cleanup Day", 'cwcud');?>
							</label>
						</div>
					</div>

					<input type="hidden" name="honeypot" value="">
					<input type="hidden" name="id_cleanup" id="id_cleanup" value="">
					<input type="submit" class="button form-item" id="sendRegisterUserCleanUp" value="<?php _e("Valider l'inscription", 'cwcud'); ?>">
					<div id="ResponseMessageRegisterUserCleanUp" class="ResponseMessageRegisterUserCleanUp">
						<p class="ctr"><?php _e('Merci, votre inscription a été enregistrée.', '');?></p>
					</div>
				</form>
			</div>
		</div>-->

            <?php
            //endif;
            //endif;
            ?>
    </main>

    <!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer();
?>
