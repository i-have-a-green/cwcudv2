<?php
/*
Template Name: Inscription membre EN
*/

if ( is_user_logged_in() ) :
    wp_redirect( get_permalink(get_field("page_my_account", "options")), 302);
endif;

?>

<?php get_header(); ?>

<!-- Header --> 
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Digital Cleanup Day Account', 'cwcud');?></h2>
    </div>
</header>

<main id="raw-content">
	<?php the_content(); ?>
</main>

        
<!-- Begining of the loop -->
<?php //if (have_posts()) : while (have_posts()) : the_post(); ?>

<section class="wrapper">
    <div class="form-container ">
        <h2 style="text-align:center"><?php _e("I already have a Digital Cleanup Day account","cwcud");?></h2>
        <?php wp_login_form(
            array(
                'remember'       => false,
                'label_username' => __( 'Email', 'cwcud' ),
                'label_password' => __( 'Password', 'cwcud' ),
                //'remember'		 => true,
                'redirect'       => get_permalink(get_field("page_member_en", "options")),
            )
        );?>
        <a class="lost-pswd" href="<?php echo wp_lostpassword_url(get_home_url()) ; ?>"><?php _e("Lost password ?", "cwcud"); ?></a>
    </div>
    <h2 style="text-align:center"><?php _e("I create a Digital Cleanup Day account","cwcud");?></h2>
    <form action="" method="post"  name="organisateurForm" id="organisateurForm" class="form-style">
        
        <input type="hidden" name="honeyPot" value="">
        <input type="hidden" name="organisateurFormEn" id="organisateurFormEn" value="<?php echo get_permalink(get_field("page_member_en", "options"));?>">
    
        <label for="user_lastname"><?php _e('Lastname *', 'cwcud'); ?></label>
        <input type="text" id="user_lastname" name="user_lastname" placeholder="Dubois" required>

        <label for="user_firstname"><?php _e('Forstname *', 'cwcud'); ?></label>
        <input type="text" id="user_firstname" name="user_firstname" placeholder="Véronique" required>

  
        <label for="user_email"><?php _e('E-mail *', 'cwcud'); ?></label>
        <input type="email" id="user_email" name="user_email" placeholder="email@exemple.com" required>
        <p class="form-info form-sub-item"><?php _e('* Your email address will serve as your account ID.', 'cwcud');?></p>

        <label for="user_pwd"><?php _e('Password *', 'cwcud'); ?></label>
        <input type="password" id="user_pwd" name="user_pwd" placeholder="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
            title="<?php _e("Must contain at least one number and one upper and lower case letter, and at least 8 or more characters", "cwcud");?>" required>
        <p class="form-info form-sub-item"><?php _e("Must contain at least one number and one upper and lower case letter, and at least 8 or more characters", "cwcud");?> <span id="user_pwd_btn" style="cursor:pointer;text-decoration:underline">Voir son mot de passe</span></p>


        <div class="checkbox form-item">
            <input type="checkbox" name="ok_cgu" id="ok_cgu" required>
            <div class="checkbox-label">
                <label for="ok_cgu"><?php _e('I agree to the processing of my personal data within the framework of the Digital Cleanup Day.', 'cwcud');?>*</label>
                <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('Find out more about the management of your data and your rights.', 'cwcud') ;?></a></div>
            </div>
        </div>

        <button class="button form-item"  type="submit" id="sendMessage"><?php _e('Save', 'cwcud'); ?></button>
        <p class="form-item form-legal-text"><?php _e('* Fields require', 'cwcud'); ?></p>
    </form>
</section>
<!-- End of the loop -->
<?php //endwhile; endif;?>

<?php get_footer(); ?>
