<?php
/*
Template Name: Blog (archive)
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main> 

	<header id="archive-header" class="wrapper">

		<!-- Fil d'Ariane -->
		<?php wpBreadcrumb() ?>
		
		<!-- Titre-->
		<?php the_title('<h1 class="center">', '</h1>'); ?>

		
	</header>

	<section class="wrapper archive-listing">
		
		<div class="grid-post wrapper-medium">
		<?php
			$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
			$type = "post";
			$args = array(
			    'paged' => $num_page,
			    'post_type'   => $type,
			);

			query_posts($args);
			global $wp_query; 
			if ( have_posts() ) : while (have_posts()) : the_post();
				get_template_part('template-parts/archive', "post");
			endwhile; endif;
		?>
		</div>

		<nav class="pagination wrapper-medium">
			<?php ihag_page_navi()?>
			<?php wp_reset_query(); ?>
		</nav>
		
	</section>

	<?php the_content('<section id="raw-content">', '</section>');?>


</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>