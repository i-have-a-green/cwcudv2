<?php
/*
Template Name: Espace organisateur
*/

?>

<?php get_header("membre"); ?>

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
        <h2 class="big"><?php _e('Mon compte', 'cwcud');?></h2>
    </div>
</header>
        
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


    <form action="" method="post" id="form_admin_organisateur" class="organizer-admin-infos form_admin_organisateur">
        <input type="hidden" name="honeyPot" value="">
        <?php
            $user_connect = wp_get_current_user();
            //$userGender = get_user_meta($user_connect->ID, 'user_gender', true );
        ?>
        <!--<div class="form-item">
            <p class="no-margin label-like"><?php _e('Civilité *', 'cwcud');?></p>
            <div class="checkbox form-sub-item">
                <input type="radio" name="user_gender" id="user_female" value="F" <?php echo ($userGender == 'F')?"checked":"";?>>
                <label class="checkbox-label" for="user_female"><?php _e('Madame', 'cwcud');?></label>
            </div>
            <div class="checkbox form-sub-item">
                <input type="radio" name="user_gender" id="user_male" value="M" <?php echo ($userGender == 'M')?"checked":"";?>>
                <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
            </div>
        </div>-->

        <label for="user_lastname"><?php _e('Nom *', 'cwcud'); ?></label>
        <input type="text" id="user_lastname" name="user_lastname" placeholder="Dubois" required value="<?php echo get_user_meta($user_connect->ID, 'last_name', true );?>">

        <label for="user_firstname"><?php _e('Prénom *', 'cwcud'); ?></label>
        <input type="text" id="user_firstname" name="user_firstname" placeholder="Véronique" required value="<?php echo get_user_meta($user_connect->ID, 'first_name', true );?>">

        <!--<label for="user_phone"><?php // _e('Téléphone *', 'cwcud'); ?></label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required value="<?php //echo get_user_meta($user_connect->ID, 'user_phone', true );?>">
        <p class="form-info form-sub-item"><?php // _e("Ce numéro est destiné à l'équipe projet pour vous contacter plus rapidement", "cwcud");?></p>-->

        <label for="user_email"><?php _e('Adresse mail *', 'cwcud'); ?></label>
        <input type="email" id="user_email" name="user_email" placeholder="adresse.mail@exemple.com" required readonly value="<?php echo $user_connect->user_email;?>">
        <p class="form-info form-sub-item"><?php _e('* Votre adresse mail n\'est pas modifiable.', 'cwcud');?></p>

        <label for="user_pwd"><?php _e('Mot de passe *', 'cwcud'); ?></label>
        <input type="password" id="user_pwd" name="user_pwd" placeholder="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
            title="<?php _e("Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères ou plus", "cwcud");?>">
        <p class="form-info form-sub-item"><?php _e('* Laissez vide si vous ne souhaitez pas modifier.', 'cwcud');?></p>

        <div class="checkbox form-item">
            <input type="checkbox" name="ok_cgu" id="ok_cgu" required>
            <div class="checkbox-label">
                <label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Digital Cleanup Day.', 'cwcud');?>*</label>
                <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
            </div>
        </div>

        <button class="button form-item"  type="submit" id="sendMessage"><?php _e('Enregistrer', 'cwcud'); ?></button>
        <p class="form-item form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

    </form>


<main id="raw-content">
	<?php the_content(); ?>
</main>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
?>
