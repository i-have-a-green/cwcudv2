<?php
/*
Template Name: Bilan CleanUp
*/
//$current_user = wp_get_current_user();

?>

<?php get_header("membre"); ?> 

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<!--<h2 class="big"><?php _e('Organiser un Digital Cleanup', 'cwcud');?></h2>-->
    </div>
</header>
        
<?php
$id_cleanup = false;
if(isset($_GET['id_cleanup'])){
    $id_cleanup = $_GET['id_cleanup'];
}
?>
<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<section class="wrapper">
    <?php 
        // CF7 Form
        echo apply_shortcodes( '[contact-form-7 id="' . get_field('bilan_formulaire', 'option') . '"]' );
    ?>
    <?php

    $cat_cybercleanup = get_post_meta($id_cleanup, 'cat_cybercleanup', true);

    if ($cat_cybercleanup == 'reuse') {
        ?>
        <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            // div a ne pas afficher
            /*let data = document.getElementById('data')
            let hardware = document.getElementById('hardware')

            // input des divs
            let inputsData = data.getElementsByTagName('input')
            let inputsHardware = hardware.getElementsByTagName('input')

            // value null sur les inputs
            Array.from(inputsData).forEach(element => {
                element.value = 'null'
            });
            
            Array.from(inputsHardware).forEach(element => {
                element.value = 'null'
            });*/
            
            // select radio des divs
            /*let radioData = data.getElementsByClassName('wpcf7-radio')
            let radioHardware = hardware.getElementsByClassName('wpcf7-radio')

            // check les radios des divs
            Array.from(radioData).forEach(element => {
                let radio = element.getElementsByTagName('input')
                radio[0].checked = true
            });
            
            Array.from(radioHardware).forEach(element => {
                let radio = element.getElementsByTagName('input')
                radio[0].checked = true
            });*/

            data.style.display = 'none'
            hardware.style.display = 'none'
        });
        </script>
        <?php
    }
    else if ($cat_cybercleanup == 'hardware') {
        ?>
        <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            // div a ne pas afficher
            /*let data = document.getElementById('data')
            let reuse = document.getElementById('reuse')

            // input des divs
            let inputsData = data.getElementsByTagName('input')
            let inputsReuse = reuse.getElementsByTagName('input')

            // value null sur les inputs
            Array.from(inputsData).forEach(element => {
                element.value = 'null'
            });
            
            Array.from(inputsReuse).forEach(element => {
                element.value = 'null'
            });*/
            
            // select radio des divs
            /*let radioData = data.getElementsByClassName('wpcf7-radio')
            let radioReuse = reuse.getElementsByClassName('wpcf7-radio')

            // check les radios des divs
            Array.from(radioData).forEach(element => {
                let radio = element.getElementsByTagName('input')
                radio[0].checked = true
            });
            
            Array.from(radioReuse).forEach(element => {
                let radio = element.getElementsByTagName('input')
                radio[0].checked = true
            });*/

            data.style.display = 'none'
            reuse.style.display = 'none'
        });
        </script>
        <?php
    }
    else if ($cat_cybercleanup == 'data') {
        ?>
        <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            // div a ne pas afficher
           /* let hardware = document.getElementById('hardware')
            let reuse = document.getElementById('reuse')

            // input des divs
            let inputsHardware = hardware.getElementsByTagName('input')
            let inputsReuse = reuse.getElementsByTagName('input')

            // value null sur les inputs
            Array.from(inputsHardware).forEach(element => {
                element.value = 'null'
            });
            
            Array.from(inputsReuse).forEach(element => {
                element.value = 'null'
            });*/
            
            // select radio des divs
            /*let radioHardware = hardware.getElementsByClassName('wpcf7-radio')
            let radioReuse = reuse.getElementsByClassName('wpcf7-radio')

            // check les radios des divs
            Array.from(radioHardware).forEach(element => {
                let radio = element.getElementsByTagName('input')
                radio[0].checked = true
            });
            
            Array.from(radioReuse).forEach(element => {
                let radio = element.getElementsByTagName('input')
                radio[0].checked = true
            });
*/
            reuse.style.display = 'none'
            hardware.style.display = 'none'
        });
        </script>
        <?php
    }
    ?>

    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            // get form
            let form = document.getElementsByClassName('wpcf7-form');
            // all inputs and labels of form
            let inputs = document.getElementsByTagName('input');
            let labels = document.getElementsByTagName('label');
            let textareas = document.getElementsByTagName('textarea');
            let selects = document.getElementsByTagName('select');
            let spans = document.getElementsByTagName('span');
            let titles2 = document.getElementsByTagName('h2');
            let titles4 = document.getElementsByTagName('h4');
            let paragraphs = document.getElementsByTagName('p');
            document.addEventListener( 'wpcf7mailsent', function( event ) { 
                /* Hide all element of form */
                for(let input of inputs) {
                    input.style.display = 'none'
                }
                for(let label of labels) {
                    label.style.display = 'none'
                }
                for(let textarea of textareas) {
                    textarea.style.display = 'none'
                }
                for(let select of selects) {
                    select.style.display = 'none'
                }
                for(let span of spans) {
                    span.style.display = 'none'
                }
                for(let title2 of titles2) {
                    title2.style.display = 'none'
                }
                for(let title4 of titles4) {
                    title4.style.display = 'none'
                }
                for(let paragraph of paragraphs) {
                    paragraph.style.display = 'none'
                }
                document.getElementsByClassName('wpcf7-spinner')[0].style.display = 'none';

                let link = document.createElement("a")
                link.classList.add("button")
                link.href = "<?php echo get_permalink(get_field('redirection_page', 'option')) ?>"
                let textLink = document.createTextNode(<?php echo '"Retour vers ' . get_the_title(get_field('redirection_page', 'option')) . '"'; ?>)
                link.appendChild(textLink)
                form[0].appendChild(link)
            }, false );
        });

        // If mail as been send
    </script>
</section>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php 
get_footer(); 
?>
