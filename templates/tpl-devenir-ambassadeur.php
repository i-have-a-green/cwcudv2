<?php
/*
Template Name: New ambassadeur
*/

get_header("membre"); ?>
<?php
    $user_connect = wp_get_current_user();
    $user_roles = $user_connect->roles;
?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<?php if (in_array( 'ambassadeur_to_validate', $user_roles, true ) ):?>
    <h2 style="text-align:center"><?php _e('Demande pour devenir abassadeur : en attente','cwcud');?></h2>
<?php else:?>

<form class="form-style" action="" method="post"  name="ambassadeurForm" id="ambassadeurForm">
    
    <!-- Honey Pot -->
    <input type="hidden" name="honeyPot" value=""> 
	
    <h2 class="ctr no-margin"><?php _e('Ambassadeur', 'cwcud');?></h2>

    <p class="ctr form-sub-item"><?php _e('Devenez l\'ambassadeur local de votre ville !', 'cwcud');?></p>

    <div class="form-item">
        <p class="no-margin label-like"><?php _e('Type de structure *', 'cwcud');?></p>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="citizen" class="structure" value="citizen" onclick="displayRadioValue()" <?php checked(get_user_meta($user_connect->ID, "type_structure", true), "citizen");?>>
            <label class="checkbox-label" for="citizen"><?php _e('Citoyen', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="association" class="structure" value="association" onclick="displayRadioValue()" <?php checked(get_user_meta($user_connect->ID, "type_structure", true), "association");?>>
            <label class="checkbox-label" for="association"><?php _e('Association', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="school" class="structure" value="school" onclick="displayRadioValue()" <?php checked(get_user_meta($user_connect->ID, "type_structure", true), "school");?>>
            <label class="checkbox-label" for="school"><?php _e('École', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="collectivity" class="structure" value="collectivity" onclick="displayRadioValue()" <?php checked(get_user_meta($user_connect->ID, "type_structure", true), "collectivity");?>>
            <label class="checkbox-label" for="collectivity"><?php _e('Collectivité', 'cwcud');?></label>
        </div>
        <div class="checkbox form-sub-item">
            <input type="radio" name="type_structure" id="company" class="structure" value="company" onclick="displayRadioValue()" <?php checked(get_user_meta($user_connect->ID, "type_structure", true), "company");?>>
            <label class="checkbox-label" for="company"><?php _e('Entreprise', 'cwcud');?></label>
        </div>
    </div>
    
    <div id="structure_name" <?php echo (empty(get_user_meta($user_connect->ID, "structure_name", true)))?'style="display: none;"':'';?>>
        <label for="structure_name"><?php _e('Nom de la structure', 'cwcud'); ?></label>
        <input type="text"  name="structure_name" placeholder="Nom de votre structure" value="<?php echo get_user_meta($user_connect->ID, "structure_name", true);?>" >
    </div>

    <label for="user_adress"><?php _e('Adresse postale *', 'cwcud'); ?></label>
    <input type="text" id="user_adress" name="user_adress" placeholder="75 rue Gemberra" value="<?php echo get_user_meta($user_connect->ID, "user_adress", true);?>" required>
    <p class="form-info">
        <?php _e('* Information destinée uniquement aux administrateurs du site. Votre adresse postale ne sera pas publiée sur le site.', 'cwcud'); ?>
    </p>
    
    <label for="user_district"><?php _e('Quartier', 'cwcud'); ?></label>
    <input type="text" id="user_district" name="user_district" placeholder="VIe arrondissement" value="<?php echo get_user_meta($user_connect->ID, "user_district", true);?>">
    <label for="user_code_postal"><?php _e('Code postal *', 'cwcud'); ?></label>
    <input type="text" id="user_code_postal" name="user_code_postal" placeholder="75006" value="<?php echo get_user_meta($user_connect->ID, "user_code_postal", true);?>" required>
    <label for="user_city"><?php _e('Ville *', 'cwcud'); ?></label>
    <input type="text" id="user_city" name="user_city" placeholder="Paris" value="<?php echo get_user_meta($user_connect->ID, "user_city", true);?>" required>

    
    <label for="user_geo_area"><?php _e('Zone géographique :', 'cwcud'); ?>*</label>
    <div class="select">
    <?php select_geo_area(get_user_meta(get_current_user_id(), "user_geo_area", true), 'user_geo_area'); ?>        
    </div>

    <label for="user_phone"><?php _e('Téléphone *', 'cwcud'); ?></label>
        <input type="tel" id="user_phone" name="user_phone" placeholder="+33 6 01 02 03 04"  required value="<?php echo get_user_meta($user_connect->ID, 'user_phone', true );?>">
        <p class="form-info form-sub-item"><?php _e("Ce numéro est destiné à l'équipe projet pour vous contacter plus rapidement", "cwcud");?></p>
    
    <p class="form-item no-margin label-like"><?php _e('Afficher mon numéro de téléphone dans l\'annuaire des ambassadeurs *', 'cwcud');?></p>
    <div class="form-row">
        <div class="checkbox">
            <input type="radio" name="number_authorisation" id="number_yes" class="number" value="number_yes"  <?php checked(get_user_meta($user_connect->ID, "number_authorisation", true), "number_yes");?>>
            <label class="checkbox-label" for="number_yes"><?php _e('Oui', 'cwcud');?></label>
        </div>
        <div class="checkbox">
            <input type="radio" name="number_authorisation" id="number_no" class="number" value="number_no" <?php checked(get_user_meta($user_connect->ID, "number_authorisation", true), "number_no");?>>
            <label class="checkbox-label" for="number_no"><?php _e('Non', 'cwcud');?></label>
        </div>
    </div>

    <p class="form-item no-margin form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

    

    <!-- ok cgu -->
    <div class="checkbox form-item">
        <input type="checkbox" name="ok_cgu" id="ok_cgu3" required>
        <div class="checkbox-label">
            <label for="ok_cgu3"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Digital Cleanup Day.', 'cwcud');?>*</label>
            <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
        </div>
    </div>

    <!-- ok cgu bis -->
    <div class="checkbox form-item">
        <input type="checkbox" name="ok_cgu" id="ok_cgu2" required>
        <div class="checkbox-label">
            <label for="ok_cgu2"><?php _e('J\'accepte que mon adresse e-mail soit affichée publiquement sur le site du Digital Cleanup Day afin de faciliter le contact avec des organisateurs de Digital Cleanups.', 'cwcud');?>*</label>
            <div class="form-info"><a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a></div>
        </div>
    </div>

    <!-- Button submit -->
	<button class="button form-item"  type="submit" id="sendMessageAmbassadeur"><?php _e('Envoyer la candidature', 'cwcud'); ?></button>
    <div id="ResponseMessageAmbassadeur" style="text-align:center;padding:2rem;">
        <?php _e('Candidature enregistrée avec succès. Un email de confirmation a été envoyé.', 'cwcud');?>
    </div>
</form>


<!-- End of the loop -->
<?php 
endif;// ! ambassadeur en attente

endwhile; endif;?>

<?php

get_footer();
