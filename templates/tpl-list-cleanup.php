<?php
/*
Template Name: Listing Cleanup
*/
?>

<?php get_header(); ?>

    <section class="wrapper" id="">
        <?php the_content(); ?>
        <?php $permalink = get_the_permalink();?>
    </section>

    <main class="wrapper">
        <?php
        $args = array(
            'post_type'		=> 'cleanup',
            'post_status'   => array("publish"),
            'posts_per_page'	=> -1,
        );

        $meta_query = array();

        if(isset($_GET['cat_cybercleanup']) && !empty($_GET['cat_cybercleanup'])){
            $meta_query[] = array(
                'key' => 'cat_cybercleanup',
                'value' => sanitize_text_field($_GET['cat_cybercleanup']),
            );
        }

        if(isset($_GET['id_cleanup']) && !empty($_GET['id_cleanup'])){
            $args['post__in'] = array($_GET['id_cleanup']);
        }

        if(isset($_GET['cleanup_name']) && !empty($_GET['cleanup_name'])){

            $meta_query[] = array(
                'key' => 'cleanup_name',
                'value' => sanitize_text_field($_GET['cleanup_name']),
                'compare' => 'LIKE',
            );
            /*$args['meta_key'] = 'cleanup_name';
            $args['meta_value'] = sanitize_text_field($_GET['cleanup_name']);
            $args['meta_compare'] = 'LIKE';*/
        }

        if(isset($_GET['visibility']) && !empty($_GET['visibility'])){
            $meta_query[] = array(
                'key' => 'visibility',
                'value' => sanitize_text_field($_GET['visibility']),
            );
        }
        else {
            $meta_query[] = array(
                'key' => 'visibility',
                'compare' => '!=',
                'value' => 'secret',
            );
        }
        
        if(isset($_GET['location']) && !empty($_GET['location'])){
            $meta_query[] = array(
                'key' => 'location',
                'value' => sanitize_text_field($_GET['location']),
            );
        }

        if(isset($_GET['area']) && !empty($_GET['area'])){
            $meta_query[] = array(
                'key' => 'cleanup_area',
                'value' => sanitize_text_field($_GET['area']),
            );
        }

        if(count($meta_query) > 1) {
            $meta_query['relation'] = 'AND';
        }

        $args['meta_query'] = $meta_query;
        $args2 = $args;
        $args2['post_status'] = array("private", "publish");
        $query = new WP_Query( $args2 );
        $inscrits = 0;
        $query->query_vars['meta_query'] = $meta_query;
        $nb_cleanup = $query->found_posts;
        echo '<p><b>';
        if(isset($_GET['s_cleanup'])):
            echo sprintf( __( 'Il y a %s Digital Cleanups correspondants à la recherche', 'cwcud' ), $nb_cleanup );
        else:
            echo sprintf( __( 'Il y a %s Digital Cleanups', 'cwcud' ), $nb_cleanup );
        endif;
        echo '</b></p>';
        ?>
        <!-- FILTERS -->
        <form method="GET" class="filters-cleanup" action="<?php echo $permalink;?>">
            <div class="filters">
                <div class="select">
                    <select name="cat_cybercleanup">
                        <option value="" <?php @ihag_selected("",$_GET['cat_cybercleanup']);?>><?php _e('Catégorie de Digital Cleanup', 'cwcud');?></option>
                        <option value="data" <?php @ihag_selected("data",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Données', 'cwcud');?></option>
                        <option value="reuse" <?php @ihag_selected("reuse",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Reemploi', 'cwcud');?></option>
                        <option value="hardware" <?php @ihag_selected("hardware",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Recyclage', 'cwcud');?></option>
                    </select>
                </div>
                <input type="text" placeholder="<?php _e('N° d\'un Digital Cleanup','cwcud');?>" name="id_cleanup" value="<?php echo (isset($_GET['id_cleanup']))?$_GET['id_cleanup']:'';?>">
                <input type="text" name="cleanup_name" id="cleanup_name" value="<?php echo isset($_GET['cleanup_name'])? $_GET['cleanup_name']:'';?>" placeholder="<?php _e('Nom du Digital Cleanup') ?>">
                <div class="select">
                    <select name="visibility">
                        <option value="" <?php @ihag_selected("",$_GET['visibility']);?>>Type de de Digital Cleanup</option>
                        <option value="public" <?php @ihag_selected("public",$_GET['visibility']);?>>Public</option>
                        <option value="private" <?php @ihag_selected("private",$_GET['visibility']);?>>Privé</option>
                    </select>
                </div>
                <div class="select">
                    <select name="location">
                        <option value="" <?php @ihag_selected("",$_GET['location']);?>>Présentiel / Distanciel</option>
                        <option value="location_facetoface" <?php @ihag_selected("location_facetoface",$_GET['location']);?>>Présentiel</option>
                        <option value="location_distancing" <?php @ihag_selected("location_distancing",$_GET['location']);?>>Distanciel</option>
                    </select>
                </div>
                <div class="select">
                    <select name="area">
                        <option value="" <?php @ihag_selected("",$_GET['area']);?>>Département</option>
                        <?php
                        $pays = get_field('geo_areas', 'option');
                        foreach($pays as $region) {
                            ?>
                            <optgroup label="<?php echo $region['region']; ?>">
                                <?php
                                $departements = $region['departements'];
                                foreach ($departements as $departement) {
                                    ?>
                                    <option value="<?php echo sanitize_key($departement['departement']) ?>" <?php @ihag_selected(sanitize_key($departement['departement']),$_GET['area']);?>><?php echo $departement['departement']; ?></option>
                                    <?php
                                }
                                ?>
                            </optgroup>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <input type="submit" class="button" value="<?php _e('Rechercher','cwcud');?>">
        </form>

        <!-- Listing Archive -->
        <?php
        query_posts($args);
        global $query;
        if ( $query->have_posts() ) :
            echo '<br><section class="listing-cleanup">';
            while ($query->have_posts()) {
                $query->the_post()
                ?>
                <div class="cleanup-card no-useless-margin">
                <?php
                echo '<a class="h2-like" href="'.get_the_permalink().'">';
                $visibility = get_post_meta( $post->ID, "visibility", true );
                $cat_cybercleanup = get_post_meta( $post->ID, "cat_cybercleanup", true );
                if ( $cat_cybercleanup == "data" ) {
                    echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_data.png" width="30" alt="Nettoyage Données"> ';
                }
                else if ( $cat_cybercleanup == "hardware" ) {
                    echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_hardware.png"  width="30" alt="Réemploi"> ';
                }
                else if ( $cat_cybercleanup == "reuse" ) {
                    echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_reuse.png" width="30" alt="Seconde vie des équipements numériques"> ';
                }
                the_title();
                echo '</a>';
                $author_id = $post->post_author;
                $user = get_userdata( $author_id );
                echo '<p>'.$user->first_name." ".$user->last_name;
                echo (!empty(get_post_meta($post->ID,'structure_name', true))) ? ' - '.get_post_meta($post->ID,'structure_name', true) : '';
                echo '<p>';
                echo '<p class="cleanup-date">';
                echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")<br>";
                echo '<br>'.get_geo_area(get_post_meta( $post->ID, "cleanup_area", true ));
                echo '</p>';
                echo '<p>';
                if(get_post_meta( $post->ID, "cat_cybercleanup", true ) == "data"){
                    _e('Digital Cleanup Données', 'cwcud');
                }
                else if(get_post_meta( $post->ID, "cat_cybercleanup", true ) == "reuse"){
                    _e('Digital Cleanup Réemploi', 'cwcud');
                }
                else{
                    _e('Digital Cleanup Recyclage ', 'cwcud');
                }
                echo '</p>';
                ?>
                <p>
                <?php echo wp_trim_words(get_the_content(), 25);?>
            </p>
                <?php
                if(get_post_meta( $post->ID, "private", true ) == "no_private"):
                    $whichLocation = get_post_meta( $post->ID, "location", true ) ;
                    $adresse = get_post_meta($post->ID, "cleanup_adresse", true);
                    $link = (!empty(get_post_meta($post->ID, "link_connection", true))) ? '<a href="'.get_post_meta($post->ID, "link_connection", true).'" target="_BLANK">'.get_post_meta($post->ID, "link_connection", true).'</a>' : 'Le lien vous sera envoyé ultérieurement par email.';
                    $otherInformation = get_post_meta( $post->ID, "more_information", true );

                    if ($whichLocation === "location_facetoface"):
                        echo '<p><span class="label-like">'.__('Évènement présentiel','cwcud').' :</span> '.$adresse.'<br>'.$otherInformation.'</p>';
                    elseif($whichLocation === "location_distancing"):
                        echo '<p><span class="label-like">'.__('Évènement distanciel','cwcud').' : </span>'.$link.'</p>';
                    elseif($whichLocation === "location_both"):
                        echo '<p><span class="label-like">'.__('Évènement présentiel','cwcud').' :</span> '.$adresse.' - '.$otherInformation;
                        echo '<br><span class="label-like">'.__('Évènement distanciel','cwcud').' :</span> '.$link.'</p>';
                    endif;
                endif;
                if(get_post_meta( $post->ID, "visibility", true ) == "private"){
                    echo '<p><strong>'.__('Le Digital Cleanup est privé','cwcud').'</strong></p>';
                }
                echo '</div>';
            }
            echo '</section>';
            ihag_page_navi();
            wp_reset_query();
        else:
            ?>
            <p>
                <?php _e( "Il n'y a pas de Digital Cleanup correpondant à votre recherche.", 'cwcud' );?>
            </p>
        <?php
        endif;
        ?>
    </main>

<?php get_footer();
?>
