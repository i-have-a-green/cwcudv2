<?php
/*
Template Name: Listing Cleanup Organisateur
*/
$current_user = wp_get_current_user();

?>

<?php get_header("membre"); ?>

<!-- Header -->
<header class="organizer-admin-header">
    <div class="wrapper">
        <h1><?php the_title(); ?></h1>
		<h2 class="big"><?php _e('Espace membre', 'cwcud');?></h2>
		<a class="button" href="<?php echo get_permalink( get_field("page_add_cleanup", "option"));?>"><?php _e('Créer un nouveau Digital Cleanup', 'cwcud'); ?></a>
    </div>
</header>


<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main id="raw-content">
	<?php the_content(); ?>
</main>

<!-- Listing Archive -->
<section class="wrapper">
	
	<?php
	global $current_user;
	get_currentuserinfo();
	$arg = array(
		'posts_per_page'    => -1,
		'post_status' 		=> array('publish', 'private'),
		'post_type'			=> 'cleanup',
		'author'        =>  $current_user->ID,
	);

	$posts = get_posts( $arg );

	// <!-- pour le scroll -->
	//$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);

	if($posts):
	?>

		<div class="listing-cleanup-organizer-view">
		
			<?php 
			foreach($posts as $post): 		
				setup_postdata( $post );
				?>
				<article class="cleanup-card organizer-view">
					<div class="info-container no-useless-margin">
						<!-- Title -->
						<?php
						$visibility = get_post_meta( $post->ID, "visibility", true );
						$cat_cybercleanup = get_post_meta( $post->ID, "cat_cybercleanup", true );
						if ( $cat_cybercleanup == "data" ) { 
							echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_data.png" width="30" alt="Nettoyage Données"> '; 
							echo 'Digital Cleanup Données : ';
						}
						elseif ( $cat_cybercleanup == "hardware" ) { 
							echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_hardware.png"  width="30" alt="Réemploi"> '; 
							echo 'Digital Cleanup Recyclage : ';
						}
						elseif ( $cat_cybercleanup == "reuse" ) { 
							echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_reuse.png" width="30" alt="Seconde vie des équipements numériques"> '; 
							echo 'Digital Cleanup Réemploi : ';
						}
						echo '<h3>';
						the_title();
						?>
						</h3>
						<!-- date  -->
						<p>
							<?php 
							echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")<br>";
							?>
						</p>
					
						<!-- nb max -->
						<?php 
						if(get_post_meta( $post->ID, "visibility", true ) == "public"){
							$participants = (int)get_post_meta( $post->ID, 'participants', true );
							$nb_participant_max = get_post_meta( $post->ID, "nb_participant_max", true );
							echo '<p>';
								echo '<span class="label-like"> Nombre d\'inscrits :</span> ';
								echo $participants;
								if(!empty($nb_participant_max)):
								echo ' / '.$nb_participant_max;
								endif;
							echo '</p>';	
						}
						else if(!empty(get_post_meta( $post->ID, 'nb_participant_target', true ))){
							echo '<p>';
								echo '<span class="label-like"> Nombre de personnes ciblées ou estimées :</span> ';
								echo get_post_meta( $post->ID, 'nb_participant_target', true );
							echo '</p>';	
						}
					
						?>
						
						<!-- Email -->
						<?php 
						$user_email = get_post_meta( $post->ID, "user_email", true );
						if(!empty($user_email)):
							echo '<p><span class="label-like">Adresse e-mail :</span> '.$user_email.'</p>';
						endif;
						?>

						<!-- Phone -->
						<?php 
						$user_phone = get_post_meta( $post->ID, "user_phone", true );
						if(!empty($user_phone)):
							echo '<p><span class="label-like">Téléphone :</span> '.$user_phone.'</p>';
						endif;
						?>

						<?php 
						echo '<p><span class="label-like">ID DigitalCleanup :</span> '.get_the_ID().'</p>';
						?>
					</div>
					<div class="btn-container no-useless-margin">
						<a class="button" href="<?php the_permalink();?>" target="_blank">
							<?php _e('Visualiser', 'cwcud');?> 
						</a>

						<a class="button" href="<?php echo esc_url( add_query_arg( 'id_cleanup', $post->ID, get_permalink( get_field("page_update_cleanup", "option") ) ) );?>">
							<?php _e('Modifier', 'cwcud');?>
						</a>

						<a class="button" href="#delete-cleanup">
							<?php _e('Supprimer', 'cwcud');?> 
						</a>

						<?php if($participants > 0):?>
						<a class="button get_list_inscrits" href="#" data-id="<?php echo $post->ID;?>">
							<?php _e('Voir les inscrits', 'cwcud');?>
						</a>
						<?php endif;?>

						<?php if('publish' === get_post_status( get_field("page_form_bilan", "option") )):

                            $flamingo = get_post_meta($post->ID, 'flamingo', true);
                            ?>
                            <form action="<?php echo add_query_arg('id_cleanup', $post->ID, get_permalink(get_post(get_field("page_form_bilan", "option"))))?>" method="post">
                            <?php
                                if(!empty($flamingo)) {
                                    $post_meta = get_post_meta($flamingo);
                                    foreach($post_meta as $key => $value) {
                                        $value = $value[0];
                                        if( preg_match("_field_", $key) && $key != '_fields' ) {
                                            if(unserialize($value) != false) {
                                                foreach(unserialize($value) as $v) {
                                                    echo '<input type="hidden" name="' . str_replace('_field_', '', $key) . '[]" value="' . $v . '">';
                                                }
                                            } else {
                                                echo '<input type="hidden" name="' . str_replace('_field_', '', $key) . '" value="' . $value . '">';
                                                /*var_dump($key);
                                                var_dump($value);*/
                                            }
                                        }
                                        ?>
                                        <!--<input type="hidden" name="<?php $key ?>" value="<?php $value ?>">-->
                                        <?php
                                    }
                                }
                                ?>
                                <button class="button" type="submit">
                                    <?php _e('Formulaire bilan', 'cwcud');?>
                                </button>
                            </form>

						<?php endif;?>
					</div>
				</article>
			<?php
			endforeach; 
			wp_reset_postdata(); 
			?>

			<div id="delete-cleanup" style="margin:150px 0;">
				
				<p>
					<?php _e("Pour supprimer un DigitalCleanup, merci d'envoyer un mail à contact@digital-cleanup-day.fr avec votre numéro de DigitalCleanup.","cwcud");?>
				</p>

			</div>

		</div><!-- /listing-cleanup-organizer-view -->
		
	<div class="modale" id="modal_list_inscrits">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X <?php _e('Fermer', 'cwcud');?></p>
		<div class="embassy-modale" id="list_inscrits">
		</div>
	</div>
		
	
	<?php
	else :

		//get_template_part( 'template-parts/content', 'none' );

	endif;
	?>

</section><!-- End of Listing Archive -->

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>
