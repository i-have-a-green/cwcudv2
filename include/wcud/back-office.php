<?php

/*
function new_contact_methods( $contactmethods ) {
    $contactmethods['phone'] = 'Téléphone';
    $contactmethods['user_email'] = 'test';
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );
*/

function new_modify_user_table( $column ) {
    $column['phone'] = 'Téléphone';
    $column['nb_cleanup'] = 'Nbr cleanUp';
    unset($column['email']);
    unset($column['posts']);
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'phone' :
            return get_the_author_meta( 'user_phone', $user_id );
        case 'nb_cleanup' :
            return '<a href="'.admin_url().'edit.php?post_type=cleanup&author='.$user_id.'">'.count_user_posts( $user_id, 'cleanup' ).'</a>';
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );


add_action('admin_menu', 'ihag_user_sub_menu_role');
function ihag_user_sub_menu_role() {
    add_submenu_page(
        'users.php',
        'ambassadeur_to_validate',
        'Ambassadeurs à valider',
        'manage_options',
        'users.php?role=ambassadeur_to_validate',
    );

    add_submenu_page(
        'users.php',
        'ambassadeur',
        'Ambassadeurs',
        'manage_options',
        'users.php?role=ambassadeur',
    );

    add_submenu_page(
        'users.php',
        'organisateur',
        'Organisateurs',
        'manage_options',
        'users.php?role=organisateur',
    );
    add_submenu_page(
        'users.php',
        'map_ambassadeur',
        'Carte des ambassadeurs',
        'manage_options',
        'map_ambassadeur',
        'map_ambassadeur',
    );

    add_submenu_page(
        'tools.php',
        'exportCleanUp',
        'Export CSV cleanup',
        'manage_options',
        'exportCleanUp',
        'exportCleanUp',
    );

    /*add_submenu_page(
        'tools.php',
        'moulinette',
        'moulinette',
        'manage_options',
        'moulinette',
        'moulinette',
    );*/

    /*add_submenu_page(
        'tools.php',
        'fixmoulinette',
        'fixmoulinette',
        'manage_options',
        'fixmoulinette',
        'fixmoulinette',
    );*/

    global $submenu;
    unset($submenu[ 'users.php' ][16]);
}

function fixmoulinette(){
    global $wpdb;
    $args = array(
        'numberposts' => -1,
        'post_type'   => 'cleanup',
        'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
      );
    $cleanups = get_posts($args);
    $tabFlamingo = array();
    foreach($cleanups as $cleanup){
        $cleanup_flamingo_id = get_post_meta($cleanup->ID, "flamingo", true);
        if(!empty($cleanup_flamingo_id)){
            if(!in_array($cleanup_flamingo_id, $tabFlamingo)){
                $tabFlamingo[] = $cleanup_flamingo_id;
                $tabFlamingo_[$cleanup_flamingo_id][] = $cleanup_flamingo_id;
            }else{

                $_field_id_cleanup = get_post_meta($cleanup_flamingo_id, '_field_id_cleanup', true);

                $args = array(
                    'meta_key'   => 'flamingo',
	                'meta_value' => $cleanup_flamingo_id,
                    'post_type'  => 'cleanup'
                );
                $cleanupsDoublon = get_posts($args);
                $doublon = 0;
                foreach($cleanupsDoublon as $cleanupDoublon){
                    if($_field_id_cleanup == $cleanupDoublon->ID){
                        echo '<span style="color:green">'.$cleanupDoublon->ID.'</span> ';
                    }
                    else{
                        echo '<span style="color:red">'.$cleanupDoublon->ID.'('.$_field_id_cleanup.')</span> ';
                        //update_post_meta($cleanupDoublon->ID, 'flamingo', $_field_id_cleanup);
                        //echo '<span style="color:orange">update_post_meta('.$cleanupDoublon->ID.' - flamingo - ? )</span> ';
                    }
                }

               
                echo '<br>';
            }
        }
        
    }
}


function moulinette(){
    $args = array(
        'numberposts' => -1,
        'post_type'   => 'cleanup',
        'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')    
      );
    $cleanups = get_posts($args);
    foreach($cleanups as $cleanup){

        if(delete_post_meta($cleanup->ID, 'flamingo', '0')){
            echo 'delete_post_meta '.$cleanup->ID.'<br>';
        }
        

        $args = array(
            'numberposts' => -1,
            'post_type'   => 'flamingo_inbound',
            's' => $cleanup->ID
          );
        $flamingos = get_posts($args);
        foreach($flamingos as $flamingo){
            if(!($flamingo->ID == get_post_meta($cleanup->ID, 'flamingo', true))){
                if(update_post_meta($cleanup->ID, 'flamingo', $flamingo->ID) !== false){
                    echo 'update_post_meta '.$cleanup->ID.' flamingo ' .$flamingo->ID .'<br>';
                }
            }
            break;
        }
        
    }
}

/*add_action('show_user_profile', 'ihag_custom_user_profile_fields_organisateur');
add_action('edit_user_profile', 'ihag_custom_user_profile_fields_organisateur');
function ihag_custom_user_profile_fields_organisateur( $user ) {
    if ( in_array( 'organisateur', $user->roles, true )) {
?>
    <h2>Organisateur</h2>
    <table class="form-table">

        <tr>
            <th><label for="user_phone">Téléphone</label></th>
            <td>
                <input
                    type="text"
                    value="<?php echo esc_attr(get_user_meta($user->ID, 'user_phone', true)); ?>"
                    name="user_phone"
                    id="user_phone"
                    class="regular-text"
                >
            </td>
        </tr>
        <tr>
            <th>
                <label for="user_gender">Genre</label>
            </th>
            <td>
                <div class="form-row">
                    <div class="checkbox">
                        <input type="radio" name="user_gender" class="gender" id="user_female" value="user_female" <?php checked( get_user_meta($user->ID , 'user_gender', true ), "user_female");?> >
                        <label class="checkbox-label"  for="user_female"><?php _e('Madame', 'cwcud');?></label>
                    </div>
                    <div class="checkbox">
                        <input type="radio" name="user_gender" class="gender" id="user_male" value="user_male"  <?php checked( get_user_meta($user->ID , 'user_gender', true ), "user_male");?>>
                        <label class="checkbox-label" for="user_male"><?php _e('Monsieur', 'cwcud');?></label>
                    </div>
                </div>
            </td>
        </tr>



    </table>
<?php
    }
}*/
/*
add_action( 'personal_options_update', 'update_extra_profile_fields_organisateur' );
add_action( 'edit_user_profile_update', 'update_extra_profile_fields_organisateur' );
function update_extra_profile_fields_organisateur( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) ){
        update_user_meta( $user_id, 'user_phone', $_POST['user_phone'] );
        update_user_meta( $user_id, 'user_gender', $_POST['user_gender'] );
    }
}

*/

add_filter( 'manage_cleanup_posts_columns', 'ihag_manage_column_cleanup',998 );
function ihag_manage_column_cleanup($columns) {
   /* unset( $columns['date'] );
    unset( $columns['wpseo-score'] );
    unset( $columns['wpseo-score-readability'] );
    unset( $columns['wpseo-links'] );
    unset( $columns['wpseo-linked'] );*/
    $columns = array();
    $columns['cb'] = "cb";
    $columns['id_cleanup'] = "ID";
    $columns['organisateur'] = "Organisateur";
    $columns['type_structure'] = "Type";
    $columns['structure_name'] = "Nom Organisation";
    $columns['name'] = "Nom du DigitalCleanup";
    $columns['date_cleanup'] = "Date";
    $columns['cleanup_area'] = "Zone";
    $columns['participants'] = "Participants";
    $columns['bilan'] = "Bilan";
    $columns['update'] = "";
    $columns['update'] = "";
    //$columns['publisher'] = __( 'Publisher', 'your_text_domain' );

    return $columns;
}

add_filter( 'manage_edit-cleanup_sortable_columns', 'my_set_sortable_columns',999 );
function my_set_sortable_columns( $columns )
{   
    $columns['id_cleanup'] = "ID";
    unset($columns['organisateur'] );
    unset($columns['bilan'] );
    $columns['type_structure'] = "type_structure";
    $columns['structure_name'] = "structure_name";
    $columns['name'] = "name";
    $columns['date_cleanup'] = "date_cleanup";
    $columns['cleanup_area'] = "cleanup_area";
    return $columns;
}

// Administration: Teach WordPress to make the column sortable
function anco_project_year_column_orderby( $vars ) {
    if(isset($_GET['post_type']) && $_GET['post_type'] == "cleanup"){
        if ( isset( $vars['orderby'] ) && 'Type' == $vars['orderby'] ) {
                $vars = array_merge( $vars, array(
                        'meta_key' => 'type_structure',
                        'orderby' => 'meta_value'
                ) );
        } else if ( isset( $vars['orderby'] ) && 'Zone' == $vars['orderby'] ) {
                $vars = array_merge( $vars, array(
                        'meta_key' => 'cleanup_area',
                        'orderby' => 'meta_value_num'
                ) );
        } else if ( isset( $vars['orderby'] ) && 'Nom Organisation' == $vars['orderby'] ) {
            $vars = array_merge( $vars, array(
                    'meta_key' => 'structure_name',
                    'orderby' => 'meta_value'
            ) );
        } else if ( isset( $vars['orderby'] ) && 'Date' == $vars['orderby'] ) {
            $vars = array_merge( $vars, array(
                    'meta_key' => 'date_start',
                    'orderby' => 'meta_value'
            ) );
        } else if ( isset( $vars['orderby'] ) && 'Nom du DigitalCleanup' == $vars['orderby'] ) {
            $vars['orderby'] = 'title';
        }
        /*else if ( isset( $vars['orderby'] ) && 'Bilan' == $vars['orderby'] ) {
            $vars = array_merge( $vars, array(
                    'meta_key'  => 'flamingo',
                    'orderby'   => 'meta_value'
            ) );
        }*/
    }
    return $vars;
}
add_filter( 'request', 'anco_project_year_column_orderby' );

// Add the data to the custom columns for the book post type:
add_action( 'manage_cleanup_posts_custom_column' , 'ihag_custom_cleanup_column', 10, 2 );
function ihag_custom_cleanup_column( $column, $post_id ) {
    $post = get_post($post_id);
    $author_id = $post->post_author;
    $user = get_userdata( $author_id );

    switch ( $column ) { 
        case 'organisateur' :
            echo "<a href=\"".admin_url()."user-edit.php?user_id=".$author_id."\">".$user->first_name." ".$user->last_name." (".$author_id.")</a>";
            break;
        case 'id_cleanup' :
            echo $post_id; 
            break;
        case 'bilan' :
            $flamingo = get_post_meta($post_id, 'flamingo', true);
            if(!empty($flamingo)) {
                $flamingo_link = 'admin.php?page=flamingo_inbound&post=' . get_post_meta($post_id, 'flamingo', true) . '&action=edit';
                echo '<a href="' . get_admin_url('', $flamingo_link, '') . '">renseigné</a>';
            }
            break;
        case 'type_structure' :
            $whichStructureChecked = get_post_meta($post_id, "type_structure", true);
            if($whichStructureChecked == "citizen"){
                echo "Citoyen";
            }
            elseif($whichStructureChecked == "association"){
                echo "Association";
            }
            elseif($whichStructureChecked == "school"){
                echo "École";
            }
            elseif($whichStructureChecked == "collectivity"){
                echo "Collectivité";
            }
            elseif($whichStructureChecked == "company"){
                echo "Entreprise";
            }
            break;
        case 'structure_name' :
            echo get_post_meta($post_id,'structure_name', true); 
            break;
        case 'name' :
            the_title(); 
            break;
        case 'date_cleanup' :
            echo "le ".date_i18n('j/m', strtotime(get_post_meta( $post_id, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta( $post_id, "time_start", true )));
            break; 
        case 'cleanup_area' :
            //$tab_departements = arrayDepartements();
            echo get_geo_area(get_post_meta( $post_id, "cleanup_area", true ));
            break;  
        case 'participants' :
            //$participants = (int)get_post_meta( $post_id, 'participants', true );
            $participants = ((int)get_post_meta( get_the_id(), 'participants', true ) + (int)get_post_meta( get_the_id(), 'nb_participant_target', true ));
			//$nb_participant = get_post_meta( $post_id, "nb_participant_max", true );
			echo $participants;
			if(!empty($nb_participant)){
			//	echo ' / '.$nb_participant; 
			}
            break;  
        case 'update' :
            $tab_arg = array(
                'id_cleanup' => $post_id,
                'imadmin' => 1,
            );
            echo '<a href="'.esc_url( add_query_arg( $tab_arg, get_permalink( get_field("page_update_cleanup", "option") ) ) ).'">Modifier</a>';
            break;  
    }
}

add_action( 'restrict_manage_posts', 'ihag_admin_posts_filter_restrict_manage_posts' );
function ihag_admin_posts_filter_restrict_manage_posts(){
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    //only add filter to post type you want
    if ('cleanup' == $type){
        $current_v = isset($_GET['cleanup_area'])? $_GET['cleanup_area']:'';
        select_geo_area($current_v, 'cleanup_area', ''); 
        
        $whichStructureChecked = array();
        $whichStructureChecked["citizen"] = "Citoyen";
        $whichStructureChecked["association"] = "Association";
        $whichStructureChecked["school"] = "École";
        $whichStructureChecked["collectivity"] = "Collectivité";
        $whichStructureChecked["company"] = "Entreprise";
        ?>
        <select name="type_structure">
        <option value="">Type de structure</option>
        <?php
            $current_v = isset($_GET['type_structure'])? $_GET['type_structure']:'';
            foreach ($whichStructureChecked as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $label,
                        $label == $current_v? ' selected="selected"':'',
                        $value
                    );
                }
        ?>
        </select>
        <select name="bilan">
            <option value="">Bilan</option>
            <?php
                $current_v_bilan = isset($_GET['bilan'])? $_GET['bilan']:'';
            ?>
            <option value="true" <?php @ihag_selected($_GET['bilan'], 'true'); if($current_v_bilan === true){echo ' selected="selected"';} ?>>Bilan renseigné</option>
            <option value="false" <?php @ihag_selected($_GET['bilan'], 'false'); if($current_v_bilan === false){echo ' selected="selected"';}?>>Bilan non renseigné</option>
        </select>
        <?php
        $whichCatChecked["data"] = "Digital Cleanup Données";
        $whichCatChecked["reuse"] = "Digital Cleanup Réemploi";
        $whichCatChecked["hardware"] = "Digital Cleanup Recyclage";
        ?>
        <select name="cat_cybercleanup">
            <option value="">Catégorie de Cleanup</option>
            <?php
                $current_v_cat = isset($_GET['cat_cybercleanup'])? $_GET['cat_cybercleanup']:'';
                foreach ($whichCatChecked as $label => $value) {
                    printf
                        (
                            '<option value="%s"%s>%s</option>',
                            $label,
                            $label == $current_v_cat? ' selected="selected"':'',
                            $value
                        );
                    }
            ?>
        </select>
        <input type="text" name="structure_name" <?php echo (isset($_GET['structure_name']) && $_GET['structure_name'] != '') ? 'value="' . $_GET['structure_name'] . '"' : 'placeholder="Nom de l\'organisation"' ?>>
        <input type="text" name="cleanup_name" <?php echo (isset($_GET['cleanup_name']) && $_GET['cleanup_name'] != '') ? 'value="' . $_GET['cleanup_name'] . '"' : 'placeholder="Nom du DigitalCleanup"' ?>>
        <?php
    }
}

function ihag_query_property( $wp_query ) {
    if (($wp_query->get('post_type') === "cleanup") ) {
        if(is_admin()){
            if(is_numeric(get_search_query())){
                $wp_query->set( 'p', get_search_query() );
                $wp_query->set( 's', '');
            }
        }
    }
}
//add_action( 'pre_get_posts','ihag_query_property' );


/*add_filter( 'parse_query', 'wpse45436_posts_filter' );
function wpse45436_posts_filter( $query ){
    global $pagenow;
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    $meta_query = array();

    if (($type === "cleanup") ) {
        if(is_admin()){
            if(is_numeric(get_search_query())){
                $query->set( 'p', get_search_query() );
                $query->set( 's', '');
            }
        }
    }

    if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cleanup_area']) && $_GET['cleanup_area'] != '') {
        if($_GET['cleanup_area'] == 'fr' || $_GET['cleanup_area'] == 'be' || $_GET['cleanup_area'] == 'ch'){
            $meta_query[] = array(
                'key' => 'localisation',
                'value' => sanitize_text_field($_GET['cleanup_area']),
            );
        }
        else {
            $meta_query[] = array(
                'key' => 'cleanup_area',
                'value' => sanitize_text_field($_GET['cleanup_area']),
            );
        }
    }
    if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['type_structure']) && $_GET['type_structure'] != '') {
        $meta_query[] = array(
            'key' => 'type_structure',
            'value' => sanitize_text_field($_GET['type_structure']),
        );
    }
    if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['bilan']) && $_GET['bilan'] == 'true') {
        $meta_query[] = array(
            'key' => 'flamingo',
            'compare' => 'EXISTS',
        );
    }
    else if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['bilan']) && $_GET['bilan'] == 'false') {
        $meta_query[] = array(
            'key' => 'flamingo',
            'compare' => 'NOT EXISTS',
        );
    }
    if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cat_cybercleanup']) && $_GET['cat_cybercleanup'] != '') {
        $meta_query[] = array(
            'key' => 'cat_cybercleanup',
            'value' => sanitize_text_field($_GET['cat_cybercleanup']),
        );
    }

    if(count($meta_query) > 1) {
        $meta_query['relation'] = 'AND';
    }

    $query->query['meta_query'] = $meta_query;
    $query->query['post_type'] = $type;
    $query->query['post_type'] = $type;
}
*/

add_filter('parse_query', 'wpse45436_posts_filter');
function wpse45436_posts_filter($query) {
    global $pagenow;
    $type = isset($_GET['post_type']) ? $_GET['post_type'] : 'post';

    if(is_admin() && $type == 'cleanup' && $pagenow == 'edit.php'){
        // array for meta_query
        $meta_query = array();

        if(is_numeric(get_search_query())){
            $query->set( 'p', get_search_query() );
            $query->set( 's', '');
        }
        else {
            /*$meta_query[] = array(
                'key' => 'structure_name',
                'value' => sanitize_text_field($_GET['s']),
            );*/

            /*$query->query_vars['meta_key'] = 'structure_name';
            $query->query_vars['meta_value'] = $_GET['s'];
            $query->query_vars['meta_compare'] = 'LIKE';*/
        }

        /*if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['s']) && $_GET['s'] != '') {
            $meta_query[] = array(
                'key' => 'structure_name',
                'value' => sanitize_text_field($_GET['s']),
                'compare' => 'LIKE',
            );
        }*/

        // add search value
        if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cleanup_area']) && $_GET['cleanup_area'] != '') {
            if($_GET['cleanup_area'] == 'fr' || $_GET['cleanup_area'] == 'be' || $_GET['cleanup_area'] == 'ch'){
                $meta_query[] = array(
                    'key' => 'localisation',
                    'value' => sanitize_text_field($_GET['cleanup_area']),
                );
            }
            else {
                $meta_query[] = array(
                    'key' => 'cleanup_area',
                    'value' => sanitize_text_field($_GET['cleanup_area']),
                );
            }
        }
        if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['type_structure']) && $_GET['type_structure'] != '') {
            $meta_query[] = array(
                'key' => 'type_structure',
                'value' => sanitize_text_field($_GET['type_structure']),
            );
        }
        if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['bilan']) && $_GET['bilan'] == 'true') {
            $meta_query[] = array(
                'key' => 'flamingo',
                'compare' => 'EXISTS',
            );
        }
        else if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['bilan']) && $_GET['bilan'] == 'false') {
            $meta_query[] = array(
                'key' => 'flamingo',
                'compare' => 'NOT EXISTS',
                //'value' => '0',
            );
        }
        if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cat_cybercleanup']) && $_GET['cat_cybercleanup'] != '') {
            $meta_query[] = array(
                'key' => 'cat_cybercleanup',
                'value' => sanitize_text_field($_GET['cat_cybercleanup']),
            );
        }
        if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cleanup_name']) && $_GET['cleanup_name'] != '') {
            $meta_query[] = array(
                'key' => 'cleanup_name',
                'value' => sanitize_text_field($_GET['cleanup_name']),
                'compare' => 'LIKE',
            );
        }
        if ( 'cleanup' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['structure_name']) && $_GET['structure_name'] != '') {
            $meta_query[] = array(
                'key' => 'structure_name',
                'value' => sanitize_text_field($_GET['structure_name']),
                'compare' => 'LIKE',
            );
        }

        // add relation if there is more than 1 argument
        if(count($meta_query) > 1) {
            $meta_query['relation'] = 'AND';
        }

        // add content of the $meta_query to the current query
        $query->query_vars['meta_query'] = $meta_query;
    }
}
add_filter('months_dropdown_results', '__return_empty_array');

// Add custom text/textarea attachment field
function add_custom_text_field_to_attachment_fields_to_edit( $form_fields, $post ) {
    $text_field = get_post_meta($post->ID, 'text_field', true);
    $form_fields['text_field'] = array(
        'label' => 'Custom text field',
        'input' => 'text', // you may also use 'textarea' field
        'value' => $text_field,
        'helps' => 'This is help text'
    );
    return $form_fields;
}
add_filter('attachment_fields_to_edit', 'add_custom_text_field_to_attachment_fields_to_edit', null, 2); 

// Save custom text/textarea attachment field
function save_custom_text_attachment_field($post, $attachment) {  
    if( isset($attachment['text_field']) ){  
        update_post_meta($post['ID'], 'text_field', sanitize_text_field( $attachment['text_field'] ) );  
    }else{
        delete_post_meta($post['ID'], 'text_field' );
    }
    return $post;  
}
add_filter('attachment_fields_to_save', 'save_custom_text_attachment_field', null, 2);


// Add custom checkbox attachment field
function add_custom_checkbox_field_to_attachment_fields_to_edit( $form_fields, $post ) {
    $checkbox_field = (bool) get_post_meta($post->ID, 'child', true);
    $form_fields['child'] = array(
        'label' => 'Checkbox',
        'input' => 'html',
        'html' => '<input type="checkbox" id="attachments-'.$post->ID.'-child" name="attachments['.$post->ID.'][child]" value="1"'.($checkbox_field ? ' checked="checked"' : '').' /> ',
        'value' => $checkbox_field,
        'helps' => ''
    );
    return $form_fields;
}
add_filter('attachment_fields_to_edit', 'add_custom_checkbox_field_to_attachment_fields_to_edit', null, 2); 

// Save custom checkbox attachment field
function save_custom_checkbox_attachment_field($post, $attachment) {  
    if( isset($attachment['child']) ){  
        update_post_meta($post['ID'], 'child', sanitize_text_field( $attachment['child'] ) );  
    }else{
         delete_post_meta($post['ID'], 'child' );
    }
    return $post;  
}
add_filter('attachment_fields_to_save', 'save_custom_checkbox_attachment_field', null, 2);


add_filter('acf/settings/remove_wp_meta_box', '__return_false');


function exportCleanUp(){
    ?>
    <div class="wrap">
        <h1>Export des cleanups</h1>
        <form action="" method="get">
            <input type="hidden" name="report" value="exportCleanUp">
            <select name="flamingo" id="flamingo">
                <option value="" selected>Bilan</option>
                <option value="true" >Bilan renseigné</option>
                <option value="false" >Bilan non-renseigné</option>
            </select>
            <select name="cat_cybercleanup">
                <option value="" selected>Catégorie de Digital Cleanup</option>
                <option value="data">Digital Cleanup Données</option>
                <option value="reuse">Digital Cleanup Reemploi</option>
                <option value="hardware">Digital Cleanup Recyclage</option>
            </select>
            <select name="localisation">
                <option value="" selected>Pays</option>
                <option value="fr">France</option>
                <option value="be">Belgique</option>
                <option value="ch">Suisse</option>
            </select>
            <button type="submit" class="button">Export</button>
        </form>
        <br>
        <!--<a href="?report=exportCleanUp" class="button">Export</a>-->
        <!--<a href="?report=removeCleanUp" class="button">Remove all cleanup</a>-->
        <!--<p><a href="?page=exportCleanUp&cleanupPublic=cleanupPublic" class="button">cleanup Public</a></p>-->
    <?php
}

add_action('init', function() {
    if(isset($_GET['report']) && $_GET['report'] == 'exportCleanUp'){
        exportCSVCleanUp();
    }
    /* if(isset($_GET['report']) && $_GET['report'] == 'removeCleanUp'){
        $posts = get_posts(array(
            "post_type" => "cleanup",
            "numberposts" => -1
        ));
        foreach($posts as $cleanup){
            wp_delete_post($cleanup->ID, true);
        }

    }*/
});



function exportCSVCleanUp(){
    global $wpdb, $post;
    $csv_fields=array();
    $csv_fields[] = 'ID';
    $csv_fields[] = 'Cleanup';
    $csv_fields[] = 'etat';
    $csv_fields[] = 'format';
    $csv_fields[] = 'Organisateur';
    $csv_fields[] = 'Type de structure';
    $csv_fields[] = 'Nom de la structure';
    $csv_fields[] = 'Pays';
    $csv_fields[] = 'Zone géographique';
    $csv_fields[] = 'email Orga';
    $csv_fields[] = 'Tèl. Orga';

    $csv_fields[] = 'Twitter';
    $csv_fields[] = 'Facebook';
    $csv_fields[] = 'Linkedin';
    $csv_fields[] = 'Instagram';

    $csv_fields[] = 'Action';
    $csv_fields[] = 'Collecte';
    $csv_fields[] = 'Qte';

    $csv_fields[] = 'Sensibilisation';
    $csv_fields[] = 'Animateur';

    $csv_fields[] = 'Date';
    $csv_fields[] = 'Heure';
    $csv_fields[] = 'Nombre de jour(s)';
    $csv_fields[] = 'Participants';

    $csv_fields[] = 'Catégorie';

    $form_ID = get_field('bilan_formulaire', 'option');
    $ContactForm = WPCF7_ContactForm::get_instance( $form_ID );
    $form_fields = $ContactForm->scan_form_tags();
    foreach($form_fields as $field){
        if(!empty($field->raw_name) && $field->raw_name != 'id_cleanup'){
            $csv_fields[] = str_replace('-', ' ', $field->raw_name);
        }
    }

    $output_filename = "cleanup_".date("Y-m-d H:i:s").'.csv';
    $output_handle = @fopen( 'php://output', 'w' );
    header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
    header( 'Content-Description: File Transfer' );
    header( 'Content-type: text/csv' );
    header( 'Content-Disposition: attachment; filename=' . $output_filename );
    header( 'Expires: 0' );
    header( 'Pragma: public' );
    // Insert header row
    fputcsv( $output_handle, $csv_fields,";" );

    $meta_query = array();

    if(isset($_GET['flamingo']) && !empty($_GET['flamingo'])) {
        if($_GET['flamingo'] == 'false') {
            $meta_query[] = array(
                'key' => 'flamingo',
                'compare' => 'NOT EXISTS',
            );
        }
        else {
            $meta_query[] = array(
                'key' => 'flamingo',
                'compare' => '>',
                'value' => '0',
            );
        }
    }

    if(isset($_GET['cat_cybercleanup']) && !empty($_GET['cat_cybercleanup'])) {
        $meta_query[] = array (
                'key' => 'cat_cybercleanup',
                'value' => $_GET['cat_cybercleanup'],
        );
    }

    if(isset($_GET['localisation']) && !empty($_GET['localisation'])) {
        $meta_query[] = array (
                'key' => 'localisation',
                'value' => $_GET['localisation'],
        );
    }

    if(count($meta_query) > 1) {
            $meta_query['relation'] = 'AND';
    }

    $args = array(
        'post_type' => 'cleanup',
        'post_status' => array('publish', 'private'),
        'meta_query' => $meta_query,
    );


    $my_query = new WP_Query( $args );
    if($my_query->have_posts()) {
        while($my_query->have_posts()) {
            $my_query->the_post();


            $author_id = get_post_field( 'post_author', get_the_id() );
            $user = get_userdata( $author_id );
            /*$status = get_post_status($post);
            if($status == "publish"){
                $status = "privée";
                $private = get_post_meta( $post->ID, "private", true );
                if($private == "no_private"){
                    $status = "public";
                }
            }
            else{
                $status = "secret";
            }*/
            
            $whichLocation = get_post_meta( get_the_id(), "location", true ) ;
            if ($whichLocation === "location_facetoface"):
                $location = "présentiel";
            elseif($whichLocation === "location_distancing"):
                $location = "distanciel";
            elseif($whichLocation === "location_both"):
                $location = "les deux";
            endif;

            $whichStructureChecked = get_post_meta(get_the_id(), "type_structure", true);
            if($whichStructureChecked == "citizen"){
                $structure = "Citoyen";
            }
            elseif($whichStructureChecked == "association"){
                $structure = "Association";
            }
            elseif($whichStructureChecked == "school"){
                $structure = "École";
            }
            elseif($whichStructureChecked == "collectivity"){
                $structure = "Collectivité";
            }
            elseif($whichStructureChecked == "company"){
                $structure = "Entreprise";
            }

            $cat = '';
            $nettoyage = '';
            $collect = '';
            /* if(get_post_meta( get_the_id(), "cat_cybercleanup", true ) == "data"):
                $cat = __('Nettoyage Données', 'cwcud');
                $data_cat = get_post_meta( get_the_id(), "data_cat", true );

                $nettoyage .= (is_array($data_cat) && in_array("data_desk", $data_cat))?''.__('Fichiers bureautiques', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("smartphone", $data_cat))?''.__('Smartphone', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("email", $data_cat))?''.__('Email', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("social", $data_cat))?''.__('Réseaux sociaux', 'cwcud').' - ':'';
                $other = get_post_meta(get_the_id(), "data_cat_other", true );
                if(!empty($other)):
                    $nettoyage .= ''.$other.' - ';
                endif;
            else:
                $cat = __('Seconde vie des équipements', 'cwcud');
                $data_cat = get_post_meta( get_the_id(), "hard_cat", true );

                $nettoyage .= (is_array($data_cat) && in_array("smartphones", $data_cat))?''.__('Smartphones (fonctionnels)', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("PC", $data_cat))?''.__('PC portables & tablettes (fonctionnels)', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("DEEE", $data_cat))?''.__('DEEE numérique', 'cwcud').' - ':'';
                $other = get_post_meta(get_the_id(), "hard_cat_other", true );
                if(!empty($other)):
                    $nettoyage .= ''.$other.' - ';
                endif;

                if("hard_orga_no_partner" == get_post_meta(get_the_id(), "hard_collect", true )){
                    $collect .= __('Au sein de l’organisation avec les collecteurs habituels de celle-ci', 'cwcud');
                }
                elseif("hard_orga_with_partner" == get_post_meta(get_the_id(), "hard_collect", true )){
                    $collect .= __('Au sein de l’organisation avec un collecteur partenaire DigitalCleanupDay', 'cwcud');
                }
                else if("hard_no_orga" == get_post_meta(get_the_id(), "hard_collect", true )){
                    $collect .= __('Dans un point d’apport local non-référencé DigitalCleanupDay : ', 'cwcud');
                    $collect .= ' '.get_post_meta(get_the_id(), "hard_collect_other", true );
                }
            endif; */

            if(get_post_meta( get_the_id(), "cat_cybercleanup", true ) == "data") {
                $cat = __('Cleanup Données', 'cwcud');
                $data_cat = get_post_meta( get_the_id(), "data_cat", true );
                $other = get_post_meta(get_the_id(), "data_cat_other", true );

                $nettoyage .= (is_array($data_cat) && in_array("data_desk", $data_cat))?''.__('Nettoyage de données sur ordinateurs', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("smartphone", $data_cat))?''.__('Nettoyage de données sur smartphones', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("cloud", $data_cat))?''.__('Nettoyage de données sur cloud', 'cwcud').' - ':'';
                if(!empty($other)) {
                    $nettoyage .= 'Nettoyage de données sur : '.$other.' - ';
                }
            }
            else if(get_post_meta( get_the_id(), "cat_cybercleanup", true ) == "reuse") {
                $cat = __('Cleanup Réemploi', 'cwcud');
                $data_cat = get_post_meta( get_the_id(), "reuse_cat", true );

                $nettoyage .= (is_array($data_cat) && in_array("reuse_smartphone", $data_cat))?''.__('Smartphones (sensibilisation)', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("reuse_fix", $data_cat))?''.__('Réparation d\'ordinateurs, de smartphones', 'cwcud').' - ':'';
                $nettoyage .= (is_array($data_cat) && in_array("reuse_collect", $data_cat))?''.__('Collecte de smartphone pour seconde vie', 'cwcud').' - ':'';
            }
            else if(get_post_meta( get_the_id(), "cat_cybercleanup", true ) == "hardware") {
                $cat = __('Cleanup Recyclage', 'cwcud');
                $data_cat = get_post_meta( get_the_id(), "hard_cat", true );

                if("hard_orga_with_partner" == get_post_meta(get_the_id(), "hard_collect", true )){
                    $collect .= __('Collecte sans partenaire', 'cwcud');
                }
                else if("hard_no_orga" == get_post_meta(get_the_id(), "hard_collect", true )){
                    $collect .= __('Collecte avec le partenaire :', 'cwcud');
                    $collect .= ' '.get_post_meta(get_the_id(), "hard_collect_other", true );
                }
            }

            $sensibilisation = '';
            $animateur = '';
            if(get_post_meta( get_the_id(), "animate_cybercleanup", true ) == "orga"):
                $sensibilisation = __('L’organisateur ou personne interne à l’organisation','cwcud');
            elseif(get_post_meta( get_the_id(), "animate_cybercleanup", true ) == "reference_cwcud"):
                $sensibilisation = __('Animateur NR partenaire référencé DigitalCleanupDay  : ','cwcud');
                $animateur =  get_post_meta( get_the_id(), "animator_cwcud", true );
            elseif(get_post_meta( get_the_id(), "animate_cybercleanup", true ) == "not_reference_cwcud"):
                $sensibilisation = __('Animateur non-référencé DigitalCleanupDay : ','cwcud');
                $animateur =  get_post_meta( get_the_id(), "animator_not_cwcud", true );
            endif;

            $tab_data = array( 
                get_the_id(),
                get_the_title(), 
                get_post_meta(get_the_id(),'visibility', true),
                $location,
                $user->first_name." ".$user->last_name,
                $structure,
                get_post_meta(get_the_id(),'structure_name', true),
                get_post_meta(get_the_id(),'localisation', true),
                get_post_meta(get_the_id(),'cleanup_area', true),
                $user->user_email,
                get_post_meta(get_the_id(),'user_phone', true),
                

                get_post_meta(get_the_id(),'user_twitter', true),
                get_post_meta(get_the_id(),'user_facebook', true),
                get_post_meta(get_the_id(),'user_linkedin', true),
                get_post_meta(get_the_id(),'user_instagram', true),

                $nettoyage,

                $collect,
                substr(get_post_meta(get_the_id(),'hard_volume', true), 12),

                $sensibilisation,
                $animateur,
                date_i18n('j/m/y', strtotime(get_post_meta( get_the_id(), "date_start", true ))),
                date_i18n('H:i', strtotime(get_post_meta( get_the_id(), "time_start", true ))),
                (int)get_post_meta( get_the_id(), 'during_day', true ),
                ((int)get_post_meta( get_the_id(), 'participants', true ) + (int)get_post_meta( get_the_id(), 'nb_participant_target', true )),
                $cat
            );
            $flamingo_id = get_post_meta(get_the_id(), "flamingo", true);
            
            foreach($form_fields as $field){
                if(!empty($field->raw_name) && $field->raw_name != 'id_cleanup'){
                    $value = get_post_meta($flamingo_id, '_field_'.$field->raw_name, true);
                    if(is_array($value)){
                        array_push($tab_data, implode(' - ',$value));
                    }
                    else {
                        array_push($tab_data, $value);
                    }
                } 
            }
            fputcsv( $output_handle, $tab_data,";");
        }
    }
    
    fclose( $output_handle );
    exit();
}

add_filter( 'manage_pages_columns', 'page_column_views' );
add_action( 'manage_pages_custom_column', 'page_custom_column_views', 5, 2 );
function page_column_views( $defaults )
{
    $defaults['page-layout'] = __('Template', 'textdomain');
    return $defaults;
}
function page_custom_column_views( $column_name, $id )
{
    if ( $column_name === 'page-layout' ) {
        $set_template = get_post_meta( get_the_ID(), '_wp_page_template', true );
        if ( $set_template == 'default' ) {
            echo __('Default Template', 'textdomain');
        }
        $templates = get_page_templates();
        ksort( $templates );
        foreach ( array_keys( $templates ) as $template ) :
            if ( $set_template == $templates[$template] ) echo $template;
        endforeach;
    }
}

// Modifier le logo sur la page de connexion à l'administration
function wpm_login_style() { ?>
    <style type="text/css">
    #login{
    width:491px !important;
    }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/image/logo-digital-cleanup-day.png);
            background-size: 494px 121px;
            width: 494px;
            height: 121px; 
        }
		/*Vous pouvez ajouter d'autres styles CSS ici */
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'wpm_login_style' );

/*
add_action('wpcf7_mail_sent', function ($cf7) {
    update_post_meta($_POST['_wpcf7_container_post'], 'bilan', 'true');
}); */
