<?php
use \Mailjet\Resources;
/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'cwcud', 'new-orga',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_new_orga',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'cwcud', 'update-orga',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_update_orga',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	/*register_rest_route( 'cwcud', 'ask-ambassadeur',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_ask_ambassadeur',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);*/
	register_rest_route( 'cwcud', 'new-cleanup',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_new_cleanup',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	register_rest_route( 'cwcud', 'search-address',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wcud_search_address',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
	/*register_rest_route( 'cwcud', 'update-cleanup',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_update_cleanup',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);*/
	register_rest_route( 'cwcud', 'registerUserCleanUp',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_registerUserCleanUp',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	register_rest_route( 'cwcud', 'nbRegisterUserCleanUp',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_nbRegisterUserCleanUp',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'cwcud', 'list-inscrits',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_list_inscrits',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);

	register_rest_route( 'cwcud', 'info-orga',
		array(
		'methods' 				=> 'POST', //WP_REST_Server::READABLE,
		'callback'        		=> 'wcud_info_orga',
		'permission_callback' 	=> array(),
		'args' 					=> array(),
		)
	);
	register_rest_route( 'cwcud', 'bilanForm',
	array(
	'methods' 				=> 'POST', //WP_REST_Server::READABLE,
	'callback'        		=> 'wcud_bilanForm',
	'permission_callback' 	=> array(),
	'args' 					=> array(),
	)
);
	
	
});

function wcud_new_orga(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {
			
			// Création de l’utilisateur
			$infos = array(
				'user_login'   	=> sanitize_email($_POST['user_email']),
				'user_email'   	=> sanitize_email($_POST['user_email']),
				'user_pass'    	=> sanitize_text_field($_POST['user_pwd']),
				'role'         	=> 'organisateur',
				'display_name' 	=> sanitize_text_field( $_POST['user_email'] ),
				'first_name'	=> sanitize_text_field( $_POST['user_firstname'] ),
				'last_name'	=> sanitize_text_field( $_POST['user_lastname'] ),
			);
			$user_id = wp_insert_user( $infos );
			
			if ( is_wp_error( $user_id ) ) { 
				return new WP_REST_Response( '', 304 );
			}

			// add user meta
			/*add_user_meta($user_id, 'user_firstname', sanitize_text_field( $_POST['user_firstname'] ));
			add_user_meta($user_id, 'user_lastname', sanitize_text_field( $_POST['user_lastname'] ));*/
			//add_user_meta($user_id, 'user_gender', sanitize_text_field( $_POST['user_gender'] ));
			//add_user_meta($user_id, 'user_phone', sanitize_text_field( $_POST['user_phone'] ));

			$creds = array();
			$creds['user_login']    = sanitize_email($_POST['user_email']);
			$creds['user_password'] = sanitize_text_field($_POST['user_pwd']);
			$user = wp_signon( $creds, false );
		
			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			$to = sanitize_email($_POST['user_email']);
			/*$subject = __("Votre inscription en tant qu'organisateur pour le Digital Cleanup Day a été enregistrée",'cwcud');
			$body = "Bonjour ".$_POST['user_firstname']." ".$_POST['user_lastname']."<br><br>
	Vous venez de vous inscrire en tant qu'organisateur pour le Digital Cleanup Day.<br><br>
	Vous pouvez désormais <a href=".get_the_permalink(get_field('page_add_cleanup', 'options')).">organiser un DigitalCleanup</a><br>
	Les coordonnées communiquées sont :<br>
	● ".sanitize_email($_POST['user_email'])."<br>";
	//● ".sanitize_text_field( $_POST['user_phone'] )."<br><br>";
	/*Les identifiants de connexion sont :<br>
	● ".sanitize_email($_POST['user_email'])."<br>
	● ".sanitize_text_field($_POST['user_pwd'])."<br><br>
		$body .= "Sachez que vous pouvez à tout moment <a href=".get_the_permalink(get_field('page_my_account','option')).">modifier votre mot de passe</a> . <br><br>";
	/*Nous organisons un webinaire hebdomadaire tous les vendredi de 12h00 à 13h00 pour répondre à vos questions et partager les bonnes pratiques entre organisateurs. Retrouvez-vous sur le lien ci-dessous :<br> 
	● https://us02web.zoom.us/j/84318996776?pwd=SUhlNWtvVEZmWXVKdW5ZSWxveTcwdz09<br>
	● ID de réunion : 843 1899 6776<br>
	● Code secret : 838762<br><br>
	$body .= "Nous ne manquerons pas de vous contacter dans les plus brefs délais afin de valider avec vous ce rôle si gratifiant et porteur d'énergie positive.<br><br>
	Numériquement Vôtre,<br>
	l'équipe du Digital Cleanup Day<br><br>
	Vous recevez ce mail car vous prenez part au Digital Cleanup Day 2022 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@digital-cleanup-day.fr";*/
			
			$subject = get_field('email_sign_in_object', 'option');
			
			$body = sprintf(
				nl2br(get_field('email_sign_in_content', 'option')), 
				$_POST['user_firstname'],
				$_POST['user_lastname'],
				$_POST['user_email'],
				get_the_permalink(get_field('page_add_cleanup', 'options')),
				get_the_permalink(get_field('page_my_account','option'))
			);
		
			wp_mail( $to, $subject, $body, $headers);
			require dirname(__DIR__, 2).'/vendor/autoload.php';

			$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
			$body = [
			'Action' => "addnoforce",
			'Contacts' => [
					[
					'Email' => sanitize_email($_POST['user_email']),
					'IsExcludedFromCampaigns' => "false",
					'Name' => sanitize_email($_POST['user_email']),
					'Properties' => "object"
					]
				]
			];
			$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste_organisateur', 'option'), 'body' => $body]);
			$response->success() && var_dump($response->getData());
			return new WP_REST_Response( '', 200 );
		}    
	}
	return new WP_REST_Response( '', 304 );
}
function wcud_update_orga(WP_REST_Request $request){

	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

			$current_user = wp_get_current_user();
			//update_user_meta( $current_user->ID, 'user_gender', sanitize_text_field( $_POST['user_gender'] ) );
			update_user_meta( $current_user->ID, 'last_name',  sanitize_text_field( $_POST['user_lastname'] ) );
			update_user_meta( $current_user->ID, 'first_name',  sanitize_text_field( $_POST['user_firstname'] ) );
			//update_user_meta( $current_user->ID, 'user_phone',  sanitize_text_field( $_POST['user_phone'] ) );

			if(isset($_POST['user_pwd']) && (!empty($_POST['user_pwd']))){
				wp_set_password($_POST['user_pwd'], $user_connect->ID);
				$creds = array();
				$creds['user_login']    = $current_user->user_email;
				$creds['user_password'] = $_POST['user_pwd'];
				$user = wp_signon( $creds, false );
			}

			return new WP_REST_Response( '', 200 );
		}
	}
	return new WP_REST_Response( '', 304 );
}

function wcud_new_cleanup(WP_REST_Request $request){
	if ( check_nonce() ) {
		$current_user = wp_get_current_user();
		if (empty($_POST['honeyPot'])) {
			$post['post_type']   = 'cleanup';
			if(isset($_POST['visibility']) && $_POST['visibility'] == "secret"){
				$post['post_status'] = 'private';
			}
			else{
				$post['post_status'] = 'publish';
			}
			$post['post_title'] = sanitize_text_field($_POST['cleanup_name']);
			$post['post_content'] = sanitize_textarea_field( $_POST['cleanup_description']);

			if(isset($_POST['id']) && !empty($_POST['id'])){
				$post['ID'] = $_POST['id'];
				wp_update_post($post);
				ihag_update_post_meta($_POST['id']);
				return new WP_REST_Response( '', 200 );	
			}
			else{
				$post['post_author'] = $current_user->ID;

				$post_id = wp_insert_post( $post, true );
				ihag_update_post_meta($post_id);

				$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
				$to = $current_user->user_email;
				
				$subject = get_field('email_new_cleanup_object', 'option');
				$body = sprintf(
					nl2br(get_field('email_new_cleanup_content', 'option')), 
					$_POST['cleanup_name'],
					get_permalink($post_id),
					get_permalink(get_field('page_list_cleanup','option'))
				);
				wp_mail( $to, $subject, $body, $headers);

				if ( !is_wp_error( $post_id ) ) { 
					return new WP_REST_Response( '', 200 );
				}
			}
		} 
		
	}
    return new WP_REST_Response( '', 403 );
}
/*
function wcud_update_cleanup(WP_REST_Request $request){	
	if (empty($_POST['honeyPot'])) {
		$post_id = $_POST['id'];
		$post = array (
			'ID' 		 => $post_id,
			'post_title' => $_POST['cleanup_name'],
		);
		if(isset($_POST['onMap']) && $_POST['onMap'] == "yes_on_map"){
			$post['post_status'] = 'publish';
		}
		else{
			$post['post_status'] = 'private';
		}
		$post['post_content'] = sanitize_textarea_field( $_POST['cleanup_description']);
		wp_update_post($post);
		ihag_update_post_meta($post_id);	
		if ( !is_wp_error( $post_id ) ) { 
			return new WP_REST_Response( '', 200 );
		}
	}
}
*/
function ihag_update_post_meta($post_id){


	update_post_meta($post_id, 'type_structure', sanitize_text_field( $_POST['type_structure'] ));

	update_post_meta($post_id, 'structure_name', sanitize_text_field( $_POST['structure_name'] ));
	update_post_meta($post_id, 'user_name', sanitize_text_field( $_POST['user_name'] ));
	update_post_meta($post_id, 'localisation', sanitize_text_field( $_POST['localisation'] ));
	
	update_post_meta($post_id, 'user_phone', sanitize_text_field( $_POST['user_phone'] ));
	update_post_meta($post_id, 'display_phone', sanitize_text_field( $_POST['display_phone'] ));
	
	update_post_meta($post_id, 'user_email', sanitize_text_field( $_POST['user_email'] ));

	/*update_post_meta($post_id, 'user_twitter', sanitize_text_field( $_POST['user_twitter'] ));
	update_post_meta($post_id, 'user_facebook', sanitize_text_field( $_POST['user_facebook'] ));
	update_post_meta($post_id, 'user_linkedin', sanitize_text_field( $_POST['user_linkedin'] ));
	update_post_meta($post_id, 'user_instagram', sanitize_text_field( $_POST['user_instagram'] ));*/

	update_post_meta($post_id, 'cat_cybercleanup', sanitize_text_field( $_POST['cat_cybercleanup'] ));


	//var_dump($_POST['data_cat']);
	update_post_meta($post_id, 'data_cat', $_POST['data_cat']);
	update_post_meta($post_id, 'data_cat_other', $_POST['data_cat_other']);

	update_post_meta($post_id, 'reuse_cat', $_POST['reuse_cat']);
	update_post_meta($post_id, 'nbr_place_deee', $_POST['nbr_place_deee']);
	
	//update_post_meta($post_id, 'hard_cat', $_POST['hard_cat']);
	//update_post_meta($post_id, 'hard_cat_other', $_POST['hard_cat_other']);

	//update_post_meta($post_id, 'hard_volume', $_POST['hard_volume']);
	
	update_post_meta($post_id, 'hard_collect', $_POST['hard_collect']);
	
	update_post_meta($post_id, 'hard_collect_other', $_POST['hard_collect_other']);
	
	update_post_meta($post_id, 'cleanup_name', sanitize_text_field( $_POST['cleanup_name'] ));
	update_post_meta($post_id, 'cleanup_description', sanitize_textarea_field( $_POST['cleanup_description']));
	
	update_post_meta($post_id, 'visibility', sanitize_text_field( $_POST['visibility'] ));
	update_post_meta($post_id, 'cleanup_tag', sanitize_text_field( $_POST['cleanup_tag'] ));

	update_post_meta($post_id, 'date_start', sanitize_text_field( $_POST['date_start'] ));
	update_post_meta($post_id, 'time_start', sanitize_text_field( $_POST['time_start'] ));
	update_post_meta($post_id, 'during_day', sanitize_text_field( $_POST['during_day'] ));
	update_post_meta($post_id, 'time_end', sanitize_text_field( $_POST['time_end'] ));


	/*update_post_meta($post_id, 'animate_cybercleanup', sanitize_text_field( $_POST['animate_cybercleanup'] ));
	update_post_meta($post_id, 'animator_cwcud', sanitize_text_field( $_POST['animator_cwcud'] ));
	update_post_meta($post_id, 'animator_not_cwcud', sanitize_text_field( $_POST['animator_not_cwcud'] ));*/
	
	//update_post_meta($post_id, 'onMap', sanitize_text_field( $_POST['onMap'] ));
	//update_post_meta($post_id, 'private', sanitize_text_field( $_POST['private'] ));
	//update_post_meta($post_id, 'child', sanitize_text_field( $_POST['child'] ));

	

	/*update_post_meta($post_id, 'nb_participant_max', sanitize_text_field( $_POST['nb_participant_max'] ));
	update_post_meta($post_id, 'nb_participant_target', sanitize_text_field( $_POST['nb_participant_target'] ));
	*/
	
	/*if (isset($_POST['coordonate']) && !empty($_POST['coordonate'])){
		update_post_meta($post_id, 'coordonate', sanitize_text_field( $_POST['coordonate'] ));
	}else{//empty
		update_post_meta($post_id, 'coordonate', "48.85302176261338,2.349699251353741" );
	}*/
	
	update_post_meta($post_id, 'location', sanitize_text_field( $_POST['location'] ));
	update_post_meta($post_id, 'coordonate', sanitize_text_field( $_POST['coordonate'] ) );
	update_post_meta($post_id, 'cleanup_adresse', sanitize_text_field( $_POST['cleanup_adresse'] ));
	
	update_post_meta($post_id, 'link_connection', sanitize_text_field( $_POST['link_connection'] ));
	update_post_meta($post_id, 'cleanup_area', sanitize_text_field( $_POST['cleanup_area'] ));
	update_post_meta($post_id, 'more_information', sanitize_text_field( $_POST['more_information'] ));
	update_post_meta($post_id, 'link_connection', sanitize_text_field( $_POST['link_connection'] ));

	update_post_meta($post_id, 'link_sign_in', sanitize_text_field( $_POST['link_sign_in'] ));
	update_post_meta($post_id, 'link_channel', sanitize_text_field( $_POST['link_channel'] ));
	
	
	
}

function wcud_search_address(WP_REST_Request $request){
	if ( check_nonce() ) {

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api-adresse.data.gouv.fr/search/?q=".str_replace(" ", "+", $_POST["address"]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);	

		$r = json_decode( curl_exec($ch) );
		$r = (empty($r->features[0]->geometry->coordinates)) ? '' : $r->features[0]->geometry->coordinates ;
		curl_close($ch);
		return new WP_REST_Response( $r, 200 );
	}
	return new WP_REST_Response( '', 403 );
}


function wcud_registerUserCleanUp(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {
			global $wpdb;

			$db_item =  $wpdb->get_results($wpdb->prepare(
				"SELECT id, name, phone FROM {$wpdb->prefix}participant WHERE email LIKE %s", sanitize_email($_POST['email_registerUserCleanUp'])
			));
			if($wpdb->num_rows == 0){
				$wpdb->insert(
					$wpdb->prefix.'participant',
					array(
						'email' => sanitize_email($_POST['email_registerUserCleanUp']),
						'name'	=> sanitize_text_field( $_POST['name_registerUserCleanUp'] ),
						'phone'	=> sanitize_text_field( $_POST['phone_registerUserCleanUp'] ),
					),
					array(
						'%s',
						'%s',
						'%s'
					)
				);
				$id_participant = $wpdb->insert_id;
			}elseif($wpdb->num_rows == 1){
				$wpdb->update(
					$wpdb->prefix.'participant',
					array(
						'name'	=> ( (!empty(sanitize_text_field( $_POST['name_registerUserCleanUp']) ) ) ? sanitize_text_field( $_POST['name_registerUserCleanUp']) : $db_item[0]->name),
						'phone'	=> ( (!empty(sanitize_text_field( $_POST['phone_registerUserCleanUp'])) ) ? sanitize_text_field( $_POST['phone_registerUserCleanUp']) : $db_item[0]->phone),
					),
					array('id' => $db_item[0]->id)
				);
				$id_participant = $db_item[0]->id;
			}

			$db_item =  $wpdb->get_results($wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}participant_cleanup WHERE id_participant = %d AND id_cleanup = %d" , array( $id_participant, sanitize_text_field($_POST['id_cleanup']))
			));
			if($wpdb->num_rows == 0){
				$wpdb->insert(
					$wpdb->prefix.'participant_cleanup',
					array(
						'id_participant' => $id_participant,
						'id_cleanup'	=> $_POST['id_cleanup'],
						'guest'	=> ( (int)sanitize_text_field ( $_POST['invite_registerUserCleanUp'] ) + 1),
						'child'	=> (int)sanitize_text_field( $_POST['invite_child_registerUserCleanUp'] ),
					),
					array(
						'%d',
						'%d',
						'%d',
						'%d'
					)
				);
			}
			elseif($wpdb->num_rows == 1){
				$wpdb->update(
					$wpdb->prefix.'participant_cleanup',
					array(
						'guest'	=> ( (int)sanitize_text_field ( $_POST['invite_registerUserCleanUp'] ) + 1),
						'child'	=> (int)sanitize_text_field( $_POST['invite_child_registerUserCleanUp'] ),
					),
					array(
						'id_participant' => $id_participant,
						'id_cleanup' => sanitize_text_field($_POST['id_cleanup']),
					)
				);
			}

			$db_item =  $wpdb->get_results($wpdb->prepare(
				"SELECT sum(guest) as guest, sum(child) as child FROM {$wpdb->prefix}participant_cleanup WHERE id_cleanup = %d" , array( sanitize_text_field($_POST['id_cleanup']))
			));
			update_post_meta($_POST['id_cleanup'], 'participants', ($db_item[0]->guest + $db_item[0]->child));
			
			

			if(isset($_POST['checkbox_newsletter']) && $_POST['checkbox_newsletter'] == "true"){
				require dirname(__DIR__, 1).'/vendor/autoload.php';

				$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
				$body = [
				'Action' => "addnoforce",
				'Contacts' => [
						[
						'Email' => sanitize_email($_POST['email_registerUserCleanUp']),
						'IsExcludedFromCampaigns' => "false",
						'Name' => sanitize_email($_POST['email_registerUserCleanUp']),
						'Properties' => "object"
						]
					]
				];
				$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_liste', 'option'), 'body' => $body]);
				$response->success() && var_dump($response->getData());
			}

			$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
			$to = sanitize_email($_POST['email_registerUserCleanUp']);
			$subject = __("Votre participation au DigitalCleanupDay a été enregistrée",'cwcud');
			$body = "Bonjour ".sanitize_text_field( $_POST['name_registerUserCleanUp'] ) ."<br><br>
Votre participation au DigitalCleanup ".get_the_title($_POST['id_cleanup'])." a été enregistrée.<br><br>
Rappel du DigitalCleanup :<br>
● ".get_the_title($_POST['id_cleanup'])."<br>
● Le ".date_i18n('j F Y', strtotime(get_post_meta( $_POST['id_cleanup'], "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($_POST['id_cleanup'], "time_start", true )))." (".get_post_meta($_POST['id_cleanup'], "time_end", true ).")<br>
● ".get_post_meta($_POST['id_cleanup'], "cleanup_adresse", true)."<br>";
			$whichLocation = get_post_meta( $_POST['id_cleanup'], "location", true );
			if ($whichLocation === "location_facetoface"):
				$body .= '● Évènement présentiel : '.get_post_meta( $_POST['id_cleanup'], "cleanup_adresse", true )."<br>";
			elseif($whichLocation === "location_distancing"):
				if(empty(get_post_meta( $_POST['id_cleanup'], "link_connection", true ))):
					$body .= '● Évènement distanciel : le lien vous sera envoyé ultérieurement par email.'."<br>";
				else:
					$body .= '● Évènement distanciel : '.get_post_meta( $_POST['id_cleanup'], "link_connection", true )."<br>";
				endif;

			elseif($whichLocation === "location_both"):
				$body .= '● Évènement présentiel : '.get_post_meta( $_POST['id_cleanup'], "cleanup_adresse", true )."<br>";
				$link = (!empty(get_post_meta($_POST['id_cleanup'], "link_connection", true))) ? get_post_meta($_POST['id_cleanup'], "link_connection", true) : 'Le lien vous sera envoyé ultérieurement par email.';
				$body .= '● Évènement distanciel : '.$link."<br>";
			endif;

			$more_information = get_post_meta( $_POST['id_cleanup'], "more_information", true ) ;
			if( isset($more_information) && !empty($more_information)){
				$body .= get_post_meta( $_POST['id_cleanup'], "more_information", true )."<br><br>";
			}
			$body .= "<br>Vous êtes susceptible de recevoir des informations complémentaires de la part le l’organisateur.<br>Sachez que vous pouvez à tout moment supprimer vos données participant.<br> Lien de désinscription : "; 
			$tab_arg = array(
				'remove_email' => sanitize_email($_POST['email_registerUserCleanUp']),
				'key'	=> crypt(sanitize_email($_POST['email_registerUserCleanUp']), 'windows95'),
			);
			$body .= esc_url( add_query_arg( $tab_arg, get_the_permalink($_POST['id_cleanup'])));

$body .= "<br><br>Numériquement Vôtre,<br>
l'équipe du Digital Cleanup Day<br><br>
Vous recevez ce mail car vous prenez part au Digital Cleanup Day 2022 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@digital-cleanup-day.fr";
		
			wp_mail( $to, $subject, $body, $headers);
			
		}
    }    
    return new WP_REST_Response( '', 200 );
}

function wcud_nbRegisterUserCleanUp(){
	$tab_participant = get_post_meta( $_POST['id_cleanup'], 'participants', true );
	if ( empty( $tab_participant ) ) {
		return new WP_REST_Response( '0', 200 );
	}
	return new WP_REST_Response( sizeof($tab_participant), 200 );
}

function wcud_list_inscrits(){
	/*$tab_participant = get_post_meta( $_POST['id_cleanup'], 'participants', true );
	if ( empty( $tab_participant ) ) {
		return new WP_REST_Response( '', 200 );
	}*/
	global $wpdb;
	$tab_participant =  $wpdb->get_results($wpdb->prepare(
		"SELECT name, phone, email, guest, child FROM {$wpdb->prefix}participant INNER JOIN {$wpdb->prefix}participant_cleanup ON {$wpdb->prefix}participant.id = {$wpdb->prefix}participant_cleanup.id_participant WHERE id_cleanup = %d", sanitize_text_field($_POST['id_cleanup'])
	));
	echo '<h2>Liste des participants à '.get_the_title($_POST['id_cleanup']).'</h2>';
	foreach($tab_participant as $participant){
		$u = get_userdata($user_id);
		echo $participant->name.' - ';
		echo $participant->email;
		if(!empty($participant->phone)){
			echo ' - tel: '.$participant->phone;
		}
		if($participant->guest > 0){
			echo ' - participants : '.$participant->guest;
		}
		if($participant->child > 0){
			echo ' - participants enfants: '.$participant->child;
		}
		echo '<br>';

	}
	return new WP_REST_Response( NULL, 200 );
}

function wcud_info_orga(){
	$post =  get_post($_POST["id_cleanup"]);
	$author_id = $post->post_author;
	$user = get_userdata( $author_id );
	echo '<h2>'.get_the_title($post->ID).'</h2>';

	echo '<p><b>Organisé par</b> : ';

	echo get_post_meta($post->ID,'user_name', true);

	$structure_name = get_post_meta($post->ID,'structure_name', true);
	if(!empty($structure_name)){
		echo ' - '.$structure_name;
	}


	echo "<br>Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")</p>";

	echo '<p><b>Adresse email</b> : '.get_post_meta($post->ID,'user_email', true).'</p>';
	if(get_post_meta($post->ID,'display_phone', true) == 'display_phone_ok'){
		echo '<p><b>Téléphone</b> : '.get_post_meta($post->ID,'user_phone', true).'</p>';
	}

	//get_the_author_meta( string $field = '', int|false $user_id = false )
	return new WP_REST_Response( NULL, 200 );
}

/*function wcud_ask_ambassadeur(){
	if (empty($_POST['honeyPot'])) {

		if ( check_nonce() ) {

		}
	}
}*/



function wcud_bilanForm(WP_REST_Request $request){
	if ( check_nonce() ) {
		if (empty($_POST['honeyPot'])) {
			$params = $request->get_params();
			foreach($params as $name => $value){
				update_post_meta($_POST['id_cleanup'], $name, sanitize_text_field( $value));
			}
			return new WP_REST_Response( '', 200 );	
		}		
	}
    return new WP_REST_Response( '', 304 );
}