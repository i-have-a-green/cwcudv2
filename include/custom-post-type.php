<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

add_action( 'init', 'ihag_custom_post_type');
function ihag_custom_post_type() {
	register_post_type( 'cleanup', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' 			=> array(
				'name' 				=> __('CleanUp', 'cwcud'), /* This is the Title of the Group */
				'singular_name' 	=> __('CleanUp', 'cwcud'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-flag', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'show_ui'            => true,
			'show_in_menu'       => true,
			'supports' 			=> array( 'title', 'editor',  'custom-fields', 'author'),
			'public' => true,
			'show_in_rest' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
	 	) /* end of options */
	);
	//flush_rewrite_rules();
}


function pluginize_local_cptui_data( $data = array() ) {
  $theme_dir = get_stylesheet_directory();
  // Create our directory if it doesn't exist.
  if ( ! is_dir( $theme_dir .= '/cptui_data' ) ) {
      mkdir( $theme_dir, 0755 );
  }

  if ( array_key_exists( 'cpt_custom_post_type', $data ) ) {
      // Fetch all of our post types and encode into JSON.
      $cptui_post_types = get_option( 'cptui_post_types', array() );
      $content = json_encode( $cptui_post_types );
      // Save the encoded JSON to a primary file holding all of them.
      file_put_contents( $theme_dir . '/cptui_post_type_data.json', $content );
  }

  if ( array_key_exists( 'cpt_custom_tax', $data ) ) {
      // Fetch all of our taxonomies and encode into JSON.
      $cptui_taxonomies = get_option( 'cptui_taxonomies', array() );
      $content = json_encode( $cptui_taxonomies );
      // Save the encoded JSON to a primary file holding all of them.
      file_put_contents( $theme_dir . '/cptui_taxonomy_data.json', $content );
  }
}
add_action( 'cptui_after_update_post_type', 'pluginize_local_cptui_data' );
add_action( 'cptui_after_update_taxonomy', 'pluginize_local_cptui_data' );