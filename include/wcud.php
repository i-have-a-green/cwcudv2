<?php

//include_once("include/template.php");
include_once("wcud/rest.php");
include_once("wcud/back-office.php");



add_action('init', 'ihag_register_roleorganisateur' );
function ihag_register_roleorganisateur() {
	add_role( 'organisateur', __( 'Organisateur	', 'cwcud' ), array( 'read' => true, 'level_0' => true ) );
	//add_role( 'participant', __( 'Participant', 'cwcud' ), array( 'read' => true, 'level_0' => true ) );
}
add_filter( 'user_search_columns', function( $search_columns ) {
	$search_columns[] = 'display_name';
	/*$search_columns[] = 'first_name';
	$search_columns[] = 'last_name';*/
	
    return $search_columns;
} );

/*function add_table_participants()
{   
	
  	global $wpdb; 
	$db_table_name = $wpdb->prefix . 'participant';  // table name
	$charset_collate = $wpdb->get_charset_collate();
	
	if($wpdb->get_var( "show tables like '$db_table_name'" ) != $db_table_name ) 
	{
		$sql = "CREATE TABLE $db_table_name (
			id int(11) NOT NULL auto_increment,
			email varchar(200) NOT NULL,
			name varchar(200),
			phone varchar(20),
			PRIMARY KEY (id)
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
		error_log($sql);
	}

	$db_table_name = $wpdb->prefix . 'participant_cleanup';  // table name
	$charset_collate = $wpdb->get_charset_collate();
	if($wpdb->get_var( "show tables like '$db_table_name'" ) != $db_table_name ) 
	{
		$sql = "CREATE TABLE $db_table_name (
			id_participant int(11) NOT NULL,
			id_cleanup int(11) NOT NULL,
			guest int(11) NOT NULL,
			child int(11) NOT NULL
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
} 
register_activation_hook( __FILE__, 'add_table_participants' );*/

/*add_action('wp_enqueue_scripts', 'ihag_add_asset', 999);
function ihag_add_asset()
{
	$plugin_data = get_plugin_data( __FILE__ );
	$plugin_name = $plugin_data['Name'];
    global $wp_styles;
    wp_enqueue_style('wcud', plugin_dir_url( __FILE__ ) . 'assets/style.css', array('styles'));

    wp_register_script('wcud', plugin_dir_url( __FILE__ ) . 'assets/script.js', array('script'), false, 'all');
    wp_enqueue_script('wcud');
	wp_localize_script('wcud', 'page_add_cleanup', get_permalink( get_field("page_add_cleanup", "option") ));
	wp_localize_script('wcud', 'page_update_cleanup', get_permalink( get_field("page_update_cleanup", "option") ));
	wp_localize_script('wcud', 'page_list_cleanup', get_permalink( get_field("page_list_cleanup", "option") ));
	wp_localize_script('wcud', 'home_url', home_url());
	wp_localize_script('wcud', 'iconBase', plugin_dir_url( __FILE__ ) . 'assets/', array('script'));
	
}*/

/*add_action( 'init', 'ihag_custom_post_type');
function ihag_custom_post_type() {
	register_post_type( 'cleanup', 
		array('labels' 			=> array(
				'name' 				=> __('CleanUp', 'cwcud'), 
				'singular_name' 	=> __('CleanUp', 'cwcud'), 
			), 
			'menu_position' 	=> 18, 
			'menu_icon' 		=> 'dashicons-flag', 
			'show_ui'            => true,
			'show_in_menu'       => true,
			'supports' 			=> array( 'title', 'editor',  'custom-fields'),
			'public' => true,
			'show_in_rest' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
	 	) 
	);
	//flush_rewrite_rules();
}
*/
/*
add_filter('single_template', 'ihag_use_single_cleanup');
function ihag_use_single_cleanup($single) {

    global $post;

    
    if ( $post->post_type == 'cleanup' ) {
        if ( file_exists( plugin_dir_path( __FILE__ ) . '/templates/single-cleanup.php' ) ) {
            return plugin_dir_path( __FILE__ ) . '/templates/single-cleanup.php' ;
        }
    }
    return $single;
}
*/

function ihag_update_cleanup( $post_id, $post, $update )  {

	if( ! $update ) { return; }
	if( wp_is_post_revision( $post_id ) ) { return; }
	if( defined( 'DOING_AUTOSAVE' ) and DOING_AUTOSAVE ) { return; }
	if( $post->post_type != 'cleanup' ) { return; }
	if (wp_is_post_autosave($post_id)) {return; }
	$tab_participant = get_post_meta( $post_id, 'participants', true );
	if ( empty( $tab_participant ) ) {return;}

	foreach ($tab_participant as $participant) {
		$user = get_userdata($participant);
		//error_log($user->user_email);
		$headers[] = 'From: '.get_bloginfo('name').' <'. __('no-reply@', 'cwcud') . str_replace('www.', '', $_SERVER['SERVER_NAME'] ) .'>';
		$to = $user->user_email;
		$subject = __("Le DigitalCleanup auquel vous êtes inscrit a été mis à jour",'cwcud');
		$body = "Bonjour ".sanitize_text_field( $user->display_name ) ."<br>
Le DigitalCleanup ".get_the_title($post_id)." auquel vous êtes inscrit a été mis à jour.<br><br>
Rappel du DigitalCleanup :<br>
● ".get_the_title($post_id)."<br>
● Le ".date_i18n('j F Y', strtotime(get_post_meta( $post_id, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post_id, "time_start", true )))." (".get_post_meta($post_id, "time_end", true ).")<br>
● ".get_post_meta($post_id, "cleanup_adresse", true)."<br>";
		$whichLocation = get_post_meta( $post_id, "location", true );
		if ($whichLocation === "location_facetoface"):
			$body .= '● Évènement présentiel : '.get_post_meta( $post_id, "cleanup_adresse", true )."<br>";
		elseif($whichLocation === "location_distancing"):
			$body .= '● Évènement distanciel : '.get_post_meta( $post_id, "link_connection", true )."<br>";
		elseif($whichLocation === "location_both"):
			$body .= '● Évènement présentiel : '.get_post_meta( $post_id, "cleanup_adresse", true )."<br>";
			$link = (!empty(get_post_meta($post_id, "link_connection", true))) ? get_post_meta($post_id, "link_connection", true) : 'Le lien vous sera envoyé ultérieurement par email.';
			$body .= '● Évènement distanciel : '.$link."<br>";
		endif;

		$more_information = get_post_meta( $post_id, "more_information", true ) ;
		if( isset($more_information) && !empty($more_information)){
			$body .= get_post_meta($post_id, "more_information", true )."<br><br>";
		}
		$body .= "Vous êtes susceptible de recevoir des informations complémentaires de la part le
l’organisateur. Sachez que vous pouvez à tout moment modifier et/ou supprimer vos
données participant.
Numériquement Vôtre,<br>
l'équipe du Digital Cleanup Day<br><br>
Vous recevez ce mail car vous prenez part au Digital Cleanup Day 2021 et en avez accepté les conditions. Vous pouvez à tout moment exercer votre droit d'accès et/ou de suppression des données vous concernant en nous contactant par email : contact@digital-cleanup-day.fr";
	
		wp_mail( $to, $subject, $body, $headers);
	}
}
add_action( 'save_post_cleanup', 'ihag_update_cleanup', 10, 3 );

add_filter('private_title_format', 'removePrivatePrefix'); 
add_filter('protected_title_format', 'removePrivatePrefix');
function removePrivatePrefix($format) {
return '%s';
}

function get_during(){
	return array(
		"",
		"0h30",
		"1h00",
		"1h30",
		"2h00",
		"2h30",
		"3h00",
		"3h30",
		"4h00",
		"4h30",
		"5h00",
		"5h30",
		"6h00",
		"6h30",
		"7h00",
		"7h30",
		"8h00",
		"8h30",
		"9h00",
		"9h30",
		"10h00",
		"10h30",
		"11h00",
		"11h30",
		"12h00",
	);
}


function ihag_no_admin_access()
{
    if ( !is_admin() || (is_user_logged_in() && isset( $GLOBALS['pagenow'] ) AND 'wp-login.php' === $GLOBALS['pagenow'] ) ) {
        return;
    }

    $redirect = esc_url(get_permalink(get_field("page_list_cleanup", "options")));
    if(!current_user_can('administrator')){
        exit( wp_redirect( $redirect ) );
	}
}
add_action( 'admin_init', 'ihag_no_admin_access', 100 );