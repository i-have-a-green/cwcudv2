<?php
// Ajoute une feuille de styles  dans l'admin
add_action( 'after_setup_theme', 'ihag_style_editor' );
 
function ihag_style_editor(){
 
	add_theme_support( 'editor-styles' );   // if you don't add this line, your stylesheet won't be added
	add_editor_style( 'style-editor.css' ); // tries to include style-editor.css directly from your theme folder
 
}

remove_theme_support( 'core-block-patterns' );

// FILTRES LES BLOCS AUTORISES SUR LE SITE
// cf. https://rudrastyh.com/gutenberg/remove-default-blocks.html
// -----------------------------------------------------------------------------
add_filter( 'allowed_block_types', 'nb_allowed_block_types', 10, 2 );
function nb_allowed_block_types($allowed_blocks, $post) {
	$allowed_blocks = array(

	// Blocs spécifiques du theme 
    /*'acf/slide-video',
    'acf/title',*/
    
    'acf/titre-texte-image',
    'acf/image',
    'acf/video',
    'acf/paragraphe',
    'acf/titre',
    'acf/texte-2-paragraphes',
    'acf/texte-3-paragraphes',
    'acf/evenement-accueil',
    'acf/titre-de-page',
    'acf/texte',
    'acf/texte-image',
    'acf/mission',
    'acf/bouton-lien',
    'acf/document-telechargeable',
    'acf/faq',
    'acf/mailjet',
    'acf/map',
    'acf/counter',
    // Blocs communs
     'core/heading',
		 'core/paragraph',
  	'core/image',
  	//'core/gallery',
		'core/list',
		//'core/quote',
		// 'core/audio',
		//'core/cover-image',
		//'core/file',
		//'core/video',
		//'core/media',

    // Mise en forme
    //'core/table',
    // 'core/verse',
    // 'core/code',
    // 'core/freeform',
    // 'core/html',
    // 'core/preformatted',
    // 'core/pullquote',

    // Mise en page
    'core/button',
    //'core/columns',
    //'core/media-text',
    // 'core/more',
    // 'core/nextpage',
    // 'core/separator',
     'core/spacer',

    // Widgets
     'core/shortcode',
    // 'core/archives',
    // 'core/categories',
    // 'core/latest-comments',
    // 'core/latest-posts',

    // Contenus embarqués
    // 'core/embed',
    // 'core-embed/youtube',
    // 'core-embed/facebook',
    // 'core-embed/twitter',
    // 'core-embed/instagram',
    // core-embed/wordpress
    // core-embed/soundcloud
    // core-embed/spotify
    // core-embed/flickr
    // core-embed/vimeo
    // core-embed/animoto
    // core-embed/cloudup
    // core-embed/collegehumor
    // core-embed/dailymotion
    // core-embed/funnyordie
    // core-embed/hulu
    // core-embed/imgur
    // core-embed/issuu
    // core-embed/kickstarter
    // core-embed/meetup-com
    // core-embed/mixcloud
    // core-embed/photobucket
    // core-embed/polldaddy
    // core-embed/reddit
    // core-embed/reverbnation
    // core-embed/screencast
    // core-embed/scribd
    // core-embed/slideshare
    // core-embed/smugmug
    // core-embed/speaker
    // core-embed/ted
    // core-embed/tumblr
    // core-embed/videopress
    // core-embed/wordpress-tv
	);

	return $allowed_blocks;
}




// DESACTIVE GUTENBERG DE CERTAINS TEMPLATES DE PAGE
// -----------------------------------------------------------------------------
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// Désactive Gutenberg par template
function ea_disable_gutenberg( $can_edit, $post_type ) {

  if(is_page_template( 'templates/tpl-range-recipe.php' )){
    return true;
  }

  if($post_type == "cleanup"){
    return false;
  }
  if($post_type == "members"){
    return false;
  }

	if( ! ( is_admin() && !empty( $_GET['post'] ) ) ){
    return $can_edit;
  }

	return $can_edit;
}



// NETTOYAGE DES OPTIONS DE LA SIDEBAR
// cf. https://joseph-dickson.com/removing-specific-gutenberg-core-blocks-and-options/
// -----------------------------------------------------------------------------


// PALETTE DE COULEURS
// -----------------------------------------------------------------------------
// Désactive la palette de couleurs
add_theme_support( 'disable-custom-colors' );

// Supprime la palette de couleur
add_theme_support( 'editor-color-palette' );


// FONTS
// -----------------------------------------------------------------------------
// Désactive les tailles de typos
add_theme_support( 'disable-custom-font-sizes' );

// Crée une liste de choix de typos adaptée à la charte du site
/*add_theme_support( 'editor-font-sizes',
  array(
  	array(
  		'name' => "Petit",
  		'shortName' => 'S',
  		'size' => 13,
  		'slug' => 'small'
  	),
  	array(
    	'name' => 'Standard',
    	'shortName' => 'D',
    	'size' => 16,
    	'slug' => 'standard'
  	),
  	array(
    	'name' => 'Moyen',
    	'shortName' => 'M',
    	'size' => 20,
    	'slug' => 'medium'
  	),
  	array(
  		'name' => 'Grand',
  		'shortName' => 'L',
  		'size' => 24,
  		'slug' => 'large'
  	),
  	array(
  		'name' => 'Très grand',
    	'shortName' => 'XL',
  		'size' => 28,
  		'slug' => 'extra-large'
  	)
  )
);*/
