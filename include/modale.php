<?php

add_action('rest_api_init', function() {
	register_rest_route( 'cwcud', 'ihag_modale',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihag_modale',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});

function ihag_modale(WP_REST_Request $request){
    echo wp_oembed_get(sanitize_url($_POST['field']));
    return new WP_REST_Response( NULL, 200 );
}

