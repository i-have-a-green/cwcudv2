<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'cwcud',
                'title' => __( 'Custom', 'cwcud' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(
            array(
                'name'				    => 'evenement-accueil',
                'title'				    => __('Évènement - Accueil'),
                'description'		    => __('Bloc Gutenberg pour mettre en place sur la page d\'accueil: un titre, une accroche, une date, un bouton lien et une image de fond'),
                'placeholder'		    => __('Évènement - Accueil'),
                'render_template'	    => 'template-parts/block/evenement-accueil.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'accroche', 'image', 'date', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'titre-de-page',
                'title'				    => __('Titre de page'),
                'description'		    => __('Bloc Gutenberg pour mettre en place le titre de la page et une image en arrière plan'),
                'placeholder'		    => __('Évènement - Accueil'),
                'render_template'	    => 'template-parts/block/titre-de-page.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'texte',
                'title'				    => __('Texte - 1 paragraphe'),
                'description'		    => __('Bloc Gutenberg pour mettre en place un titre de niveau administrable et un texte tout les deux centrables.'),
                'placeholder'		    => __('Texte'),
                'render_template'	    => 'template-parts/block/texte.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'texte', 'paragraphe'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'texte-2-paragraphes',
                'title'				    => __('Texte - 2 paragraphes'),
                'description'		    => __('Bloc Gutenberg pour mettre en place un titre de niveau administrable et un texte à deux paragraphes, tout les deux centrables.'),
                'placeholder'		    => __('Texte_2'),
                'render_template'	    => 'template-parts/block/texte2.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'texte', 'paragraphe', '2'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'texte-3-paragraphes',
                'title'				    => __('Texte - 3 paragraphes'),
                'description'		    => __('Bloc Gutenberg pour mettre en place un titre de niveau administrable et un texte à trois paragraphes, tout les deux centrables.'),
                'placeholder'		    => __('Texte_3'),
                'render_template'	    => 'template-parts/block/texte3.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'texte', 'paragraphe', '3'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'texte-image',
                'title'				    => __('Texte & image'),
                'description'		    => __('Bloc Gutenberg pour mettre en place un titre de niveau administrable,un texte et une image. L’utilisateur a la possibilité de choisir la disposition qu\'il souhaite pour l\'image (à droite ou à gauche)'),
                'placeholder'		    => __('Texte & image'),
                'render_template'	    => 'template-parts/block/texte-image.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'texte', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'Mission',
                'title'				    => __('Mission'),
                'description'		    => __('Bloc Gutenberg pour mettre en place une image en arrière plan, un titre et un texte administrable.'),
                'placeholder'		    => __('Mission'),
                'render_template'	    => 'template-parts/block/mission.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('texte', 'image', 'mission', 'titre'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'bouton-lien',
                'title'				    => __('Bouton - Lien'),
                'description'		    => __('Bloc Gutenberg pour mettre en place 1 ou 2 bouton lien qui redirigent vers d\autres pages'),
                'placeholder'		    => __('Bouton - Lien'),
                'render_template'	    => 'template-parts/block/bouton-lien.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('bouton', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
   
        acf_register_block_type(
            array(
                'name'				    => 'document-telechargeable',
                'title'				    => __('Document téléchargeable'),
                'description'		    => __('Bloc Gutenberg avec un titre pour le document, une image d\'illustration, son poid, une description et un bouton pour le télécharger'),
                'placeholder'		    => __('Document téléchargeable'),
                'render_template'	    => 'template-parts/block/document-telechargeable.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('document', 'titre', 'image', 'télécharger',),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        
        acf_register_block_type(
            array(
                'name'				    => 'image',
                'title'				    => __('Image'),
                'description'		    => __('Affiche une image avec deux formats possibles'),
                'placeholder'		    => __('image'),
                'render_template'	    => 'template-parts/block/image.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'format-image',
                'keywords'			    => array('agence', 'bloc', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'faq',
                'title'				    => __('FAQ'),
                'description'		    => __('Permet d’afficher les onglets déroulants questions/réponses'),
                'placeholder'		    => __('FAQ'),
                'render_template'	    => 'template-parts/block/faq.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'flag',
                'keywords'			    => array('Titre', 'faq', 'question', 'reponse'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );  

        acf_register_block_type(
            array(
                'name'				    => 'mailjet',
                'title'				    => __('Form mailJet'),
                'description'		    => __('Affiche form inscription organisateur : ambassadeur (provisoir)'),
                'placeholder'		    => __('mailjet'),
                'render_template'	    => 'template-parts/block/mailjet.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'flag',
                'keywords'			    => array('mailjet', 'form', 'inscription'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );  

        acf_register_block_type(
            array(
                'name'				    => 'map',
                'title'				    => __('Carte cleanups'),
                'description'		    => __('Affiche le carte des Cleanups'),
                'render_template'	    => 'template-parts/block/map.php',
                'category'			    => 'cwcud',
                'mode'                  => 'edit',
                'icon'				    => 'admin-site-alt',
                'keywords'			    => array('map', 'carte'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );  

        acf_register_block_type(
            array(
                'name'				    => 'counter',
                'title'				    => __('counter cleanups'),
                'description'		    => __('Affiche le compteur des Cleanups'),
                'render_template'	    => 'template-parts/block/counter.php',
                'category'			    => 'cwcud',
                'mode'                  => 'view',
                'icon'				    => 'admin-site-alt',
                'keywords'			    => array('compteur', 'counter'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );  

    }
}