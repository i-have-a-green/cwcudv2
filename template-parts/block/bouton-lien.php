<?php
/**
 * Block Name: bouton lien 
 */
?>

<div class="blk-button blk-wp wrapper ctr block-pad">

<?php

$link_one = get_field('link_one');

if ( empty($link_one) ):?>

	<em>Renseigner le paragraphe</em>

<?php else :?>

	<?php $link_number = get_field('link_number');
	if ($link_number == '1'):
		$link_one = get_field('link_one');
		if( $link_one ): 
			$link_one_url = $link_one['url'];
			$link_one_title = $link_one['title'];
			$link_one_target = $link_one['target'] ? $link_one['target'] : '_self';
			?>
			<a class="button" href="<?php echo esc_url( $link_one_url ); ?>" target="<?php echo esc_attr( $link_one_target ); ?>"><?php echo esc_html( $link_one_title ); ?></a>
		<?php endif; ?>

	<?php else: 

		$link_one = get_field('link_one');
		if( $link_one ): 
			$link_one_url = $link_one['url'];
			$link_one_title = $link_one['title'];
			$link_one_target = $link_one['target'] ? $link_one['target'] : '_self';
			?>
			<a class="button" href="<?php echo esc_url( $link_one_url ); ?>" target="<?php echo esc_attr( $link_one_target ); ?>"><?php echo esc_html( $link_one_title ); ?></a>
		<?php endif; 

		$link_two = get_field('link_two');
		if( $link_two ): 
			$link_two_url = $link_two['url'];
			$link_two_title = $link_two['title'];
			$link_two_target = $link_two['target'] ? $link_two['target'] : '_self';
			?>
			<a class="button-red" href="<?php echo esc_url( $link_two_url ); ?>" target="<?php echo esc_attr( $link_two_target ); ?>"><?php echo esc_html( $link_two_title ); ?></a>
		<?php endif; ?>
	
	<?php endif; ?>

<?php endif; ?>

</div>