<?php
/**
 * Block Name: Titre de page
 */
 ?>

<?php

$title = get_field('title');
$image = get_field('background_image');

if($image) {
	$class = "has-image";
} else {
	$class = "no-image";
}
?>

<section class="blk-page-title blk-wp wrapper block-pad-top <?php echo $class;?>">

<?php
if ( empty($title) ):
	echo '<em>Renseigner le bloc</em>';
else :
?>


	<?php if(!empty(get_field('title'))):?>
		<h1><?php the_field('title');?></h1>
	<?php endif; ?>

	<?php if ( !empty($image) ):?>
		<div class="img-content">
			<?php echo wp_get_attachment_image($image, 'cover'); ?>
		</div>
	<?php endif; ?>

<?php endif; ?>

</section>

