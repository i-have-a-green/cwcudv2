<?php
/**
 * Block Name: Bloc Video
 */
 ?>

<?php $block_uniq_id = "id_".uniqid(); ?>


<section id="block_<?php echo $block_uniq_id; ?>_section" class="blk-video">

    <div class="wrapper v-padding-small center">

        <?php
        $video = get_field('link_video');
        if ( !$video ) :?>
            <em>Renseigner le bloc</em>
            
        <?php else :?>

            <div class="video-container">

                <?php 
                // 01 -Test if thumbnail_video doesn't exists
                if (!get_field('thumbnail_video')) {    
                ?>
                    <a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
                        <script> 
                            if (typeof iframe === 'undefined') {
                            var iframe = new Object();
                            }
                            iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
                        </script>
    
                        <?php
                            $video = get_field( 'link_video' );
                            $id = retrieve_id_video($video);
                        ?>
    
                        <img src="http://img.youtube.com/vi/<?php echo $id; ?>/mqdefault.jpg">
    
                    </a> 
                 
                <?php 
                } else { 
                // 02 - If we have a thumbnail
                ?>
 
                    <a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
                        <script> 
                            if (typeof iframe === 'undefined') {
                                var iframe = new Object();
                            }
                            iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
                        </script>

                        <?php
                        $videoThumbnail = get_field('thumbnail_video');
                        $size = 'medium';
                        echo wp_get_attachment_image($videoThumbnail, $size);?>
                    </a>

                <?php 
                } ?>
 
            </div><!-- /embed-container -->

        <?php endif; ?>

    </div><!-- /wrapper-->

</section>