<?php
/**
* Block Name: Counter
*/
$args = array(
	'posts_per_page'	=> -1,
	'post_status'   	=> array("private", "publish"),
	'post_type'			=> 'cleanup',
);

$query = new WP_Query( $args );
$nb_cleanup = $query->found_posts;
$inscrits = 0;
if ( $query->have_posts() ) : 
	while ($query->have_posts()) : $query->the_post();
		$inscrits += ((int) get_post_meta( get_the_id(), "participants", true )) + ((int) get_post_meta( get_the_id(), "nb_participant_target", true ));
	endwhile; 
endif;
if($nb_cleanup > 0):
	echo '<div class="blk-counter blk-button blk-wp wrapper lft block-pad-top" style="text-align:center">';
	echo '<a href="'.get_the_permalink(get_field('page_map_cleanup', 'option')).'" class="button-yellow">'.sprintf( __( '%s Digital Cleanups', 'cwcud' ), number_format($nb_cleanup, 0, ',', ' ') ).'</a>';
	//echo '<span class="button-yellow">'.sprintf( __( '%s participants', 'cwcud' ), number_format($inscrits, 0, ',', ' ') ).'</span>';
	echo '</div>';
endif;