<?php
/**
 * Block Name: Bloc FAQ
 */
 ?>

<div class="blk-faq blk-wp wrapper lft block-pad-top">

	<?php
	$question = get_field('question');
	$answer = get_field('answer');

	if ( empty($question && $answer) ):?>

		<em>Renseigner le contenu</em>;

	<?php else :
		
		echo '<details>';
			echo '<summary>'. $question .'</summary>';
			echo $answer;
		echo '</details>';

	endif; 
	?>

</div>
