<?php
/**
 * Block Name: Map
 */

if(is_admin()):
	echo 'Carte des cleanups';
else:
	?>
	<section class="">
	<?php
    $args = array(
        'post_type'		=> 'cleanup',
        'posts_per_page'	=> -1,
        'post_status'   => array("publish"),
    );

    $meta_query = array();

    if(isset($_GET['cat_cybercleanup']) && !empty($_GET['cat_cybercleanup'])){
        $meta_query[] = array(
            'key' => 'cat_cybercleanup',
            'value' => sanitize_text_field($_GET['cat_cybercleanup']),
        );
    }

	if(isset($_GET['id_cleanup']) && !empty($_GET['id_cleanup'])){
		$args['post__in'] = array($_GET['id_cleanup']);
	}

    if(isset($_GET['cleanup_name']) && !empty($_GET['cleanup_name'])){
        $meta_query[] = array(
            'key' => 'cleanup_name',
            'value' => sanitize_text_field($_GET['cleanup_name']),
            'compare' => 'LIKE',
        );
    }

    if(isset($_GET['visibility']) && !empty($_GET['visibility'])){
        $meta_query[] = array(
            'key' => 'visibility',
            'value' => sanitize_text_field($_GET['visibility']),
        );
    }
    else {
        $meta_query[] = array(
            'key' => 'visibility',
            'compare' => '!=',
            'value' => 'secret',
        );
    }

    if(isset($_GET['location']) && !empty($_GET['location'])){
        $meta_query[] = array(
            'key' => 'location',
            'value' => sanitize_text_field($_GET['location']),
        );
    }

    if(isset($_GET['area']) && !empty($_GET['area'])){
        $meta_query[] = array(
            'key' => 'cleanup_area',
            'value' => sanitize_text_field($_GET['area']),
        );
    }
    
    if(count($meta_query) > 1) {
        $meta_query['relation'] = 'AND';
    }

    $args['meta_query'] = $meta_query;
	$args2 = $args;
	$args2['post_status'] = array("private", "publish");
	$query2 = new WP_Query( $args2 );
    $nb_cleanup = $query2->found_posts;
	?>
        <p><b>
        <?php
        if(isset($_GET['cat_cybercleanup'])):
            sprintf( __( 'Il y a %s Digital Cleanups correspondants à la recherche', 'cwcud' ), $nb_cleanup );
        else:
            sprintf( __( 'Il y a %s Digital Cleanups', 'cwcud' ), $nb_cleanup );
        endif;
        ?>
        </b></p>
        <form method="GET" class="filters-cleanup" action="<?php echo get_the_permalink();?>">
            <div class="filters">
                <div class="select">
                    <select name="cat_cybercleanup">
                        <option value="" <?php @ihag_selected("",$_GET['cat_cybercleanup']);?>><?php _e('Catégorie de Digital Cleanup', 'cwcud');?></option>
                        <option value="data" <?php @ihag_selected("data",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Données', 'cwcud');?></option>
                        <option value="reuse" <?php @ihag_selected("reuse",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Reemploi', 'cwcud');?></option>
                        <option value="hardware" <?php @ihag_selected("hardware",$_GET['cat_cybercleanup']);?>><?php _e('Digital Cleanup Recyclage', 'cwcud');?></option>
                    </select>
                </div>
                <input type="text" placeholder="<?php _e('N° d\'un Digital Cleanup','cwcud');?>" name="id_cleanup" value="<?php echo (isset($_GET['id_cleanup']))?$_GET['id_cleanup']:'';?>">
                <input type="text" name="address" id="address" value="<?php echo isset($_GET['address'])? $_GET['address']:'';?>">
                <input type="text" name="cleanup_name" id="cleanup_name" value="<?php echo isset($_GET['cleanup_name'])? $_GET['cleanup_name']:'';?>" placeholder="<?php _e('Nom du Digital Cleanup') ?>">
                <input type="hidden" name="coordonate" id="coordonate" value="<?php echo isset($_GET['coordonate'])? $_GET['coordonate']:'';?>">
                <div class="select">
                    <select name="visibility">
                        <option value="" <?php @ihag_selected("",$_GET['visibility']);?>>Type de de Digital Cleanup</option>
                        <option value="public" <?php @ihag_selected("public",$_GET['visibility']);?>>Public</option>
                        <option value="private" <?php @ihag_selected("private",$_GET['visibility']);?>>Privé</option>
                    </select>
                </div>
                <div class="select">
                    <select name="location">
                        <option value="" <?php @ihag_selected("",$_GET['location']);?>>Présentiel / Distanciel</option>
                        <option value="location_facetoface" <?php @ihag_selected("location_facetoface",$_GET['location']);?>>Présentiel</option>
                        <option value="location_distancing" <?php @ihag_selected("location_distancing",$_GET['location']);?>>Distanciel</option>
                    </select>
                </div>
                <div class="select">
                    <select name="area">
                        <option value="" <?php @ihag_selected("",$_GET['area']);?>>Département</option>
                        <?php
                            $pays = get_field('geo_areas', 'option');
                            foreach($pays as $region) {
                                ?>
                                <optgroup label="<?php echo $region['region']; ?>">
                                <?php
                                $departements = $region['departements'];
                                foreach ($departements as $departement) {
                                    ?>
                                    <option value="<?php echo sanitize_key($departement['departement']) ?>" <?php @ihag_selected(sanitize_key($departement['departement']),$_GET['area']);?>><?php echo $departement['departement']; ?></option>
                                    <?php
                                }
                                ?>
                                </optgroup>
                                <?php
                            }
                        ?>
                    </select>
                </div>
                <script>
                    let autocomplete;
                    function autocompleteMap() {
                        const center = { lat: 46.227638, lng: 2.213749 };
                                
                        const input = document.getElementById("address");
                        const options = {
                                    
                            //componentRestrictions: { country: ["fr","ch","be"] },
                            fields: ["geometry"],
                            strictBounds: false,
                            types: ["locality", "postal_code", "street_address"],
                        };
                        autocomplete = new google.maps.places.Autocomplete(input, options);
                        autocomplete.addListener("place_changed", fillInAddress);
                    }
                    // [START maps_places_autocomplete_addressform_fillform]
                    function fillInAddress() {
                        var place = autocomplete.getPlace();
                        var lat = place.geometry.location.lat();
                        var lng = place.geometry.location.lng();
                        document.getElementById('coordonate').value = lat+','+lng;
                    }
                </script>
                <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6Mw5zx6630K1-bj76ffwABUmOALfsGwE&libraries=places&callback=autocompleteMap"></script>
            </div>
            <input type="submit" class="button" value="<?php _e('Rechercher','cwcud');?>">
        </form>
	</section>

    <div>
        <h3>Les Digital Cleanups en distanciel n'apparaissent pas sur la carte, mais sont visibles dans la liste de l'annuaire ci-dessous ⬇</h3>
    </div>

	<!-- Listing Archive -->
	<div class="">
		<!-- wrapper -->
		<?php
		wp_reset_query();
		query_posts($args);
		if ( $query2->have_posts() ) : 
			while ($query2->have_posts()) : $query2->the_post();
				$author_id = $post->post_author;
				$user = get_userdata( $author_id );
				if(!empty(get_post_meta( get_the_id(), "coordonate", true )) && get_post_meta( get_the_id(), "coordonate", true ) != ','){
					$tab_cleanup[] = array(
						'name'		=> get_the_title(),
						'link'		=> get_permalink(),
						'coordonate'=> get_post_meta( get_the_id(), "coordonate", true ),
						'etat'		=> get_post_meta( get_the_id(), "visibility", true ),
						'cat'		=> get_post_meta( get_the_id(), "cat_cybercleanup", true ),
						'organisator'=> $user->first_name." ".$user->last_name.' - '.get_post_meta(get_the_id(),'structure_name', true),
						'date' 		=> "le ".date_i18n('j/m', strtotime(get_post_meta( get_the_id(), "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta(get_the_id(), "time_start", true ))),
					);
				}
			endwhile; 
            ?>
            <style type="text/css">
                #mapCleanup{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
                    height:650px;
                    max-width:90vw;
                }
            </style>
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
            <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
            <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
            
            <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
            <script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>
            
            <script>
                var tab_cleanup = <?php echo json_encode($tab_cleanup);?>;
                document.addEventListener('DOMContentLoaded', function() {				
                <?php 
                if(isset($_GET['coordonate']) && !empty($_GET['coordonate'])):
                    $coordonate = explode(',',$_GET['coordonate']);
                    ?>
                    var lat = <?php echo $coordonate[0];?>;
                    var lon = <?php echo $coordonate[1];?>;
                    var zoom = 12;
                <?php else:?>
                    var lat = 46.5;
                    var lon = 2.349903;
                    var zoom = 6;
                <?php endif;?>
                var macarte = null;
                var markerClusters;
                elemMap = "mapCleanup"; 

                function initCluster() {
                    var iconBase = '<?php echo get_stylesheet_directory_uri(). '/image/' ?>';
                    macarte = L.map(elemMap).setView([lat, lon], zoom);
                    markerClusters = L.markerClusterGroup(
                        {
                            maxClusterRadius : 30,
                        }
                    );
                    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                        // Il est toujours bien de laisser le lien vers la source des données
                        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                        minZoom: 1,
                        maxZoom: 20
                    }).addTo(macarte);
                    tab_cleanup.forEach(cleanup => {
                        let popup = '<a href="' + cleanup['link'] + '">' + cleanup['name'] + '</a></br>' + cleanup['date'] + '</br>' + cleanup['organisator']
                        let cat_cleanup = cleanup['cat']
                        let coordonate = cleanup['coordonate'].split(',')

                        if(cat_cleanup == 'reuse') {
                            var myIcon = L.icon({
                                iconUrl: iconBase + "picto_reuse.png",
                                iconSize: [30, 45],
                                iconAnchor: [25, 50],
                                popupAnchor: [-3, -76],
                            });
                        }
                        else if(cat_cleanup == 'hardware') {
                            var myIcon = L.icon({
                                iconUrl: iconBase + "picto_hardware.png",
                                iconSize: [30, 45],
                                iconAnchor: [25, 50],
                                popupAnchor: [-3, -76],
                            });
                        }
                        else if(cat_cleanup == 'data') {
                            var myIcon = L.icon({
                                iconUrl: iconBase + "picto_data.png",
                                iconSize: [30, 45],
                                iconAnchor: [25, 50],
                                popupAnchor: [-3, -76],
                            });
                        }

                        var marker = L.marker([coordonate[0], coordonate[1]], { icon: myIcon });
                        marker.bindPopup(popup);
                        markerClusters.addLayer(marker);
                    });
                    macarte.addLayer(markerClusters);
                }
                
                function initMap() {
                    var iconBase = '<?php echo get_stylesheet_directory_uri(). '/image/' ?>';
                    tab_cleanup.forEach(cleanup => {
                        let coordonate = cleanup['coordonate'].split(',')
                        let cat_cleanup = cleanup['cat']

                        if(cat_cleanup == 'reuse') {
                            var myIcon = L.icon({
                                iconUrl: iconBase + "picto_reuse.png",
                                iconSize: [30, 45],
                                iconAnchor: [25, 50],
                                popupAnchor: [-3, -76],
                            });
                        }
                        else if(cat_cleanup == 'hardware') {
                            var myIcon = L.icon({
                                iconUrl: iconBase + "picto_hardware.png",
                                iconSize: [30, 45],
                                iconAnchor: [25, 50],
                                popupAnchor: [-3, -76],
                            });
                        }
                        else if(cat_cleanup == 'data') {
                            var myIcon = L.icon({
                                iconUrl: iconBase + "picto_data.png",
                                iconSize: [30, 45],
                                iconAnchor: [25, 50],
                                popupAnchor: [-3, -76],
                            });
                        }

                        var marker = L.marker([coordonate[0], coordonate[1]], { icon: myIcon }).addTo(macarte);
                    });
                }

		        initCluster();
		        //initMap();
                //initMap(lat, lon,zoom);
                });
            </script>

            <div id="mapCleanup"></div> 
            <p>
            <?php echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_data.png" width="30" alt="Nettoyage Données"> '; ?>Digital Cleanup Données <small>Supprimer nos données stockées sur nos équipements et sur le cloud</small><br>
            <?php echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_reuse.png"  width="30" alt="Réemploi"> '; ?>Digital Cleanup Réemploi : <small>Ressortir mes anciens équipements fonctionnels et leur donner une seconde vie (protéger, réutiliser, donner, réparer)</small><br>
            <?php echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_hardware.png" width="30" alt="Seconde vie des équipements numériques"> '; ?>Digital Cleanup Recyclage : <small>Récolter vos équipements numériques non fonctionnels afin de favoriser leur fin de vie (DEEE)</small><br>
            </p>
            <?php
		else :
            ?>
            <p>
                <?php _e( 'Il n\'y a pas de Digital Cleanup correpondant à votre recherche', 'cwcud' );?>
            </p>
            <?php
        endif;
        wp_reset_query();
	    ?>
	</div>
<?php endif; ?>