<?php
/**
 * Block Name: Mailjet
 */
 ?>

<?php

$title = get_field('title');
?>

<section class="wrapper">

<?php
if ( empty($title) ):
	echo '<em>Renseigner le bloc</em>';
else :
?>


	<?php if(!empty(get_field('title'))):?>
		<h2><?php the_field('title');?></h2>
	<?php endif; ?>

	<?php the_field('text');?>

	<form action="" method="post" name="mailjetForm" id="mailjetForm">
		<label for="email_newsletter"><?php _e('Adresse e-mail', 'cwcud') ?></label>
		<input type="email" name="email_newsletter" id="email_newsletter" required placeholder="<?php _e('adresse@mail.com', 'cwcud'); ?>">

		<!-- ok cgu -->
		<div class="checkbox form-item">
			<input type="checkbox" name="ok_cgu" id="ok_cgu" required >
			<div class="checkbox-label">
				<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles.', 'cwcud');?></label>
				<a href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits. *', 'cwcud') ;?></a>
			</div>
		</div>

		<p class="form-item">
			<?php _e('Votre adresse de messagerie est uniquement utilisée pour vous envoyer la newsletter du Digital Cleanup Day France. Vous pouvez à tout moment utiliser le lien de désabonnement intégré dans chaque newsletter.', 'cwcud');?>
		</p>

		<input type="hidden" name="honeypot" value="">
		<input type="submit" class="button" id="sendEmailMailjet" value="<?php the_field('button');?>">
		<div id="ResponseMessageMailjet" class="ResponseMessageMailjet">
		<?php _e('Merci, votre inscription a été enregistrée.', 'cwcud') ;?>
		</div>
	</form>

<?php endif; ?>

</section>

