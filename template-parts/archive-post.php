<?php
/**
 * Template part for displaying page archive-post in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="article-post">

	<!-- Thumbnail -->
	<a class="thumbnail" href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php 
			if ( has_post_thumbnail() ) { ?>
				<img src="<?php echo get_the_post_thumbnail_url($post, 'medium');?>" alt="<?php echo get_the_post_thumbnail_caption();?>">
			<?php
			} else {
				$image = get_field('imageFallback', 'option');
				$size = 'medium';
				echo wp_get_attachment_image( $image, $size );
			} 
		?>
	</a>

	<!-- Category -->
	<?php
	$term = ihag_get_term($post, 'taxo_post');
	echo '<i class="secondary body-like">'. $term->name. '</i>';
	?> 

	<!-- Title -->
	<a class="h6-like link-default" href="<?php the_permalink();?>">
		<?php the_title();?>
	</a>
	
	<?php // echo get_terms($cpt->name) ?>

	<!-- Date -->
	<time class="body-like gray-medium">
		<?php echo get_the_date();?>
	</time>

	<!-- Keywords -->
	<?php $all_keyWords = get_the_terms($post, "taxo_tag");
		if($all_keyWords):?>

			<?php foreach ($all_keyWords as $row):?>
				<a class="link-primary" href="<?php echo get_category_link($row);?>">#<?php echo $row->name; ?></a>
			<?php endforeach;?>
		 
		<?php endif; ?>

	<!-- Read more -->
	<!--<a href="<?php // the_permalink();?>">Plus de détails</a>-->

</article>


