<?php get_header(); ?>

<!-- Title -->
<header  id="cleanup-header" class="wrapper">
	<h1 class="no-margin">
	<?php	
	$visibility = get_post_meta( $post->ID, "visibility", true );
	$cat_cybercleanup = get_post_meta( $post->ID, "cat_cybercleanup", true );
	if ( $cat_cybercleanup == "data" ) { 
		echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_data.png" width="30" alt="Nettoyage Données"> '; 
	}
	elseif ( $cat_cybercleanup == "hardware" ) { 
		echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_hardware.png"  width="30" alt="Réemploi"> '; 
	}
	elseif ( $cat_cybercleanup == "reuse" ) { 
		echo '<img src="'.get_stylesheet_directory_uri() . '/image/picto_reuse.png" width="30" alt="Seconde vie des équipements numériques"> '; 
	}
	?>
	<?php the_title();?></h1>
</header>

<main class="wrapper" id="cleanup-layout">

	<div id="cleanup-content" class="no-useless-margin">
	<?php
    //désinscription particupant
    if(isset($_GET['remove_email']) && $_GET['key'] == crypt(sanitize_email($_GET['remove_email']), 'windows95')){
	//delete participant
	global $wpdb;

	$db_item =  $wpdb->get_results($wpdb->prepare(
		"SELECT id FROM {$wpdb->prefix}participant WHERE email LIKE %s", sanitize_email($_GET['remove_email'])
	));
	if($wpdb->num_rows == 1){
		$id_participant = $db_item[0]->id;
		$wpdb->delete( $wpdb->prefix.'participant_cleanup', array( 'id_participant' => $id_participant, 'id_cleanup' => get_the_id() ) );

		echo "<h2>".__('Votre désinscription à bien été prise en compte.', 'cwcud')."</h2>";

		$db_item =  $wpdb->get_results($wpdb->prepare(
			"SELECT id_cleanup FROM {$wpdb->prefix}participant_cleanup WHERE id_participant = %d", $id_participant
		));

		if($wpdb->num_rows == 0){
			$wpdb->delete( $wpdb->prefix.'participant', array( 'id' => $id_participant ) );
			echo "<br><h2>".__('Toutes les informations vous concernant, ont été supprimées.', 'cwcud')."</h2>";
		}

		$db_item =  $wpdb->get_results($wpdb->prepare(
			"SELECT sum(guest) as guest, sum(child) as child FROM {$wpdb->prefix}participant_cleanup WHERE id_cleanup = %d" , array( sanitize_text_field(get_the_id() ))
		));
		update_post_meta(get_the_id() , 'participants', ($db_item[0]->guest + $db_item[0]->child));
	}	
}
?>
		<p>
		<?php

		echo '</p><p><span class="label-like">';
		if(get_post_meta( $post->ID, "cat_cybercleanup", true ) == "data"):
			_e('Digital Cleanup Données', 'cwcud');
			echo '</span><br>';
			_e('Ce Digital Cleanup prévoit de nettoyer : ','cwcud');?>
			<ul>
				<?php 
				$data_cat = get_post_meta( get_the_id(), "data_cat", true );

				echo (is_array($data_cat) && in_array("data_desk", $data_cat))?'<li>'.__('Fichiers bureautiques', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("smartphone", $data_cat))?'<li>'.__('Smartphone', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("email", $data_cat))?'<li>'.__('Email', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("social", $data_cat))?'<li>'.__('Réseaux sociaux', 'cwcud').'</li>':'';
				$other = get_post_meta($post->ID, "data_cat_other", true );
				if(!empty($other)):
					echo '<li>'.$other.'</li>';
				endif;
				?>
			</ul>
		<?php
		elseif(get_post_meta( $post->ID, "cat_cybercleanup", true ) == "reuse"):
			_e('Digital Cleanup Reemploi', 'cwcud');
			echo '</span><br>';
			_e('Ce Digital Cleanup prévoit une (des) session(s) pour : ','cwcud');?>
			<ul>
				<?php 
				$data_cat = get_post_meta( get_the_id(), "reuse_cat", true );

				echo (is_array($data_cat) && in_array("reuse_smartphone", $data_cat))?'<li>'.__('Prendre soin de son smartphone', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("reuse_fix", $data_cat))?'<li>'.__('Réparer son ordinateur ou son smartphone', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("reuse_collect", $data_cat))?'<li>'.__('Collecter des smartphones et leur donner une seconde vie', 'cwcud').'</li>':'';
				?>
			</ul>
		<?php else:
			_e('Digital Cleanup Recyclage', 'cwcud');
			echo '</span><br>';
			/*_e('Ce Digital Cleanup prévoit de donner une seconde vie aux','cwcud');?>
			<ul>
				<?php 
				$data_cat = get_post_meta( get_the_id(), "hard_cat", true );

				echo (is_array($data_cat) && in_array("smartphones", $data_cat))?'<li>'.__('Smartphones (fonctionnels)', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("PC", $data_cat))?'<li>'.__('PC portables & tablettes (fonctionnels)', 'cwcud').'</li>':'';
				echo (is_array($data_cat) && in_array("DEEE", $data_cat))?'<li>'.__('DEEE numérique', 'cwcud').'</li>':'';
				$other = get_post_meta($post->ID, "hard_cat_other", true );
				if(!empty($other)):
					echo '<li>'.$other.'</li>';
				endif;*/
				?>
			</ul>
			
			<?php
				if("hard_no_orga" == get_post_meta($post->ID, "hard_collect", true )){
					?>

<p><b>
			<?php (!empty(get_post_meta($post->ID, "hard_collect", true ))) ? _e('La collecte sera effectuée', 'cwcud') : '';?><br>
			</b>
			<?php

					_e('dans un point d’apport local non-référencé Digital Cleanup Day : ', 'cwcud');
					echo get_post_meta($post->ID, "hard_collect_other", true );
				}
				
			?>
		<?php endif;?>
		</p>
		
		<!-- Description -->
		<p>
			<span class="label-like"><?php _e('Description : ') ;?></span></br>
			<?php the_content();//echo nl2br(get_post_meta( $post->ID, "description", true ));?>
		</p>
		<!--<span class="label-like">
		<?php
		_e('Sensibilisation animée par : </span><br>','cwcud');
		if(get_post_meta( $post->ID, "animate_cybercleanup", true ) == "orga"):
			_e('L’organisateur ou personne interne à l’organisation','cwcud');
		elseif(get_post_meta( $post->ID, "animate_cybercleanup", true ) == "reference_cwcud"):
			_e('Animateur NR partenaire référencé Digital Cleanup Day  : ','cwcud');
			echo get_post_meta( $post->ID, "animator_cwcud", true );
		elseif(get_post_meta( $post->ID, "animate_cybercleanup", true ) == "not_reference_cwcud"):
			_e('Animateur non-référencé Digital Cleanup Day : ','cwcud');
			echo get_post_meta( $post->ID, "animator_not_cwcud", true );
		endif;
		?>-->
		<p>
			<button id="display-map-cleanup" class="button"><?php _e('Afficher la carte','cwcud');?></button>
		</p>
		<div id="single-map-cleanup"></div>
		<?php
		$author_id = $post->post_author;
		$user = get_userdata( $author_id );
		$tab_cleanup = array();
		$tab_cleanup[] = array(
			'name'		=> get_the_title(),
			'link'		=> get_permalink(),
			'coordonate'=> get_post_meta( $post->ID, "coordonate", true ),
			'etat'		=> get_post_meta( $post->ID, "visibility", true ),
			'cat'		=> get_post_meta( $post->ID, "cat_cybercleanup", true ),
			'organisator'=> $user->first_name." ".$user->last_name.' - '.get_post_meta($post->ID,'structure_name', true),
			'date' 		=> "le ".date_i18n('j/m', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta( $post->ID, "time_start", true ))),
		);
		?>
		<script>
			var tab_cleanup = <?php echo json_encode($tab_cleanup);?>;
		</script>
		<style type="text/css">
			#single-map-cleanup{ /* la carte DOIT avoir une hauteur sinon elle n'apparaît pas */
				height:650px;
			}
		</style>
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
		<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
		<!--<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />-->
		
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
		<!--<script type='text/javascript' src='https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js'></script>-->
			
		<!-- date  -->
		
	</div>

	<div id="cleanup-infos" class="no-useless-margin">
		<div class="buttons-container">

			<?php
/*$tab_arg = array(
	'remove_email' => sanitize_email("romain@ihaveagreen.fr"),
	'key'	=> crypt(sanitize_email("romain@ihaveagreen.fr"), 'windows95'),
);
echo esc_url( add_query_arg( $tab_arg, get_the_permalink($_POST['id_cleanup'])));*/
			?>

			<?php
            if(get_post_meta( $post->ID, "button_visible", true ) != "no"):
                if(get_post_meta( $post->ID, "visibility", true ) == "public"):
                    $link_sign_in = get_post_meta($post->ID, 'link_sign_in', true);
                    if(!empty($link_sign_in)):?>
                        <a class="button " target="_blank" href="<?php echo $link_sign_in;?>">
                            <?php _e('Participer', 'cwcud'); ?>
                        </a>
                    <?php
                    else:
                    ?>
                    <button class="button inscription_cleanup" data-id="<?php echo get_the_ID();?>" 
                        data-title="<h2><?php _e('Inscription','cwcud');?> : <?php echo str_replace('"', '', get_the_title());?></h2><p>Organisé par <?php echo get_post_meta($post->ID, "structure_name", true);?>, le <?php echo date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")";?></p>">
                        <?php _e('Participer', 'cwcud'); ?>
                    </button>
                    <?php 
                    endif;
                    ?>
                    <button class="button-yellow" id="get_modale_orga" data-id-cleanup="<?php echo get_the_ID();?>">
                        <?php _e("Contacter l'organisateur", 'cwcud'); ?>
                    </button>
                <?php
                else:
                    echo '<p><span class="label-like">'.__('Le Digital Cleanup est privé.','cwcud').'</p>';
                endif;
            endif;
			?>
		</div>
		
		<p>
			<?php 
				echo "Le ".date_i18n('j F Y', strtotime(get_post_meta( $post->ID, "date_start", true )))." à ".date_i18n('H:i', strtotime(get_post_meta($post->ID, "time_start", true )))." (".get_post_meta($post->ID, "time_end", true ).")";

				if(!empty(get_post_meta( $post->ID, "during_day", true ) )){
					echo '<br>'. __('S\'étend sur ','cwcud') .' '. get_post_meta( $post->ID, "during_day", true ).' '. __('jour(s)','cwcud');
				}

			?>
		</p>

		<?php




		//$departements = arrayDepartements();
		$departement = get_post_meta($post->ID, "cleanup_area", true);
		echo '<p class="label-like">'.get_geo_area($departement).'</p>';
		?>

		<!-- if register 'structure name', display it -->
		<p class="label-like">
			<?php 
			$author_id = $post->post_author;
			//$user = get_userdata( $author_id );
			$nameStructure = get_post_meta( $post->ID, "structure_name", true );
			echo '<b>'.__('Organisé par','cwcud').' </b>: ';
			//echo ' '.$user->first_name.' '.$user->last_name;
			echo get_post_meta ($post->ID, 'user_name', true);
			if(!empty($nameStructure)):
				echo ' - '.$nameStructure;
			endif;
			?>
		</p>

		<p class="label-like">
			<?php 
				echo '<b>'.__('Numéro du Digital Cleanup','cwcud').'</b> : '.$post->ID;
			?>
		</p>

		<!-- nb participant -->
		<!--<p>
			<?php 
			if(get_post_meta( $post->ID, "visibility", true ) == "public"){
				$participants = (int)get_post_meta( $post->ID, 'participants', true );
				$nb_participant = get_post_meta( $post->ID, "nb_participant_max", true );
				
				echo '<span class="label-like">'.__('Nombre de participant','cwcud').' : </span>'.$participants;
				if(!empty($nb_participant)){
					echo ' / '.$nb_participant; 
				}
			}
			else{
				if(!empty(get_post_meta( $post->ID, 'nb_participant_target', true ))){
					echo '<span class="label-like">'.__('Nombre de personnes ciblées ou estimées','cwcud').' : </span>';
					echo get_post_meta( $post->ID, 'nb_participant_target', true );
				}
				echo '</span></p>';
			}
			?>  
		</p>-->

		<!-- child autorisation -->
		<?php /*$child = get_post_meta( $post->ID, "child", true );?>
		<?php if ($child == 'yes_child'):?>
			<p class="label-like">
				<?php _e('Accessible aux enfants', 'cwcud'); ?>
			</p>
		<?php endif;*/?>

		<?php 
		if(get_post_meta( $post->ID, "visibility", true ) == "public"):
			$whichLocation = get_post_meta( $post->ID, "location", true ) ;
			$adresse = get_post_meta($post->ID, "cleanup_adresse", true);
			$link = (!empty(get_post_meta($post->ID, "link_connection", true))) ? '<a href="'.get_post_meta($post->ID, "link_connection", true).'" target="_BLANK">'.get_post_meta($post->ID, "link_connection", true).'</a>' : 'Le lien vous sera envoyé ultérieurement par email.';
			$otherInformation = get_post_meta( $post->ID, "more_information", true );

			if ($whichLocation === "location_facetoface"):
				echo '<p><span class="label-like">'.__('Évènement présentiel','cwcud').' :</span> '.$adresse.'<br>'.$otherInformation.'</p>';
			elseif($whichLocation === "location_distancing"):
				echo '<p><span class="label-like">'.__('Évènement distanciel','cwcud').' : </span>'.$link.'</p>';
			elseif($whichLocation === "location_both"):
				echo '<p><span class="label-like">'.__('Évènement présentiel','cwcud').' :</span> '.$adresse.' - '.$otherInformation;
				echo '<br><span class="label-like">'.__('Évènement distanciel','cwcud').' :</span> '.$link.'</p>';
			endif;
		endif;
		?>

		<!-- share on social medias -->
		<div id="share">
		
			<?php
				$link_channel = get_post_meta($post->ID, 'link_channel', true);
				if(!empty($link_channel)):?>
					<a class="button" target="_blank" href="<?php echo $link_channel;?>">
						<?php _e('Plus d\'info', 'cwcud'); ?>
					</a>
				<?php
				endif;
			?>

			<p class="label-like"><?php esc_html_e('Partager :', 'cwcud')?></p>

			<a class="JSrslink link-icon" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Facebook', 'cwcud')?>">
				<img alt="<?php esc_html_e('Facebook', 'cwcud')?>" src="<?php echo get_template_directory_uri(); ?>/image/facebook.png" height="40" width="40">
			</a>

			<a class="JSrslink link-icon" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Twitter', 'cwcud')?>">
				<img alt="<?php esc_html_e('Twitter', 'cwcud')?>" src="<?php echo get_template_directory_uri(); ?>/image/twitter.png" height="40" width="40">
			</a>

			<a class="JSrslink link-icon" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Linkedin', 'cwcud')?>">
				<img alt="<?php esc_html_e('Linkedin', 'cwcud')?>" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.png" height="40" width="40">
			</a>          
		</div>
	</div>
	  
	<div class="modale <?php echo (isset($_GET['open']))?'active':'';?>" id="modale_inscription_cleanup">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
		<div class="cleanup-modale">
			<div id="modale_title_cleanup"><?php the_title();?>
			</div>
			<form class="form-white" action="" method="post" name="registerUserCleanUp" id="registerUserCleanUp">
				<label for="name_registerUserCleanUp"><?php _e('Nom Prénom', 'cwcud') ?>*</label>
				<input type="text" name="name_registerUserCleanUp" id="name_registerUserCleanUp" placeholder="<?php _e('Véronique Dubois', 'cwcud'); ?>" required>

				<label for="phone_registerUserCleanUp"><?php _e('Téléphone', 'cwcud') ?></label>
				<input type="tel" name="phone_registerUserCleanUp" id="phone_registerUserCleanUp" placeholder="<?php _e('+33 6 01 02 03 04', 'cwcud'); ?>">

				<label for="email_registerUserCleanUp"><?php _e('Adresse e-mail', 'cwcud') ?>*</label>
				<input type="email" name="email_registerUserCleanUp" id="email_registerUserCleanUp" required placeholder="<?php _e('adresse@mail.com', 'cwcud'); ?>">

				<p class="form-info">
				<?php _e('* Votre adresse email sera utilisée pour vous recontacter et vous tenir infomé.', 'cwcud'); ?>
				</p>

				<?php if ($child == 'yes_child'):?>
				<label for="invite_registerUserCleanUp"><?php _e("Nombre d'adultes invités (en plus de vous)", 'cwcud') ?></label>
				<input type="number" min="0" name="invite_registerUserCleanUp" id="invite_registerUserCleanUp" value="" placeholder="Laisser vide si pas d'invité">
				
				<label for="invite_child_registerUserCleanUp"><?php _e("Nombre d'enfants invités", 'cwcud') ?></label>
				<input type="number" min="0" name="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp"  value="" placeholder="Laisser vide si pas d'invité">
				<?php else:?>
				<label for="invite_registerUserCleanUp"><?php _e("Nombre d'invités (en plus de vous)", 'cwcud') ?></label>
				<input type="number" min="0" name="invite_registerUserCleanUp" id="invite_registerUserCleanUp"  value="" placeholder="Laisser vide si pas d'invité">
				
				<input type="hidden" name="invite_child_registerUserCleanUp" id="invite_child_registerUserCleanUp" >
				<?php endif;?>
				<p class="form-item no-margin form-legal-text"><?php _e('* Champs obligatoires', 'cwcud'); ?></p>

				<div class="checkbox form-item">
					<input type="checkbox" name="ok_cgu" id="ok_cgu" required>
					<div class="checkbox-label">
						<label for="ok_cgu"><?php _e('J\'accepte le traitement de mes données personnelles dans le cadre du Digital Cleanup Day.', 'cwcud');?>*</label>
						<div class="form-info">
							<a class="link-underlined" href="<?php echo get_privacy_policy_url();?>"><?php _e('En savoir plus sur la gestion de vos données et vos droits.', 'cwcud') ;?></a>
						</div>
					</div>
				</div>

				<div class="checkbox form-item">
					<input type="checkbox" name="checkbox_newsletter" id="checkbox_newsletter" value="true">
					<div class="checkbox-label">
						<label for="checkbox_newsletter">
							<?php _e("Je désire m'inscrire à la newsletter du Digital Cleanup Day", 'cwcud');?>
						</label>
					</div>
				</div>

				<input type="hidden" name="honeypot" value="">
				<input type="hidden" name="id_cleanup" id="id_cleanup" value="<?php echo get_the_id();?>">
				<input type="submit" class="button form-item" id="sendRegisterUserCleanUp" value="<?php _e("Valider l'inscription", 'cwcud'); ?>">
				<div id="ResponseMessageRegisterUserCleanUp" class="ResponseMessageRegisterUserCleanUp">
					<p class="ctr"><?php _e('Merci, votre inscription a été enregistrée.', '');?></p>
				</div>
			</form>
		</div>
	</div>

	<div class="modale" id="modale_orga">
		<p style="text-align:center;cursor:pointer;color:white" class="close-modale">X Fermer</p>
		<div class="embassy-modale" id="modale_orga_content">
		</div>
	</div>
	
</main>

<?php get_footer(); ?>
