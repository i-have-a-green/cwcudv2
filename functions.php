<?php
//include_once("include/required_plugins.php");
include_once("include/acf.php");
include_once("include/acf-block.php");
include_once("include/settings-gutenberg.php");
include_once("include/widget.php");
include_once("include/clean.php");
include_once("include/no-comment.php");
include_once("include/images.php");
include_once("include/custom-post-type.php");
include_once("include/breadcrumb.php");
include_once("include/menu.php");
include_once("include/enqueue_scripts.php");
include_once("include/form.php");
include_once("include/modale.php");
include_once("include/pagination.php");
include_once("include/profil-ambassadeur.php");
include_once('include/list-departements.php');
include_once('include/list-regions.php');
include_once('include/wcud.php');

// Adding excerpt for page
//add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
//add_filter( 'excerpt_length', function( $length ) { return 20; } );

function ihag_unregister_taxonomy(){
		register_taxonomy('post_tag', array());
		register_taxonomy('category', array());
	// add_post_type_support( 'page', 'excerpt' );
	// remove_post_type_support( 'page', 'thumbnail' );
}
add_action('init', 'ihag_unregister_taxonomy');

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {    
		if ( isset( $_GET['from_post'] ) ) {
				$my_post = get_post( $_GET['from_post'] );
				if ( $my_post )
						return $my_post->post_content;
		}
		return $content;
}
//add_filter( 'default_content', 'cw2b_content_copy' );

function my_pre_get_posts($query) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
			$query->set('post_type', array("post", "rendez-vous", "solution"));
			$query->set('posts_per_page', -1);
	} 

}
//add_action( 'pre_get_posts', 'my_pre_get_posts' );

function nbTinyURL($url)  {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

// recupere le term(valeur) d'une taxonomy (ex: Brad Pitt est le term de la taxonomy actor)
function ihag_get_term($post, $taxonomy){
	
	if ( class_exists('WPSEO_Primary_Term') ):
	$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, get_the_id( $post ) );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
	$term = get_term( $wpseo_primary_term );
	if ( is_wp_error( $term ) ) {
		$term = get_the_terms($post, $taxonomy);
		return $term[0];
	}
	return $term;
 else:
	 $term = get_the_terms($post, $taxonomy);
	return $term[0];
 endif;
}

function ihag_the_post_thumbnail( $size = 'medium', $attr = array() ){
	if ( has_post_thumbnail() ) :
		the_post_thumbnail($size, $attr);
	elseif (get_field('imageFallback', 'option')) :
		$image = get_field('imageFallback', 'option');
		echo wp_get_attachment_image( $image, $size );
	endif;
}

function select_geo_area($value, $name, $required="required"){
	
	?>
	<select name="<?php echo $name;?>" id="user_geo_area" <?php echo $required;?>>
	<option value=""><?php _e('Selectionner votre zone géographique', 'cwcud');?></option>
	<?php
	if( have_rows('geo_areas', 'options') ):
			while( have_rows('geo_areas', 'options') ) : the_row();
					?>
					<optgroup label="<?php the_sub_field('region');?>">
					<?php   
                        
                        switch (get_sub_field('region')) {
                            case 'France':
                                $select = ($value == 'fr') ? 'selected' : '';
                                echo'<option value="fr" '. $select . '>' . get_sub_field('region') . '</option>';
                                break;
                            case 'Suisse':
                                $select = ($value == 'ch') ? 'selected' : '';
                                echo'<option value="ch" '. $select . '>' . get_sub_field('region') . '</option>';
                                break;
                            case 'Belgique':
                                $select = ($value == 'be') ? 'selected' : '';
                                echo'<option value="be" '. $select . '>' . get_sub_field('region') . '</option>';
                                break;
                        }
                        
							$departements = get_sub_field('departements' );
							foreach( $departements as $departement ) :
							$departement = $departement['departement'];
							$val = sanitize_key($departement);
							?>
							<option value="<?php echo $val;?>" <?php selected($value, $val);?>><?php echo $departement;?></option>
							<?php
							endforeach;
					?>
					</optgroup>
					<?php
			endwhile;
	endif;
	?>
	</select>
	<?php
}

function get_geo_area($value){
	$r = '';
	if( have_rows('geo_areas', 'options') ):
		while( have_rows('geo_areas', 'options') ) : the_row();
			$departements = get_sub_field('departements' );
			foreach( $departements as $departement ) :
				$departement = $departement['departement'];
				if($value == sanitize_key($departement)){
					$r = $departement;
				}
			endforeach;
		endwhile;
	endif;
	return $r;
}

function ihag_selected($v1, $v2){
    if(isset($v1) && isset($v2)){
        selected($v1, $v2);
    }
}


function action_wpcf7_after_flamingo($results) { 
    $id_cleanup = get_post_meta($results['flamingo_inbound_id'], '_field_id_cleanup', true);

    update_post_meta($id_cleanup, 'flamingo', $results['flamingo_inbound_id']);
} 
add_action( 'wpcf7_after_flamingo', 'action_wpcf7_after_flamingo', 10, 1 );

// valeur par default de la la meta_key 'flamingo'
/*function creating_post_add_meta( $post_id ) {
    update_post_meta( $post_id, 'flamingo', '0' );
}
add_action( 'wp_insert_post', 'creating_post_add_meta', 1 );*/
