if (document.getElementById("registerUserCleanUp")) {
    //submit form
    document.forms.namedItem("registerUserCleanUp").addEventListener('submit', function(e) {
        document.getElementById('sendRegisterUserCleanUp').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("registerUserCleanUp");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'registerUserCleanUp', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById('sendRegisterUserCleanUp').disabled = false;
                document.getElementById('sendRegisterUserCleanUp').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageRegisterUserCleanUp').classList.add("showResponseMessage");
            } else if (xhr.status === 304) {
                console.log("304");
                document.getElementById('sendRegisterUserCleanUp').disabled = false;
                document.getElementById('sendRegisterUserCleanUp').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageRegisterUserCleanUp').innerHTML = "Votre inscription à déjà été prise en compte.";
                document.getElementById('ResponseMessageRegisterUserCleanUp').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    }); 
} 

if (document.getElementById("organisateurForm")) {
    //submit form
    document.forms.namedItem("organisateurForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("organisateurForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'new-orga', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                //alert(xhr.response);
                document.getElementById('sendMessage').disabled = false;
                if(document.getElementById('organisateurFormEn')){
                    location.href = document.getElementById('organisateurFormEn').value;
                }
                else{
                    location.href = page_add_cleanup; 
                }
            }
            if (xhr.status === 304) {
                //alert(xhr.response);
                document.getElementById('sendMessage').disabled = false;
                alert("Erreur : l'utilisateur existe déjà");
                //location.href = home_url;
            }
        };
        xhr.send(formData);
    });

    document.getElementById('user_pwd_btn').addEventListener('click', function(){
        document.getElementById('user_pwd').type = (document.getElementById('user_pwd').type == 'password') ? 'text' : 'password';
    });

}

if (document.getElementById("form_admin_organisateur")) {
    //submit form
    document.forms.namedItem("form_admin_organisateur").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("form_admin_organisateur");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'update-orga', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                location.href = page_my_account; 
            }
            if (xhr.status === 304) {
                location.href = home_url;
            }
        };
        xhr.send(formData);
    });
}
/*if (document.getElementById("form_ask_ambassadeur")) {
    //submit form
    document.forms.namedItem("form_ask_ambassadeur").addEventListener('submit', function(e) {
        document.getElementById('sendMessageAmbassadeur').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("form_ask_ambassadeur");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'ask-ambassadeur', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessageAmbassadeur').disabled = false;
                location.href = page_my_account; 
            }
            if (xhr.status === 304) {
                location.href = home_url;
            }
        };
        xhr.send(formData);
    });
}*/


if (document.getElementById("cleanupForm")) {
    
    //submit form
    document.forms.namedItem("cleanupForm").addEventListener('submit', function(e) {
       // console.log('test');
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic 
        e.preventDefault();

        var form = document.forms.namedItem("cleanupForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'new-cleanup', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                console.log(xhr.response);
                if (xhr.response === 'false') {
                    alert('problème.');
                } else {
                    document.getElementById('sendMessage').disabled = true;
                    //alert(xhr.response);
                    //location.href = page_list_cleanup;
                    if (typeof action_url !== 'undefined') {
                        location.href = action_url;
                    } else {
                        location.href = page_list_cleanup;
                    }
                }
            }
        };
        xhr.send(formData);
    });
}
/*
if (document.getElementById("updateCleanupForm")) {
    //submit form
    document.forms.namedItem("updateCleanupForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("updateCleanupForm");
        var formData2 = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'update-cleanup', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                console.log(xhr.response);
                if (xhr.response === 'false') {
                    alert('problème.');
                } else {
                    if (typeof action_url !== 'undefined') {
                        location.href = action_url;
                    } else {
                        location.href = page_list_cleanup;
                    }

                }
            }
        };
        xhr.send(formData2);
    });
}*/ 

function displayRadioValue() {
    var ele = document.getElementsByClassName('structure');
    for (i = 0; i < ele.length; i++) {
        if ((ele[i].id == 'citizen') && (ele[i].checked)) {
            document.getElementById("structure_name").style.display = 'none';
            document.getElementById("structure_name_id").required = false;
        } else if (ele[i].id == 'citizen') {
            document.getElementById("structure_name").style.display = 'inline-block';
            document.getElementById("structure_name_id").required = true;
        }
    }
}
if(document.getElementsByClassName('structure').length > 0){
    displayRadioValue();
}

function onchangeAnimateCWCUD(e){
    if(e.value == "reference_cwcud"){
        document.getElementById("animator_cwcud_container").style.display = "block";
    }
    else{
        document.getElementById("animator_cwcud_container").style.display = "none";
    }

    if(e.value == "not_reference_cwcud"){
        document.getElementById("animator_not_cwcud_container").style.display = "block";
    }
    else{
        document.getElementById("animator_not_cwcud_container").style.display = "none";
    }
}



/*
if(document.getElementsByClassName('location').length){
    displayInformations();
}
*/
function displayDate(e) {
    document.getElementById("date_sec").style.display = 'inline-block';
    if (!e.checked) {
        document.getElementById("date_sec").style.display = 'none';
    }
}

/*
if (document.getElementById("searchAdressMap")) {

    var lat = 48.852969;
    var lon = 2.349903;
    var macarte = null;
    var myMarker = null;
    window.onload = function() {
        AdminInitMap();
    };
    document.getElementById("searchAdressMap").addEventListener("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = new FormData();
        formData.append('address', document.getElementById("cleanup_adresse_id").value);
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'search-address', true);
        console.log(resturl);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() {
            if (xhr.status === 200) {
                coordinates = JSON.parse(xhr.response);
                if (coordinates[1]) {
                    var newLatLng = new L.LatLng(parseFloat(coordinates[1]), parseFloat(coordinates[0]));
                    myMarker.setLatLng(newLatLng);
                    macarte.setView(myMarker.getLatLng(), macarte.getZoom());
                    document.getElementById("coordonate").value = coordinates[1] + ',' + coordinates[0];
                }
            }
            if (xhr.status === 304) {
                console.log("304");
            }
        };
        xhr.send(formData);
    });
}

// Fonction d'initialisation de la carte
function AdminInitMap() {
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"

    if (document.getElementById("coordonate").value != "") {
        coordinates = document.getElementById("coordonate").value.split(',');
        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);
    }

    macarte = L.map('map').setView([lat, lon], 10);

    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);

    myMarker = L.marker([lat, lon], { title: "MyPoint", alt: "The Big I", draggable: true })
        .addTo(macarte)
        .on('dragend', function() {
            var coord = myMarker.getLatLng();
            document.getElementById("coordonate").value = coord['lat'] + "," + coord['lng'];
        });

}
*/
function displayMap() {
    var no = document.getElementById("no_on_map");
    var yes = document.getElementById("yes_on_map");
    if (no.checked) {
        document.getElementById("allMap").style.display = 'none';
    } else if (yes.checked) {
        document.getElementById("allMap").style.display = 'inline-block';
    }
}


/*
function changeVisibility(){
    var element = document.getElementsByClassName('visibility');
    for (var i = 0; i < element.length; i++) {
        if ((element[i].id == 'public') && (element[i].checked)) {
            document.getElementById("nb_participant_max_container").style.display = 'inline-block';
            document.getElementById("nb_participant_target_max_container").style.display = 'none';
        }
        else if(element[i].checked){
            document.getElementById("nb_participant_max_container").style.display = 'none';
            document.getElementById("nb_participant_target_max_container").style.display = 'inline-block';
        }
    }
}
*/

function initMap(lat, lon, zoom) { 
    // Nous définissons le dossier qui contiendra les marqueurs
    //var iconBase = 'http://localhost/carte/icons/';
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map(elemMap).setView([lat, lon], zoom);
    //markerClusters = L.markerClusterGroup();
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);
    // Nous parcourons la liste des villes
    var myIconData = L.icon({
        iconUrl: iconBase + "picto_data.png", 
        iconSize: [34, 42],
        iconAnchor: [17, 42],
        popupAnchor: [-0, -42],
    });
    var myIconHardware = L.icon({
        iconUrl: iconBase + "picto_hardware.png",
        iconSize: [34, 42],
        iconAnchor: [17, 42],
        popupAnchor: [-0, -42],
    });
    var myIconReuse = L.icon({
        iconUrl: iconBase + "picto_reuse.png",
        iconSize: [34, 42],
        iconAnchor: [17, 42],
        popupAnchor: [-0, -42],
    });
    for (ville in tab_cleanup) {        

        coordinates = tab_cleanup[ville].coordonate.split(",");

        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);
        
        
        if (tab_cleanup[ville].cat == "data") {
            var marker = L.marker([lat, lon], { icon: myIconData }).addTo(macarte);
        }
        else if (tab_cleanup[ville].cat == "hardware") {
            var marker = L.marker([lat, lon], { icon: myIconHardware }).addTo(macarte);
        }
        else if (tab_cleanup[ville].cat == "reuse") {
            var marker = L.marker([lat, lon], { icon: myIconReuse }).addTo(macarte);
        }
        marker.bindPopup('<a href="' + tab_cleanup[ville].link + '">' + tab_cleanup[ville].name + '</a><br>' + tab_cleanup[ville].date + '<br>' + tab_cleanup[ville].organisator);
        //markerClusters.addLayer(marker);
    }
    //macarte.addLayer(markerClusters);
}

if (document.getElementById("single-map-cleanup")) {
    var lat = 46.5;
    var lon = 2.349903;
    for (ville in tab_cleanup) {
        coordinates = tab_cleanup[ville].coordonate.split(",");
        lat = parseFloat(coordinates[0]);
        lon = parseFloat(coordinates[1]);
    }
    var macarte = null;
    var zoom = 12;
    elemMap = "single-map-cleanup";
    document.getElementById("display-map-cleanup").addEventListener("click", function(e) {
        initMap(lat, lon,zoom);
    });
}


document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("inscription_cleanup");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();

            //document.getElementById("modale_title_cleanup").innerHTML = el.dataset.title;
            //document.getElementById("id_cleanup").value = el.dataset.id;

            document.getElementById('modale_inscription_cleanup').classList.add("active");
        });
    });
});


document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("get_list_inscrits");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            var formData = new FormData();
            formData.append('id_cleanup', el.dataset.id);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'list-inscrits', true);
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    document.getElementById("list_inscrits").innerHTML = xhr.response;
                    document.getElementById('modal_list_inscrits').classList.add("active");
                }
            };
            xhr.send(formData);

        });
    });
}); 

if (document.getElementById("get_modale_orga")) {
    document.getElementById("get_modale_orga").addEventListener("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = new FormData();
        formData.append('id_cleanup', document.getElementById("get_modale_orga").dataset.idCleanup);
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'info-orga', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() { 
            if (xhr.status === 200) {
                console.log(xhr.response);
                document.getElementById("modale_orga_content").innerHTML = xhr.response;
                document.getElementById('modale_orga').classList.add("active");
            }
        };
        xhr.send(formData);
    });
}

function onchangeCatCWCUD(){
    var e = '';
    if(document.querySelectorAll('input[name="cat_cybercleanup"]:checked').length > 0){
        e = document.querySelector('input[name="cat_cybercleanup"]:checked').value;
    }
    document.getElementById("ok_engagement_collect").required = false;
    if(e == 'data'){
        document.getElementById('checkbox_data').style.display = 'block';
        document.getElementById('checkbox_hardware').style.display = 'none';
        document.getElementById('checkbox_reuse').style.display = 'none';
    }
    else if(e == 'reuse'){
        document.getElementById('checkbox_data').style.display = 'none';
        document.getElementById('checkbox_hardware').style.display = 'none';
        document.getElementById('checkbox_reuse').style.display = 'block';
    }
    else if(e == 'hardware'){
        document.getElementById('checkbox_data').style.display = 'none';
        document.getElementById('checkbox_hardware').style.display = 'block';
        document.getElementById('checkbox_reuse').style.display = 'none';
        document.getElementById('ok_engagement_collect').required = true;
    }
    else{
        document.getElementById('checkbox_data').style.display = 'none';
        document.getElementById('checkbox_hardware').style.display = 'none';
        document.getElementById('checkbox_reuse').style.display = 'none';
    }
    displayInformations();
    
}

function displayInformations() {
    var cat = '';
    if(document.querySelectorAll('input[name="cat_cybercleanup"]:checked').length > 0){
        cat = document.querySelector('input[name="cat_cybercleanup"]:checked').value;
    }
    var cat_a_b = document.querySelectorAll('.cat_a, .cat_b');
    var cat_c = document.querySelectorAll('.cat_c');
    
    
    if(cat == 'data' || cat == 'reuse'){
        cat_a_b.forEach(cat_a_b => {
            cat_a_b.style.display = 'block';
        });
        cat_c.forEach(cat_c => {
            cat_c.style.display = 'none';
        });
        //document.getElementById('add_info').style.display = 'none';
    }
    else if(cat == 'hardware'){
        document.getElementById('location_facetoface').checked = true;
        cat_a_b.forEach(cat_a_b => {
            cat_a_b.style.display = 'none';
        });
        cat_c.forEach(cat_c => {
            cat_c.style.display = 'block';
        });
        //document.getElementById('add_info').style.display = 'block';
    }
    else{
        
        cat_a_b.forEach(cat_a_b => {
            cat_a_b.style.display = 'none';
        });
        cat_c.forEach(cat_c => {
            cat_c.style.display = 'none';
        });
        
    }
    
    document.getElementById('cleanup_adresse').style.display = 'block';
    document.getElementById('cleanup_adresse_id').required = true;
    document.getElementById('link_connection').style.display = 'none';
    document.getElementById('add_info').style.display = 'block';
    if(document.querySelectorAll('input[name="location"]:checked').length > 0){
        if(document.querySelector('input[name="location"]:checked').value == 'location_distancing'){
            document.getElementById('add_info').style.display = 'none';
            document.getElementById('cleanup_adresse').style.display = 'none';
            document.getElementById('cleanup_adresse_id').required = false;
            document.getElementById('link_connection').style.display = 'block';
        }
    }
}

function onchangeHardCollect(e){
    document.getElementById('cont_hard_collect_other').style.display = 'none';
    if(e.value == 'hard_no_orga'){
        document.getElementById('cont_hard_collect_other').style.display = 'block';
    }
}


if (document.getElementById("bilanForm")) {
    document.getElementById("bilanForm").addEventListener("submit", function(e) {
        e.preventDefault();
        e.stopPropagation();
        var form = document.forms.namedItem("bilanForm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'bilanForm', true);
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        xhr.onload = function() { 
            if (xhr.status === 200) {
                //console.log(xhr.response);
                document.getElementById('submit-ok').style.display = 'inline-block';
                //location.href = page_list_cleanup;
            }
        };
        xhr.send(formData);
    });
}

